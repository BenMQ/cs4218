package test.sg.edu.nus.comp.cs4218.m2.integration;

import static org.junit.Assert.assertEquals;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.exception.AbstractApplicationException;
import sg.edu.nus.comp.cs4218.exception.ShellException;
import sg.edu.nus.comp.cs4218.impl.cmd.Evaluator;

public class ChainOfInteractionsGrepAndSubCmdTest {

	private final static String TEXT1 = "123" + System.lineSeparator() + "abc"
			+ System.lineSeparator() + "456" + System.lineSeparator() + "def"
			+ System.lineSeparator() + "";
	private final static String TEXT2 = "456" + System.lineSeparator() + "def"
			+ System.lineSeparator() + "123" + System.lineSeparator() + "abc"
			+ System.lineSeparator() + "";

	private final static String TEXT3 = "./fileA.txt";

	private static Evaluator evaluator = null;
	private static File testDir = null;
	private static File folderDepth1 = null;
	private static BufferedWriter writer = null;

	@BeforeClass
	public static void setUp() throws IOException {
		testDir = new File("test_resources");
		testDir.mkdir();

		String[] fileList1 = { "computer.txt", "computing.jar", "science.txt",
				"math.txt", "data.txt", "probability.txt" };
		for (String aFileList1 : fileList1) {
			File file = new File(testDir, aFileList1);
			file.createNewFile();
		}

		File testFile = new File(testDir, "fileA.txt");
		testFile.createNewFile();
		writer = new BufferedWriter(new FileWriter(testFile));
		writer.write(TEXT1);
		writer.close();

		// file with content text2
		testFile = new File(testDir, "fileC.txt");
		testFile.createNewFile();
		writer = new BufferedWriter(new FileWriter(testFile));
		writer.write(TEXT3);
		writer.close();

		// make folders in test_resources
		folderDepth1 = new File(testDir, "depth1");
		folderDepth1.mkdir();

		String[] fileList2 = { "computer.txt", "information.txt", "tech.txt",
				"coffee.txt", "dictionary.txt", "program.jar" };
		for (String aFileList2 : fileList2) {
			File file = new File(folderDepth1, aFileList2);
			file.createNewFile();
		}

		// file with content text2
		testFile = new File(folderDepth1, "fileB.txt");
		testFile.createNewFile();
		writer = new BufferedWriter(new FileWriter(testFile));
		writer.write(TEXT2);
		writer.close();

		// Go to folder test_resources
		Environment.currentDirectory += File.separatorChar + "test_resources";

		evaluator = new Evaluator();
	}

	@AfterClass
	public static void tearDown() {
		// recover working directory
		Environment.currentDirectory = System.getProperty("user.dir");

		for (File file : folderDepth1.listFiles()) {
			file.delete();
		}
		folderDepth1.delete();

		for (File file : testDir.listFiles()) {
			file.delete();
		}
		testDir.delete();

		evaluator = null;
		testDir = null;
		folderDepth1 = null;
		writer = null;
	}

	/**
	 * Boundary case
	 */
	@Test
	public void testGrepUsingSubcmdThatHasEmptyCommand()
			throws AbstractApplicationException, ShellException, IOException {
		String cmd = "grep 45 ``";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String expected = "";
		assertEquals(expected, outputStream.toString());
	}

	@Test
	public void testGrepUsingSubcmdFindWithNoSearchPattern()
			throws AbstractApplicationException, ShellException, IOException {
		String cmd = "grep `find -name *.txt`";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		// accept user input
		String expected = "456789";
		assertEquals("", outputStream.toString());
	}

	/**
	 * Positive and negative test cases
	 */
	@Test
	public void testGrepUsingSubcmdFindForSingleFile()
			throws AbstractApplicationException, ShellException {
		String cmd = "grep 45 `find -name fileA.txt`";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString();
		String expected = "456" + System.lineSeparator();
		assertEquals(expected, result);
	}

	@Test
	public void testGrepUsingSubcmdFindForSingleFileWithNoContentFound()
			throws AbstractApplicationException, ShellException {
		String cmd = "grep test `find -name fileA.txt`";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString();
		String expected = "";
		assertEquals(expected, result);
	}

	@Test
	public void testGrepUsingSubcmdFindForMultipleFile()
			throws AbstractApplicationException, ShellException {
		String cmd = "grep 45 `find -name \"*.txt\"`";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString();
		String expected = "456" + System.lineSeparator() + "456"
				+ System.lineSeparator();
		assertEquals(expected, result);
	}

	@Test
	public void testGrepUsingSubcmdFindForMultipleFileWithNoContentFound()
			throws AbstractApplicationException, ShellException {
		String cmd = "grep test `find -name \"*.txt\"`";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString();
		String expected = "";
		assertEquals(expected, result);
	}

	@Test
	public void testGrepUsingSubcmdFindForSingleFileBySpecifyingPath()
			throws AbstractApplicationException, ShellException {
		String cmd = "grep 45 `find ./depth1 -name \"*.txt\"`";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString();
		String expected = "456" + System.lineSeparator();
		assertEquals(expected, result);
	}

	@Test
	public void testGrepUsingSubcmdFindForSingleFileBySpecifyingInvalidPath()
			throws AbstractApplicationException, ShellException, IOException {
		String cmd = "grep 45 `find ./depth2 -name \"*.txt\"`";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String expected = "";
		assertEquals(expected, outputStream.toString());
	}

	@Test
	public void testGrepUsingSubcmdLsForCurrentDirectory()
			throws AbstractApplicationException, ShellException {
		String cmd = "grep 45 `ls`";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString();
		String expected = ""; // may print out directory information
		assertEquals(expected, result);
	}

	@Test
	public void testGrepUsingSubcmdLsForCurrentDirectoryWithNoContentFound()
			throws AbstractApplicationException, ShellException {
		String cmd = "grep test `ls`";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString();
		String expected = ""; // may print out directory information
		assertEquals(expected, result);
	}

	@Test
	public void testGrepUsingSubcmdLsForDepth1Directory()
			throws AbstractApplicationException, ShellException {
		String cmd = "grep 45 `ls ./depth`";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString();
		String expected = ""; // may print out directory information
		assertEquals(expected, result);
	}

	@Test
	public void testGrepUsingSubcmdLsForDepth1DirectoryWithNoContentFound()
			throws AbstractApplicationException, ShellException {
		String cmd = "grep test `ls ./depth`";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString();
		String expected = ""; // may print out directory information
		assertEquals(expected, result);
	}

	/**
	 * For special case, there may have file name contained in file string For
	 * cat, cd, echo, grep, head, tail, wc in subcommand, The result should
	 * print lots of invalid file message, since 'grep' treat each word as file
	 * name and search from that file. Generally the word has very small change
	 * to be the file name
	 */
	@Test
	public void testGrepUsingSubcmdCat() throws AbstractApplicationException,
			ShellException {
		String cmd = "grep 45 `cat fileC.txt`";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString();
		String expected = "456" + System.lineSeparator();
		assertEquals(expected, result);
	}

	@Test
	public void testGrepUsingSubcmdCatContentThatHasNoFileName()
			throws AbstractApplicationException, ShellException {
		String cmd = "grep 45 `cat fileA.txt`";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString();
		String expected = "";
		assertEquals(expected, result);
	}

	@Test
	public void testGrepUsingSubcmdEcho() throws AbstractApplicationException,
			ShellException {
		String cmd = "grep 45 `echo \"fileA.txt\"`";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString();
		String expected = "456" + System.lineSeparator();
		assertEquals(expected, result);
	}

	@Test
	public void testGrepUsingSubcmdEchoContentThatHasNoMatch()
			throws AbstractApplicationException, ShellException {
		String cmd = "grep 45 `echo \"fileC.txt\"`";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString();
		String expected = "";
		assertEquals(expected, result);
	}

	@Test
	public void testGrepUsingSubcmdHead() throws AbstractApplicationException,
			ShellException {
		String cmd = "grep 45 `head fileC.txt`";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString();
		String expected = "456" + System.lineSeparator();
		assertEquals(expected, result);
	}

	@Test
	public void testGrepUsingSubcmdHeadContentThatHasNoFileName()
			throws AbstractApplicationException, ShellException {
		String cmd = "grep 45 `head fileA.txt`";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString();
		String expected = "";
		assertEquals(expected, result);
	}

	@Test
	public void testGrepUsingSubcmdTail() throws AbstractApplicationException,
			ShellException {
		String cmd = "grep 45 `tail fileC.txt`";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString();
		String expected = "456" + System.lineSeparator();
		assertEquals(expected, result);
	}

	@Test
	public void testGrepUsingSubcmdTailContentThatHasNoFileName()
			throws AbstractApplicationException, ShellException {
		String cmd = "grep 45 `tail fileA.txt`";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString();
		String expected = "";
		assertEquals(expected, result);
	}
}
