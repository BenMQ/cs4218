package test.sg.edu.nus.comp.cs4218.m2.integration;

import static org.junit.Assert.assertEquals;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.exception.AbstractApplicationException;
import sg.edu.nus.comp.cs4218.exception.ShellException;
import sg.edu.nus.comp.cs4218.impl.cmd.Evaluator;

public class ChainOfApplicationSubCmdTest {
	private final static String TEXT1 = "123" + System.lineSeparator()
			+ "abc testtest" + System.lineSeparator() + "456"
			+ System.lineSeparator() + "def testtest" + System.lineSeparator()
			+ "";
	private final static String TEXT2 = "456" + System.lineSeparator() + "def"
			+ System.lineSeparator() + "123" + System.lineSeparator() + "abc"
			+ System.lineSeparator() + "";

	private final static String TEXT3 = "./fileA.txt";

	private static Evaluator evaluator = null;
	private static File testDir = null;
	private static File folderDepth1 = null;
	private static BufferedWriter writer = null;

	@BeforeClass
	public static void setUp() throws IOException {
		testDir = new File("test_resources");
		testDir.mkdir();

		String[] fileList1 = { "computer.txt", "computing.jar", "science.txt",
				"math.txt", "data.txt", "probability.txt" };
		for (String aFileList1 : fileList1) {
			File file = new File(testDir, aFileList1);
			file.createNewFile();
		}

		File testFile = new File(testDir, "fileA.txt");
		testFile.createNewFile();
		writer = new BufferedWriter(new FileWriter(testFile));
		writer.write(TEXT1);
		writer.close();

		// file with content text2
		testFile = new File(testDir, "fileC.txt");
		testFile.createNewFile();
		writer = new BufferedWriter(new FileWriter(testFile));
		writer.write(TEXT3);
		writer.close();

		// make folders in test_resources
		folderDepth1 = new File(testDir, "depth1");
		folderDepth1.mkdir();

		String[] fileList2 = { "computer.txt", "information.txt", "tech.txt",
				"coffee.txt", "dictionary.txt", "program.jar" };
		for (String aFileList2 : fileList2) {
			File file = new File(folderDepth1, aFileList2);
			file.createNewFile();
		}

		// file with content text2
		testFile = new File(folderDepth1, "fileB.txt");
		testFile.createNewFile();
		writer = new BufferedWriter(new FileWriter(testFile));
		writer.write(TEXT2);
		writer.close();

		// Go to folder test_resources
		Environment.currentDirectory += File.separatorChar + "test_resources";

		evaluator = new Evaluator();
	}

	@AfterClass
	public static void tearDown() {
		// recover working directory
		Environment.currentDirectory = System.getProperty("user.dir");

		for (File file : folderDepth1.listFiles()) {
			file.delete();
		}
		folderDepth1.delete();

		for (File file : testDir.listFiles()) {
			file.delete();
		}
		testDir.delete();

		evaluator = null;
		testDir = null;
		folderDepth1 = null;
		writer = null;
	}

	@Test
	public void testTwoSubCommandsWithCatAndFind()
			throws AbstractApplicationException, ShellException {
		String cmd = "cat `find -name fileA.txt` | grep \"testtest\" ; echo \"this is `echo \"testtest\"`\"";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString();
		String expected = "abc testtest" + System.lineSeparator()
				+ "def testtest" + System.lineSeparator() + "this is testtest"
				+ System.lineSeparator();
		assertEquals(expected, result);
	}

	@Test(expected = ShellException.class)
	public void testTwoSubCommandsWithCatAndFindAndInvalidSequentialCommandAtTail()
			throws AbstractApplicationException, ShellException {
		String cmd = "cat `find -name fileA.txt` | grep \"testtest\" ; echo \"this is `echo \"testtest\"`\" ; ''";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString();
		String expected = "abc testtest" + System.lineSeparator()
				+ "def testtest" + System.lineSeparator() + "this is testtest"
				+ System.lineSeparator();
		assertEquals(expected, result);
	}

	@Test(expected = ShellException.class)
	public void testTwoSubCommandsWithCatAndFindAndInvalidPipedCommandAtSecondCommand()
			throws AbstractApplicationException, ShellException {
		String cmd = "cat `find -name fileA.txt` | grep \"testtest\" ; '' | echo \"this is `echo \"testtest\"`\"";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString();
		String expected = "abc testtest" + System.lineSeparator()
				+ "def testtest" + System.lineSeparator();
		assertEquals(expected, result);
	}

	@Test
	public void testTwoSubCommandsWithLsCatAndFind()
			throws AbstractApplicationException, ShellException {
		String cmd = "cat `ls | grep fileA` | grep testtest ; grep testtest `find -name \"*.txt\"` | grep \"abc\"";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString();
		String expected = "abc testtest" + System.lineSeparator()
				+ "def testtest" + System.lineSeparator() + "abc testtest"
				+ System.lineSeparator();
		assertEquals(expected, result);
	}

	@Test(expected = ShellException.class)
	public void testTwoSubCommandsWithLsCatAndFindWithInvalidSequentialCommandAtMiddle()
			throws AbstractApplicationException, ShellException {
		String cmd = "cat `ls | grep fileA` | grep testtest ; '' ; grep testtest `find -name \"*.txt\"` | grep \"abc\"";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString();
		String expected = "abc testtest" + System.lineSeparator()
				+ "def testtest" + System.lineSeparator();
		assertEquals(expected, result);
	}

	@Test
	public void testThreeCommands() throws AbstractApplicationException,
			ShellException {
		String cmd = "echo \"testtest `cat fileA.txt | grep testtest`\"; "
				+ "echo \"testtest `cat fileA.txt`\" | grep \"def testtest\"; "
				+ "grep testtest `find -name \"*.txt\"`";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString();
		String expected = "testtest abc testtest def testtest"
				+ System.lineSeparator()
				+ "testtest 123 abc testtest 456 def testtest"
				+ System.lineSeparator() + "abc testtest"
				+ System.lineSeparator() + "def testtest"
				+ System.lineSeparator();
		assertEquals(expected, result);
	}

	@Test(expected = ShellException.class)
	public void testThreeCommandsWithInvalidPipedCommandInBetween()
			throws AbstractApplicationException, ShellException {
		String cmd = "echo \"testtest `cat fileA.txt | grep testtest`\"; "
				+ "echo \"testtest `cat fileA.txt`\" | grep \"def testtest\" | ''; "
				+ "grep testtest `find -name \"*.txt\"`";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString();
		String expected = "test abc testtest" + System.lineSeparator()
				+ "def testtest" + System.lineSeparator();
		assertEquals(expected, result);
	}

	@Test(expected = ShellException.class)
	public void testThreeCommandsWithInvalidSequentialCommandInBetween()
			throws AbstractApplicationException, ShellException {
		String cmd = "echo \"testtest `cat fileA.txt | grep testtest`\"; "
				+ "echo \"testtest `cat fileA.txt`\" | grep \"def testtest\" ; ''; "
				+ "grep testtest `find -name *.txt`";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString();
		String expected = "testtest abc testtest" + System.lineSeparator()
				+ "def testtest" + System.lineSeparator() + "def testtest"
				+ System.lineSeparator();
		assertEquals(expected, result);
	}
}
