package test.sg.edu.nus.comp.cs4218.m2.integration;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.exception.AbstractApplicationException;
import sg.edu.nus.comp.cs4218.exception.ShellException;
import sg.edu.nus.comp.cs4218.impl.cmd.Evaluator;

public class RedirectionAndFindTest {
	private final static String TEXT1 = "123" + System.lineSeparator() + "abc"
			+ System.lineSeparator() + "456" + System.lineSeparator() + "def"
			+ System.lineSeparator() + "";
	private final static String TEXT2 = "456" + System.lineSeparator() + "def"
			+ System.lineSeparator() + "123" + System.lineSeparator() + "abc"
			+ System.lineSeparator() + "";

	private final static String TEXT3 = "test string";

	private static Evaluator evaluator = null;
	private static File testDir = null;
	private static File folderDepth1 = null;
	private static BufferedWriter writer = null;

	/**
	 * Get string in the input file name
	 * 
	 * @param file
	 *            : The file name
	 * @return The content of the file in String
	 */
	private String getFileString(String file) throws IOException {
		BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
		String everything = null;
		try {
			StringBuilder stringBuilder = new StringBuilder();
			String line = bufferedReader.readLine();

			while (line != null) {
				stringBuilder.append(line);
				stringBuilder.append(System.lineSeparator());
				line = bufferedReader.readLine();
			}
			everything = stringBuilder.toString();

		} finally {
			bufferedReader.close();
		}
		return everything;
	}

	@Before
	public void setUp() throws IOException {
		testDir = new File("test_resources");
		testDir.mkdir();

		String[] fileList1 = { "computer.txt", "computing.jar", "science.txt",
				"math.txt", "data.txt", "probability.txt" };
		for (String aFileList1 : fileList1) {
			File file = new File(testDir, aFileList1);
			file.createNewFile();
		}

		File testFile = new File(testDir, "fileA.txt");
		testFile.createNewFile();
		writer = new BufferedWriter(new FileWriter(testFile));
		writer.write(TEXT1);
		writer.close();

		// file with content text2
		testFile = new File(testDir, "fileC.txt");
		testFile.createNewFile();
		writer = new BufferedWriter(new FileWriter(testFile));
		writer.write(TEXT3);
		writer.close();

		// make folders in test_resources
		folderDepth1 = new File(testDir, "depth1");
		folderDepth1.mkdir();

		String[] fileList2 = { "computer.txt", "information.txt", "tech.txt",
				"coffee.txt", "dictionary.txt", "program.jar" };
		for (String aFileList2 : fileList2) {
			File file = new File(folderDepth1, aFileList2);
			file.createNewFile();
		}

		// file with content text2
		testFile = new File(folderDepth1, "fileB.txt");
		testFile.createNewFile();
		writer = new BufferedWriter(new FileWriter(testFile));
		writer.write(TEXT2);
		writer.close();

		// Go to folder test_resources
		Environment.currentDirectory += File.separatorChar + "test_resources";

		evaluator = new Evaluator();
	}

	@After
	public void tearDown() {
		// recover working directory
		Environment.currentDirectory = System.getProperty("user.dir");

		for (File file : folderDepth1.listFiles()) {
			file.delete();
		}
		folderDepth1.delete();

		for (File file : testDir.listFiles()) {
			file.delete();
		}
		testDir.delete();

		evaluator = null;
		testDir = null;
		folderDepth1 = null;
		writer = null;
	}

	/**
	 * Test 1
	 */
	@Test
	public void testWriteToFile() throws AbstractApplicationException,
			ShellException, IOException {
		String cmd = "echo a > computer.txt";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String resultFileOut = this.getFileString("test_resources"
				+ File.separator + "computer.txt");
		// written to file
		assertEquals("a" + System.lineSeparator(), resultFileOut);
	}

	@Test
	public void testWriteToFileWithInvalidSequentialCommandAtTail()
			throws AbstractApplicationException, ShellException, IOException {
		String cmd = "echo a > computer.txt; ''";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		try {
			evaluator.parseAndEvaluate(cmd, outputStream);
		} catch (ShellException e) {
			// no op
		}
		String resultFileOut = this.getFileString("test_resources"
				+ File.separator + "computer.txt");
		// written to file
		assertEquals("a" + System.lineSeparator(), resultFileOut);
	}

	@Test(expected = ShellException.class)
	public void testWriteToFileWithInvalidSequentialCommandAtHead()
			throws AbstractApplicationException, ShellException, IOException {
		String cmd = "meow; echo a > computer.txt";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
	}

	/**
	 * Test 2
	 */
	@Test
	public void testWriteToFileWithSubCmd()
			throws AbstractApplicationException, ShellException, IOException {
		String cmd = "echo `echo a > computer.txt`";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String resultFileOut = this.getFileString("test_resources"
				+ File.separator + "computer.txt");
		// written to file
		assertEquals("a" + System.lineSeparator(), resultFileOut);
	}

	@Test
	public void testWriteToFileWithSubCmdWithInvalidSequentialCommandAtTail()
			throws AbstractApplicationException, ShellException, IOException {
		String cmd = "`echo a > computer.txt`; ''";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		try {
			evaluator.parseAndEvaluate(cmd, outputStream);
		} catch (ShellException e) {
			// no op
		}
		String resultFileOut = this.getFileString("test_resources"
				+ File.separator + "computer.txt");
		// written to file
		assertEquals("a" + System.lineSeparator(), resultFileOut);
	}

	@Test(expected = ShellException.class)
	public void testWriteToFileWithSubCmdWithInvalidSequentialCommandAtHead()
			throws AbstractApplicationException, ShellException, IOException {
		String cmd = "''; `echo a > computer.txt`";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String resultFileOut = this.getFileString("test_resources"
				+ File.separator + "computer.txt");
		// written to file
		assertEquals("", resultFileOut);
	}

	/**
	 * Test 3
	 */
	@Test
	public void testChangeDirectoryAndWriteToFile()
			throws AbstractApplicationException, ShellException, IOException {
		String cmd = "cd ./depth1 ; echo a > computer.txt";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String resultOut = outputStream.toString();
		String resultFileOut = this.getFileString("test_resources"
				+ File.separator + "depth1" + File.separator + "computer.txt");
		assertEquals("", resultOut);
		assertEquals("a" + System.lineSeparator(), resultFileOut);
	}

	@Test(expected = ShellException.class)
	public void testChangeDirectoryAndWriteToFileWithInvalidSequentialCommandAtMiddle()
			throws AbstractApplicationException, ShellException, IOException {
		String cmd = "cd ./depth1 ; ''; echo a > computer.txt";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
	}

	@Test(expected = ShellException.class)
	public void testEchoAndWriteToFileWithParsingError()
			throws AbstractApplicationException, ShellException, IOException {
		String cmd = "echo a > computer.txt > text.txt";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
	}

	/**
	 * Test 4
	 */
	@Test
	public void testWriteToFileAndThenReadTheFile()
			throws AbstractApplicationException, ShellException, IOException {
		String cmd = "cd ./depth1; echo a > computer.txt; cat computer.txt";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String resultOut = outputStream.toString();
		String resultFileOut = this.getFileString("test_resources"
				+ File.separator + "depth1" + File.separator + "computer.txt");
		assertEquals("a" + System.lineSeparator(), resultOut);
		assertEquals("a" + System.lineSeparator(), resultFileOut);
	}

	@Test(expected = ShellException.class)
	public void testWriteToFileAndThenReadTheFileWithInvalidCommandInBetween()
			throws AbstractApplicationException, ShellException, IOException {
		String cmd = "cd ./depth1; echo a > computer.txt; ''; cat computer.txt";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String resultFileOut = this.getFileString("test_resources"
				+ File.separator + "depth1" + File.separator + "computer.txt");
		// no output
		assertEquals("", outputStream.toString());
		// Write to file
		assertEquals("a" + System.lineSeparator(), resultFileOut);
	}

	/**
	 * Test 5
	 */
	@Test
	public void testFindApplicationByChangingWorkingDirectory()
			throws AbstractApplicationException, ShellException {
		String cmd = "cd ./depth1; find -name 'file*.txt'";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		assertEquals("."+File.separator+"fileB.txt" + System.lineSeparator(),
				outputStream.toString());
	}

	@Test
	public void testFindApplicationByChangingWorkingDirectoryWithNoFileFound()
			throws AbstractApplicationException, ShellException {
		String cmd = "cd ./depth1; find -name fileA.txt";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		assertEquals("", outputStream.toString());
	}

	/**
	 * Test 6
	 */
	@Test
	public void testFindApplicationByChangingWorkingDirectoryWithSpecifiedFindPath()
			throws AbstractApplicationException, ShellException {
		String cmd = "cd ./depth1; find .. -name \"*.txt\" | grep file";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String expected = ".."+File.separator+"fileA.txt" + System.lineSeparator()
				+ ".."+File.separator+"fileC.txt" + System.lineSeparator()
				+ ".."+File.separator+"depth1"+File.separator+"fileB.txt" + System.lineSeparator();
		assertEquals(expected, outputStream.toString());
	}

	@Test
	public void testFindApplicationByChangingWorkingDirectoryWithSpecifiedFindPathAndNoResultFound()
			throws AbstractApplicationException, ShellException {
		String cmd = "cd ./depth1; find .. -name test.txt";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String expected = "";
		assertEquals(expected, outputStream.toString());
	}

	/**
	 * Test 7
	 */
	@Test
	public void testInputRedirection() throws AbstractApplicationException,
			ShellException {
		String cmd = "cat < fileC.txt";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String expected = "test string";
		assertEquals(expected, outputStream.toString());
	}

	@Test(expected = ShellException.class)
	public void testInputRedirectionWithNoFileSpecified()
			throws AbstractApplicationException, ShellException {
		String cmd = "cat < ";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
	}

	/**
	 * Test 8
	 */
	@Test
	public void testInputRedirectionWithFind()
			throws AbstractApplicationException, ShellException {
		String cmd = "cat < `find -name fileC.txt`";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String expected = "test string";
		assertEquals(expected, outputStream.toString());
	}

	@Test
	// (expected = ShellException.class)
	public void testInputRedirectionWithFindWithNoFileFound()
			throws AbstractApplicationException, ShellException {
		String cmd = "cat < `find -name nosuchfile.txt`";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
	}

	@Test(expected = ShellException.class)
	public void testInputRedirectionWithFindWithInvalidCommandAtHead()
			throws AbstractApplicationException, ShellException {
		String cmd = "''; cat < `find -name fileC.txt`";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
	}

	@Test(expected = ShellException.class)
	public void testInputRedirectionWithFindWithInvalidCommandAtTail()
			throws AbstractApplicationException, ShellException {
		String cmd = "cat < `find -name fileC.txt`; ''";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
	}

	/**
	 * Test 9
	 */
	// @Test
	// public void testInputRedirectionWithFindAndChangeDirectory()
	// throws AbstractApplicationException, ShellException {
	// String cmd = "cd ./depth1;cat < `find -name fileB.txt` | grep ab";
	// ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
	// evaluator.parseAndEvaluate(cmd, outputStream);
	// String expected = "abc";
	// assertEquals(expected, outputStream.toString());
	// }

	@Test(expected = ShellException.class)
	public void testInputRedirectionWithFindAndChangeDirectoryWithInvalidCommandInBetween()
			throws AbstractApplicationException, ShellException {
		String cmd = "cd ./depth1;'';cat < `find -name fileB.txt`| grep ab";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
	}
}
