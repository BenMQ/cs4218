package test.sg.edu.nus.comp.cs4218.m2.integration;

import static org.junit.Assert.assertEquals;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.exception.AbstractApplicationException;
import sg.edu.nus.comp.cs4218.exception.ShellException;
import sg.edu.nus.comp.cs4218.impl.cmd.Evaluator;

public class ChainOfApplicationsPipeTest {
	private final static String TEXT1 = "123" + System.lineSeparator()
			+ "abc test" + System.lineSeparator() + "456"
			+ System.lineSeparator() + "def test" + System.lineSeparator() + "";
	private final static String TEXT2 = "456" + System.lineSeparator() + "def"
			+ System.lineSeparator() + "123" + System.lineSeparator() + "abc"
			+ System.lineSeparator() + "";

	private final static String TEXT3 = "456" + System.lineSeparator() + "def"
			+ System.lineSeparator() + "123" + System.lineSeparator() + "abc"
			+ System.lineSeparator() + "";

	private static Evaluator evaluator = null;
	private static File testDir = null;
	private static File folderDepth1 = null;
	private static BufferedWriter writer = null;

	@BeforeClass
	public static void setUp() throws IOException {
		testDir = new File("test_resources");
		testDir.mkdir();

		String[] fileList1 = { "computer.txt", "computing.jar", "science.txt",
				"math.txt", "data.txt", "probability.txt" };
		for (String aFileList1 : fileList1) {
			File file = new File(testDir, aFileList1);
			file.createNewFile();
		}

		File testFile = new File(testDir, "fileA.txt");
		testFile.createNewFile();
		writer = new BufferedWriter(new FileWriter(testFile));
		writer.write(TEXT1);
		writer.close();

		// file with content text2
		testFile = new File(testDir, "fileC.txt");
		testFile.createNewFile();
		writer = new BufferedWriter(new FileWriter(testFile));
		writer.write(TEXT3);
		writer.close();

		// make folders in test_resources
		folderDepth1 = new File(testDir, "depth1");
		folderDepth1.mkdir();

		String[] fileList2 = { "computer.txt", "information.txt", "tech.txt",
				"coffee.txt", "dictionary.txt", "program.jar" };
		for (String aFileList2 : fileList2) {
			File file = new File(folderDepth1, aFileList2);
			file.createNewFile();
		}

		// file with content text2
		testFile = new File(folderDepth1, "fileB.txt");
		testFile.createNewFile();
		writer = new BufferedWriter(new FileWriter(testFile));
		writer.write(TEXT2);
		writer.close();

		// Go to folder test_resources
		Environment.currentDirectory += File.separatorChar + "test_resources";

		evaluator = new Evaluator();
	}

	@AfterClass
	public static void tearDown() {
		// recover working directory
		Environment.currentDirectory = System.getProperty("user.dir");

		for (File file : folderDepth1.listFiles()) {
			file.delete();
		}
		folderDepth1.delete();

		for (File file : testDir.listFiles()) {
			file.delete();
		}
		testDir.delete();

		evaluator = null;
		testDir = null;
		folderDepth1 = null;
		writer = null;
	}

//	@Test
//	public void testAtLeastTwoPipes() throws AbstractApplicationException,
//			ShellException {
//		String cmd = "ls | grep \"file\"; cat fileA.txt | grep \"45\"";
//		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//		evaluator.parseAndEvaluate(cmd, outputStream);
//		String result = outputStream.toString().trim();
//		String expected = "computer.txt	computing.jar	data.txt	depth1	fileA.txt	fileC.txt	math.txt	probability.txt	science.txt"
//				+ System.lineSeparator() + "456";
//		assertEquals(expected, result);
//	}

//	@Test
//	public void testAtLeastTwoPipesWithNoFileInputForCatApplication()
//			throws AbstractApplicationException, ShellException, IOException {
//		String cmd = "ls | grep \"file\"; cat | grep \"45\"";
//		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//		evaluator.parseAndEvaluate(cmd, outputStream);
//		String result = outputStream.toString().trim();
//		String expected = "computer.txt	computing.jar	data.txt	depth1	fileA.txt	fileC.txt	math.txt	probability.txt	science.txt";
//		assertEquals(expected, result);
//	}

//	@Test
//	public void testAtLeastTwoPipesAddInvalidCommandButValidOutput()
//			throws AbstractApplicationException, ShellException {
//		String cmd = "ls | grep \"file\"; cat fileA.txt | grep \"45\";";
//		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//		evaluator.parseAndEvaluate(cmd, outputStream);
//		String result = outputStream.toString().trim();
//		String expected = "computer.txt	computing.jar	data.txt	depth1	fileA.txt	fileC.txt	math.txt	probability.txt	science.txt"
//				+ System.lineSeparator() + "456";
//		assertEquals(expected, result);
//	}

	@Test(expected = ShellException.class)
	public void testAtLeastTwoPipesAddInvalidCommandButNoOutput()
			throws AbstractApplicationException, ShellException {
		String cmd = "''; ls | grep \"file\"; cat fileA.txt | grep \"45\"";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
	}

	@Test
	public void testTwoConnectedPipesWithCat()
			throws AbstractApplicationException, ShellException {
		String cmd = "cat fileA.txt | grep \"test\" | grep \"abc\"";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString().trim();
		String expected = "abc test";
		assertEquals(expected, result);
	}

	@Test(expected = ShellException.class)
	public void testTwoConnectedPipesWithCatAndInvalidPipedCommand()
			throws AbstractApplicationException, ShellException {
		String cmd = "cat fileA.txt | grep \"test\" | grep \"abc\" | ''";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
	}

	@Test(expected = ShellException.class)
	public void testTwoConnectedPipesWithCatAndInvalidSequentialCommandAtTail()
			throws AbstractApplicationException, ShellException {
		String cmd = "cat fileA.txt | grep \"test\" | grep \"abc\" ; ''";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
	}

	@Test(expected = ShellException.class)
	public void testTwoConnectedPipesWithCatAndInvalidSequentialCommandAtHead()
			throws AbstractApplicationException, ShellException {
		String cmd = "''; cat fileA.txt | grep \"test\" | grep \"abc\"";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
	}

	@Test
	public void testTwoConnectedPipesWithFind()
			throws AbstractApplicationException, ShellException {
		String cmd = "find -name '*.txt' | grep \"file\" | grep \"C\"";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString().trim();
		String expected = "." + File.separatorChar + "fileC.txt";
		assertEquals(expected, result);
	}

	@Test(expected = ShellException.class)
	public void testTwoConnectedPipesWithFindAndInvalidPipedCommand()
			throws AbstractApplicationException, ShellException {
		String cmd = "find -name '*.txt' | grep \"file\" | grep \"C\" | ''";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString().trim();
		String expected = "";
		assertEquals(expected, result);
	}

	@Test(expected = ShellException.class)
	public void testTwoConnectedPipesWithFindAndInvalidSequentialCommandAtTail()
			throws AbstractApplicationException, ShellException {
		String cmd = "find -name '*.txt' | grep \"file\" | grep \"C\" ; ''";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
	}

	@Test(expected = ShellException.class)
	public void testTwoConnectedPipesWithFindAndInvalidSequentialCommandAtHead()
			throws AbstractApplicationException, ShellException {
		String cmd = "''; find -name '*.txt' | grep \"file\" | grep \"C\"";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
	}

	@Test
	public void testTwoConnectedPipesWithEcho()
			throws AbstractApplicationException, ShellException {
		String cmd = "echo \"abc test\" | grep \"test\" | grep \"abc\"";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString().trim();
		String expected = "abc test";
		assertEquals(expected, result);
	}

	@Test
	public void testTwoConnectedPipesWithEchoAndNoPatternFoundInSecondGrep()
			throws AbstractApplicationException, ShellException {
		String cmd = "echo \"test abc\" | grep \"test\" | grep \"def\"";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString();
		String expected = "";
		assertEquals(expected, result);
	}

	@Test
	public void testTwoConnectedPipesWithHead()
			throws AbstractApplicationException, ShellException {
		String cmd = "head ./fileA.txt | grep \"test\" | grep \"def\"";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString().trim();
		String expected = "def test";
		assertEquals(expected, result);
	}

	@Test
	public void testTwoConnectedPipesWithTail()
			throws AbstractApplicationException, ShellException {
		String cmd = "tail ./fileA.txt | grep \"test\" | grep \"def\"";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString().trim();
		String expected = "def test";
		assertEquals(expected, result);
	}
}
