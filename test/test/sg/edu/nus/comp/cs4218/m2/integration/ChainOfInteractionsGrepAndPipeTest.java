package test.sg.edu.nus.comp.cs4218.m2.integration;

import static org.junit.Assert.assertEquals;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.exception.AbstractApplicationException;
import sg.edu.nus.comp.cs4218.exception.ShellException;
import sg.edu.nus.comp.cs4218.impl.cmd.Evaluator;

public class ChainOfInteractionsGrepAndPipeTest {
	private final static String TEXT1 = "123" + System.lineSeparator() + "abc"
			+ System.lineSeparator() + "456" + System.lineSeparator() + "def"
			+ System.lineSeparator() + "";
	private final static String TEXT2 = "456" + System.lineSeparator() + "def"
			+ System.lineSeparator() + "123" + System.lineSeparator() + "abc"
			+ System.lineSeparator() + "";

	private final static String TEXT3 = "456" + System.lineSeparator() + "def"
			+ System.lineSeparator() + "123" + System.lineSeparator() + "abc"
			+ System.lineSeparator() + "";

	private static Evaluator evaluator = null;
	private static File testDir = null;
	private static File folderDepth1 = null;
	private static BufferedWriter writer = null;

	@BeforeClass
	public static void setUp() throws IOException {
		testDir = new File("test_resources");
		testDir.mkdir();

		String[] fileList1 = { "computer.txt", "computing.jar", "science.txt",
				"math.txt", "data.txt", "probability.txt" };
		for (String aFileList1 : fileList1) {
			File file = new File(testDir, aFileList1);
			file.createNewFile();
		}

		File testFile = new File(testDir, "fileA.txt");
		testFile.createNewFile();
		writer = new BufferedWriter(new FileWriter(testFile));
		writer.write(TEXT1);
		writer.close();

		// file with content text2
		testFile = new File(testDir, "fileC.txt");
		testFile.createNewFile();
		writer = new BufferedWriter(new FileWriter(testFile));
		writer.write(TEXT3);
		writer.close();

		// make folders in test_resources
		folderDepth1 = new File(testDir, "depth1");
		folderDepth1.mkdir();

		String[] fileList2 = { "computer.txt", "information.txt", "tech.txt",
				"coffee.txt", "dictionary.txt", "program.jar" };
		for (String aFileList2 : fileList2) {
			File file = new File(folderDepth1, aFileList2);
			file.createNewFile();
		}

		// file with content text2
		testFile = new File(folderDepth1, "fileB.txt");
		testFile.createNewFile();
		writer = new BufferedWriter(new FileWriter(testFile));
		writer.write(TEXT2);
		writer.close();

		// Go to folder test_resources
		Environment.currentDirectory += File.separatorChar + "test_resources";

		evaluator = new Evaluator();
	}

	@AfterClass
	public static void tearDown() {
		// recover working directory
		Environment.currentDirectory = System.getProperty("user.dir");

		for (File file : folderDepth1.listFiles()) {
			file.delete();
		}
		folderDepth1.delete();

		for (File file : testDir.listFiles()) {
			file.delete();
		}
		testDir.delete();

		evaluator = null;
		testDir = null;
		folderDepth1 = null;
		writer = null;
	}

	/**
	 * Boundary case
	 */
	@Test
	public void testGrepUsingPipeCatNoFile()
			throws AbstractApplicationException, ShellException, IOException {
		String cmd = "cat | grep \"45\"";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString();
		String expected = "";
		assertEquals(expected, result);
	}

	@Test
	public void testGrepUsingPipeCatNoPatternSpecified()
			throws AbstractApplicationException, ShellException {
		String cmd = "cat fileA.txt | grep";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString();
		String expected = "";
		assertEquals(expected, result);
	}

	/**
	 * Positive and negative test cases
	 */
	@Test
	public void testGrepUsingPipeSingleFileForCat()
			throws AbstractApplicationException, ShellException {
		String cmd = "cat fileA.txt | grep \"45\"";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString().trim();
		String expected = "456";
		assertEquals(expected, result);
	}

	@Test
	public void testGrepUsingPipeSingleFileForCatNoContentMatched()
			throws AbstractApplicationException, ShellException {
		String cmd = "cat computer.txt | grep \"45\"";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString();
		String expected = "";
		assertEquals(expected, result);
	}

	@Test
	public void testGrepUsingPipeMultipleFileForCat()
			throws AbstractApplicationException, ShellException {
		// "45" can be found in fileA.txt and fileC.txt
		String cmd = "cat * | grep \"45\"";

		ByteArrayOutputStream outputStream1 = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate("cat *", outputStream1);
		System.out.println(outputStream1.toString());
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString();
		String expected = "456" + System.lineSeparator() + "456"
				+ System.lineSeparator();
		assertEquals(expected, result);
	}

	@Test
	public void testGrepUsingPipeMultipleFileForCatWithNoContentFound()
			throws AbstractApplicationException, ShellException {
		// "test" can not be found in all files
		String cmd = "cat ./* | grep \"NOMATCH\"";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString();
		String expected = "";
		assertEquals(expected, result);
	}

	@Test
	public void testGrepUsingPipeSingleFileForFind()
			throws AbstractApplicationException, ShellException {
		String cmd = "find -name fileA.txt | grep \"file\"";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString().trim();
		String expected = "." + File.separatorChar + "fileA.txt";
		assertEquals(expected, result);
	}

	@Test
	public void testGrepUsingPipeSingleFileForFindNoContentFound()
			throws AbstractApplicationException, ShellException {
		String cmd = "find -name test.txt | grep \"file\"";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString();
		String expected = "";
		assertEquals(expected, result);
	}

	@Test
	public void testGrepUsingPipeMultipleFileForFind()
			throws AbstractApplicationException, ShellException {
		String cmd = "find -name '*.txt' | grep \"file\"";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString().trim();
		String expected = "." + File.separatorChar + "fileA.txt"
				+ System.lineSeparator() + "." + File.separatorChar
				+ "fileC.txt" + System.lineSeparator() + "."
				+ File.separatorChar + "depth1" + File.separatorChar
				+ "fileB.txt";
		assertEquals(expected, result);
	}

	@Test
	public void testGrepUsingPipeMultipleFileForFindNoContentMatch()
			throws AbstractApplicationException, ShellException {
		String cmd = "find -name *.txt | grep \"test\"";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString();
		String expected = "";
		assertEquals(expected, result);
	}

//	@Test
//	public void testGrepUsingPipeForLs() throws AbstractApplicationException,
//			ShellException {
//		String cmd = "ls | grep \"file\"";
//		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//		evaluator.parseAndEvaluate(cmd, outputStream);
//		String result = outputStream.toString().trim();
//		String expected = "computer.txt	computing.jar	data.txt	depth1	fileA.txt	fileC.txt	math.txt	probability.txt	science.txt";
//		assertEquals(expected, result);
//	}

	@Test
	public void testGrepUsingPipeForLsWithNoContentMatch()
			throws AbstractApplicationException, ShellException {
		String cmd = "ls | grep \"test\"";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString();
		String expected = "";
		assertEquals(expected, result);
	}

//	@Test
//	public void testGrepUsingPipeForLsInDepth1()
//			throws AbstractApplicationException, ShellException {
//		String cmd = "ls ./depth1 | grep \"file\"";
//		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//		evaluator.parseAndEvaluate(cmd, outputStream);
//		String result = outputStream.toString().trim();
//		String expected = "coffee.txt	computer.txt	dictionary.txt	fileB.txt	information.txt	program.jar	tech.txt";
//		assertEquals(expected, result);
//	}

	@Test
	public void testGrepUsingPipeForLsInDepth1WithNoContentFound()
			throws AbstractApplicationException, ShellException {
		String cmd = "ls ./depth1 | grep \"test\"";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString();
		String expected = "";
		assertEquals(expected, result);
	}

	@Test
	public void testGrepUsingPipeForEcho() throws AbstractApplicationException,
			ShellException {
		String cmd = "echo test | grep \"te\"";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString().trim();
		String expected = "test";
		assertEquals(expected, result);
	}

	@Test
	public void testGrepUsingPipeForEchoWithNoContent()
			throws AbstractApplicationException, ShellException {
		String cmd = "echo | grep \"te\"";
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		evaluator.parseAndEvaluate(cmd, outputStream);
		String result = outputStream.toString();
		String expected = "";
		assertEquals(expected, result);
	}

}
