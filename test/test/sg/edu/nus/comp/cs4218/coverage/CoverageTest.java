package test.sg.edu.nus.comp.cs4218.coverage;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.exception.SedException;
import sg.edu.nus.comp.cs4218.exception.WcException;
import sg.edu.nus.comp.cs4218.impl.app.SedApplication;
import sg.edu.nus.comp.cs4218.impl.app.WcApplication;
import sg.edu.nus.comp.cs4218.impl.util.StringUtilities;

public class CoverageTest {
	private static WcApplication wcApp = null;
	private ByteArrayOutputStream outContent = new ByteArrayOutputStream();

	@Before
	public void before() throws Exception {
		wcApp = new WcApplication();

		Environment.currentDirectory = System.getProperty("user.dir");
	}

	@After
	public void after() throws Exception {
		wcApp = null;
	}
	

	static private File testDir = new File("testDir");

	@BeforeClass
	public static void setup() throws IOException {
		testDir.mkdir();
		File testFile = new File(testDir, "1.txt");
		testFile.createNewFile();
		BufferedWriter writer = new BufferedWriter(new FileWriter(testFile));
		writer.write("1");
		writer.close();

		testFile = new File(testDir, "2.txt");
		testFile.createNewFile();
		writer = new BufferedWriter(new FileWriter(testFile));
		writer.write("2");
		writer.close();
	}

	@AfterClass
	public static void tearDown() throws InterruptedException {
		for (File file : testDir.listFiles()) {
			file.delete();
		}
		testDir.delete();
	}
	
	@Test
	public void testWcOptionsCombination() {
		String testString = "12345678";
		for (int i = 1; i <= 0b111; i ++ ) {

			ArrayList<String> args = new ArrayList<String>();
			ArrayList<String> expected = new ArrayList<String>();

			if ((i & 0b010) != 0) {
				args.add("-w");
				expected.add("1");
			}
			if ((i & 0b100) != 0) {
				args.add("-l");
				expected.add("1");
			}
			if ((i & 0b001) != 0) {
				args.add("-m");
				expected.add("8");
			}
			String[] argsArray = new String[args.size()];
			argsArray = args.toArray(argsArray);
			String[] expectedArray = new String[expected.size()];
			expectedArray = expected.toArray(expectedArray);

			String expectedString = StringUtilities.joinArray(expectedArray, "\t");
			InputStream inputStream = new ByteArrayInputStream(
					testString.getBytes(StandardCharsets.UTF_8));
			try {
				wcApp.run(argsArray, inputStream, outContent);
			} catch (WcException e) {
				fail(e.getMessage());
			}
			assertEquals(expectedString + System.lineSeparator(), outContent.toString());
			outContent = new ByteArrayOutputStream();
		}
	}
	

	@Test
	public void testWcEmpty() {
		String testString = "";
		InputStream inputStream = new ByteArrayInputStream(
				testString.getBytes(StandardCharsets.UTF_8));
		try {
			wcApp.run(null, inputStream, outContent);
		} catch (WcException e) {
			fail(e.getMessage());
		}
		assertEquals("1\t1\t0" + System.lineSeparator(), outContent.toString());
	}
	

	@Test(expected=WcException.class)
	public void testNonExistingFile() throws IOException, WcException {
		wcApp.run(new String[] { "THIS_SHOULD_NOT_BE_FOUND/1.txt"}, null,
					outContent);
	}

	@Test//(expected=WcException.class)
	public void testWcDir() throws IOException, WcException {
		try {
			wcApp.run(new String[] { "testDir"}, null,
					outContent);
		} catch (WcException e) {
			return;
		}
		fail();
		
	}
	

	@Test(expected=WcException.class)
	public void testWcUnknownOptions() throws WcException {
		String testString = "12345678";
		String[] options = {"-moption", "-loptions", "-woptions"};
		InputStream inputStream = new ByteArrayInputStream(
		testString.getBytes(StandardCharsets.UTF_8));
		wcApp.run(options, inputStream, outContent);
	}
	

	@Test(expected=WcException.class)
	public void testWcBlankOptions() throws WcException {
		String[] options = {"-"};
		wcApp.run(options, null, outContent);
	}


	@Test(expected=WcException.class)
	public void testWcNullArg() throws WcException {
		String[] options = { null };
		wcApp.run(options, null, outContent);

	}

	@Test(expected=NullPointerException.class)
	public void testWcNullArg2() throws WcException {
		String testString = "12345678";

		InputStream inputStream = new ByteArrayInputStream(
		testString.getBytes(StandardCharsets.UTF_8));
		
		String[] options = { null };

		wcApp.run(options, inputStream, outContent);
	}

	@Test(expected=WcException.class)
	public void testWcEmptyArg() throws WcException {
		String[] options = { "  " };
		wcApp.run(options, null, outContent);
	}
	

	@Test(expected=WcException.class)
	public void testWcEmptyArg2() throws WcException {
		String testString = "12345678";

		InputStream inputStream = new ByteArrayInputStream(
		testString.getBytes(StandardCharsets.UTF_8));
		
		String[] options = { "   " };

		wcApp.run(options, inputStream, outContent);
	}

	@Test
	public void testWcFiles() throws IOException, InterruptedException {
		for (int i = 1; i <= 0b111; i ++ ) {
			ArrayList<String> args = new ArrayList<String>();
			ArrayList<String> expected1 = new ArrayList<String>();
			ArrayList<String> expected2 = new ArrayList<String>();
			ArrayList<String> expected3 = new ArrayList<String>();

			if ((i & 0b010) != 0) {
				args.add("-w");
				expected1.add("1");
				expected2.add("1");
				expected3.add("2");
			}
			if ((i & 0b100) != 0) {
				args.add("-l");
				expected1.add("1");
				expected2.add("1");
				expected3.add("2");
			}
			if ((i & 0b001) != 0) {
				args.add("-m");
				expected1.add("1");
				expected2.add("1");
				expected3.add("2");
			}
			
			args.add("testDir/1.txt");
			args.add("testDir/2.txt");
			
			String[] argsArray = new String[args.size()];
			argsArray = args.toArray(argsArray);
			String[] expectedArray1 = new String[expected1.size()];
			expectedArray1 = expected1.toArray(expectedArray1);
			String[] expectedArray2 = new String[expected2.size()];
			expectedArray2 = expected2.toArray(expectedArray2);
			String[] expectedArray3 = new String[expected3.size()];
			expectedArray3 = expected3.toArray(expectedArray3);

			String expectedString = StringUtilities.joinArray(expectedArray1, "\t") + System.lineSeparator()
					+StringUtilities.joinArray(expectedArray2, "\t") + System.lineSeparator()
					+StringUtilities.joinArray(expectedArray3, "\t") + "\ttotal";
			
			try {
				wcApp.run(argsArray, null,
						outContent);
			} catch (WcException e) {
				fail(e.getMessage());
			}
			assertEquals(expectedString + System.lineSeparator(), outContent.toString());
			outContent = new ByteArrayOutputStream();
		}
	}
	
	@Test(expected=SedException.class)
	public void testSedNullStdin() throws SedException {
		SedApplication sedApp= new SedApplication(); 
		sedApp.run(new String[] { "x" }, null, outContent);
	}

	@Test(expected=SedException.class)
	public void testSedFileNotFound() throws SedException {
		SedApplication sedApp= new SedApplication(); 
		sedApp.run(new String[] { "x", "THIS_IS_NOT_A_FILE/1.txt" }, null, outContent);
	}
	
	@Test(expected=SedException.class)
	public void testSedDir() throws SedException {
		SedApplication sedApp= new SedApplication(); 
		sedApp.run(new String[] { "x", "testDir" }, null, outContent);
	}


	@Test(expected=SedException.class)
	public void testSedMissingG() throws SedException {
		SedApplication sedApp= new SedApplication(); 
		sedApp.run(new String[] { "s/1/1/1", "testDir/1.txt" }, null, outContent);
	}

	@Test(expected=SedException.class)
	public void testSedMissingS() throws SedException {
		SedApplication sedApp= new SedApplication(); 
		sedApp.run(new String[] { "x/1/1/", "testDir/1.txt" }, null, outContent);
	}

	@Test(expected=SedException.class)
	public void testSedMissingSWithG() throws SedException {
		SedApplication sedApp= new SedApplication(); 
		sedApp.run(new String[] { "x/1/1/g", "testDir/1.txt" }, null, outContent);
	}
	

	@Test(expected=SedException.class)
	public void testSedMissingGWithS() throws SedException {
		SedApplication sedApp= new SedApplication(); 
		sedApp.run(new String[] { "sx1x1x1x", "testDir/1.txt" }, null, outContent);
	}

	@Test
	public void testSedGAsDelimiter() throws SedException {
		String testString = "12345678";
		InputStream inputStream = new ByteArrayInputStream(
		testString.getBytes(StandardCharsets.UTF_8));
		SedApplication sedApp= new SedApplication(); 
		sedApp.run(new String[] { "sgagag" }, inputStream, outContent);
		assertEquals(testString+System.lineSeparator(), outContent.toString());
	}
	

	@Test
	public void testSedG() throws SedException {
		String testString = "12345678";
		InputStream inputStream = new ByteArrayInputStream(
		testString.getBytes(StandardCharsets.UTF_8));
		SedApplication sedApp= new SedApplication(); 
		sedApp.run(new String[] { "s/x/x/g" }, inputStream, outContent);
		assertEquals(testString+System.lineSeparator(), outContent.toString());
	}

}
