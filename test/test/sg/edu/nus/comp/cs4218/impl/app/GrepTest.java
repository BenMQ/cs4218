package test.sg.edu.nus.comp.cs4218.impl.app;

import static org.junit.Assert.assertEquals;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Pattern;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.exception.GrepException;
import sg.edu.nus.comp.cs4218.impl.app.GrepApplication;

public class GrepTest {

	static private GrepApplication grep;

	static private File testDir = new File("testGrepDir");

	@BeforeClass
	public static void setup() throws IOException {
		testDir.mkdir();
		File testFile = new File(testDir, "1.txt");
		testFile.createNewFile();
		BufferedWriter writer = new BufferedWriter(new FileWriter(testFile));
		writer.write("The quick brown fox jumps over the lazy dog");
		writer.write(System.lineSeparator());
		writer.write("123");
		writer.write(System.lineSeparator());
		writer.write("12345");
		writer.close();

		testFile = new File(testDir, "2.txt");
		testFile.createNewFile();
		writer = new BufferedWriter(new FileWriter(testFile));
		writer.write("The quick brown fox jumps over the lazy dog2");
		writer.write(System.lineSeparator());
		writer.close();
	}

	@AfterClass
	public static void tearDown() {
		for (File file : testDir.listFiles()) {
			file.delete();
		}
		testDir.delete();
	}

	@Before
	public void before() {
		grep = new GrepApplication();
	}

	@Test
	public void testGenerateContentsFrom() throws GrepException {
		ArrayList<String> expected = new ArrayList<String>(Arrays.asList("123",
				"12345"));
		grep.pattern = Pattern.compile("[0-9]{2,3}");
		File target = new File(testDir, "1.txt");
		ArrayList<String> actual = grep.generateContentsFrom(target);
		assertEquals(expected, actual);
	}

	@Test
	public void testGenerateContentsFromNoMatch() throws GrepException {
		ArrayList<String> expected = new ArrayList<String>();
		grep.pattern = Pattern.compile("[0-9]{2,3}");
		File target = new File(testDir, "2.txt");
		ArrayList<String> actual = grep.generateContentsFrom(target);
		assertEquals(expected, actual);
	}

	@Test(expected = GrepException.class)
	public void testGenerateContentsFromNoFile() throws GrepException {
		ArrayList<String> expected = new ArrayList<String>();
		grep.pattern = Pattern.compile("[0-9]{2,3}");
		File target = new File(testDir, "NOSUCHFILE.txt");
		ArrayList<String> actual = grep.generateContentsFrom(target);
		assertEquals(expected, actual);
	}

	@Test
	public void testGenerateContentsFromStdIn() throws GrepException {
		ArrayList<String> expected = new ArrayList<String>(Arrays.asList("123",
				"12345"));
		grep.pattern = Pattern.compile("[0-9]{2,3}");
		InputStream target = new ByteArrayInputStream(("hello"
				+ System.lineSeparator() + "world" + System.lineSeparator()
				+ "123" + System.lineSeparator() + "12345").getBytes());
		ArrayList<String> actual = grep.generateContentFromStdIn(target);
		assertEquals(expected, actual);
	}

	@Test
	public void testGenerateContentsFromStdInNoMatch() throws GrepException {
		ArrayList<String> expected = new ArrayList<String>();
		grep.pattern = Pattern.compile("[0-9]{2,3}");
		InputStream target = new ByteArrayInputStream(("hello"
				+ System.lineSeparator() + "world").getBytes());
		ArrayList<String> actual = grep.generateContentFromStdIn(target);
		assertEquals(expected, actual);
	}

	@Test(expected = GrepException.class)
	public void testProcessPatternInvalid() throws GrepException {
		grep.processPattern("[0-9");
	}

	@Test
	public void testProcessPatternValid() throws GrepException {
		grep.processPattern("[0-9]");
		assertEquals("[0-9]", grep.pattern.toString());
	}

	@Test(expected = GrepException.class)
	public void testProcessOption() throws GrepException {
		grep.processOption("-t");
	}

	@Test(expected = GrepException.class)
	public void testProcessOption2() throws GrepException {
		grep.processOption("-m");
	}

	@Test
	public void testProcessPath() throws GrepException {
		grep.processPath("testGrepDir/1.txt");
		grep.processPath("testGrepDir/2.txt");
		ArrayList<File> expected = new ArrayList<File>();
		File currentDirectory = new File(Environment.currentDirectory);
		expected.add(new File(currentDirectory, "testGrepDir/1.txt"));
		expected.add(new File(currentDirectory, "testGrepDir/2.txt"));
		assertEquals(expected, grep.targetPaths);
	}

	@Test(expected = GrepException.class)
	public void testProcessPathNoSuchFile() throws GrepException {
		grep.processPath("NOSUCHFILE.txt");
	}

	@Test
	public void testProcessArgument() throws GrepException {
		grep.processArguments("[0-9]{2,3}", "testGrepDir/1.txt");
		assertEquals("[0-9]{2,3}", grep.pattern.toString());

		ArrayList<File> expected = new ArrayList<File>();
		File currentDirectory = new File(Environment.currentDirectory);
		expected.add(new File(currentDirectory, "testGrepDir/1.txt"));
		assertEquals(expected, grep.targetPaths);
	}

	@Test
	public void testProcessArgument2() throws GrepException {
		grep.processArguments("[0-9]{2,3}", "testGrepDir/1.txt",
				"testGrepDir/2.txt");
		assertEquals("[0-9]{2,3}", grep.pattern.toString());

		ArrayList<File> expected = new ArrayList<File>();
		File currentDirectory = new File(Environment.currentDirectory);
		expected.add(new File(currentDirectory, "testGrepDir/1.txt"));
		expected.add(new File(currentDirectory, "testGrepDir/2.txt"));
		assertEquals(expected, grep.targetPaths);
	}

	@Test
	public void testProcessArgument3() throws GrepException {
		grep.processArguments("[0-9]{2,3}");
		assertEquals("[0-9]{2,3}", grep.pattern.toString());

		ArrayList<File> expected = new ArrayList<File>();
		assertEquals(expected, grep.targetPaths);
	}

	@Test(expected = GrepException.class)
	public void testProcessArguments4() throws GrepException {
		grep.processArguments("[0-9]{2,3}", "-m");
	}

	@Test
	public void testRun() throws GrepException {
		String[] args = { "[0-9]{2,3}", "testGrepDir/1.txt" };
		ByteArrayOutputStream stdout = new ByteArrayOutputStream();
		grep.run(args, null, stdout);
		assertEquals(
				"123" + System.lineSeparator() + "12345"
						+ System.lineSeparator() + "",
				new String(stdout.toByteArray()));
	}

	@Test
	public void testRunMultipleFiles() throws GrepException {
		String[] args = { "[0-9]{2,3}", "testGrepDir/1.txt",
				"testGrepDir/2.txt" };
		ByteArrayOutputStream stdout = new ByteArrayOutputStream();
		grep.run(args, null, stdout);
		assertEquals(
				"123" + System.lineSeparator() + "12345"
						+ System.lineSeparator() + "",
				new String(stdout.toByteArray()));
	}

	@Test
	public void testRunStdin() throws GrepException {

		String[] args = { "[0-9]{2,3}", "testGrepDir/1.txt",
				"testGrepDir/2.txt" };
		ByteArrayInputStream stdin = new ByteArrayInputStream(("123"
				+ System.lineSeparator() + "12345" + System.lineSeparator()
				+ "hello" + System.lineSeparator() + "").getBytes());

		ByteArrayOutputStream stdout = new ByteArrayOutputStream();
		grep.run(args, stdin, stdout);
		assertEquals(
				"123" + System.lineSeparator() + "12345"
						+ System.lineSeparator() + "",
				new String(stdout.toByteArray()));
	}

	@Test(expected = GrepException.class)
	public void testRunFail() throws GrepException {

		String[] args = { "-v" };
		ByteArrayInputStream stdin = new ByteArrayInputStream(("123"
				+ System.lineSeparator() + "12345" + System.lineSeparator()
				+ "hello" + System.lineSeparator()).getBytes());

		ByteArrayOutputStream stdout = new ByteArrayOutputStream();
		grep.run(args, stdin, stdout);
	}

}
