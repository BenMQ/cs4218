package test.sg.edu.nus.comp.cs4218.impl.cmd;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.exception.AbstractApplicationException;
import sg.edu.nus.comp.cs4218.exception.ShellException;
import sg.edu.nus.comp.cs4218.impl.app.CatApplication;
import sg.edu.nus.comp.cs4218.impl.app.CdApplication;
import sg.edu.nus.comp.cs4218.impl.app.EchoApplication;
import sg.edu.nus.comp.cs4218.impl.app.FindApplication;
import sg.edu.nus.comp.cs4218.impl.app.GrepApplication;
import sg.edu.nus.comp.cs4218.impl.app.HeadApplication;
import sg.edu.nus.comp.cs4218.impl.app.LsApplication;
import sg.edu.nus.comp.cs4218.impl.app.PwdApplication;
import sg.edu.nus.comp.cs4218.impl.app.SedApplication;
import sg.edu.nus.comp.cs4218.impl.app.TailApplication;
import sg.edu.nus.comp.cs4218.impl.app.WcApplication;
import sg.edu.nus.comp.cs4218.impl.cmd.AppThread;
import sg.edu.nus.comp.cs4218.impl.cmd.Call;

public class CallTest {

	private static Call call;

	private static File testDir;
	private static File folderDepth1;
	private static BufferedWriter writer;

	private final static String TEXT1_NO_NEWLINE = "123" + System.lineSeparator() + "abc"
			+ System.lineSeparator() + "456" + System.lineSeparator() + "def";
	private final static String TEXT1 = TEXT1_NO_NEWLINE
			+ System.lineSeparator() + "";
	private final static String TEXT2_NO_NEWLINE = "456" + System.lineSeparator() + "def"
			+ System.lineSeparator() + "123" + System.lineSeparator() + "abc";
	private final static String TEXT2 = TEXT2_NO_NEWLINE 
			+ System.lineSeparator() + "";

	@BeforeClass
	public static void setUp() throws Exception {
		testDir = new File("test_resources");
		testDir.mkdir();

		// file with content text1
		File testFile = new File(testDir, "fileA.txt");
		testFile.createNewFile();
		writer = new BufferedWriter(new FileWriter(testFile));
		writer.write(TEXT1);
		writer.close();

		// make folders in test_resources
		folderDepth1 = new File(testDir, "depth1");
		folderDepth1.mkdir();

		// file with content text2 in depth1 folder
		testFile = new File(folderDepth1, "fileB.txt");
		testFile.createNewFile();
		writer = new BufferedWriter(new FileWriter(testFile));
		writer.write(TEXT2);
		writer.close();
	}

	@Before
	public void instantiation() {
		Environment.currentDirectory = System.getProperty("user.dir");
	}

	@AfterClass
	public static void tearDown() throws Exception {
		for (File file : folderDepth1.listFiles()) {
			file.delete();
		}
		folderDepth1.delete();

		for (File file : testDir.listFiles()) {
			file.delete();
		}
		testDir.delete();

		call = null;
		writer = null;
	}

	@Test
	public void testRunCatApplication() throws AbstractApplicationException,
			ShellException, InterruptedException {
		CatApplication cat = new CatApplication();
		call = new Call(cat, new String[] { "." + File.separatorChar
				+ "test_resources" + File.separatorChar + "fileA.txt" });
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		call.evaluate(System.in, outputStream);
		Thread.sleep(100);
		assertNotNull(outputStream);
		assertEquals(TEXT1, outputStream.toString());
	}

	@Test
	public void testRunCdApplication() throws AbstractApplicationException,
			ShellException, InterruptedException {
		CdApplication cd = new CdApplication();
		call = new Call(cd, new String[] { "./test_resources" });
		call.evaluate(System.in, System.out);
		Thread.sleep(100);
		assertEquals(testDir.getAbsolutePath(), Environment.currentDirectory);
	}

	@Test
	public void testRunEchoApplication() throws AbstractApplicationException,
			ShellException, InterruptedException {
		EchoApplication echo = new EchoApplication();
		call = new Call(echo, new String[] { TEXT1 });
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		call.evaluate(System.in, outputStream);
		Thread.sleep(100);
		assertNotNull(outputStream);
		assertEquals(TEXT1 + System.lineSeparator(), outputStream.toString());
	}

	@Test
	public void testRunFindApplication() throws AbstractApplicationException,
			ShellException, InterruptedException {
		Environment.currentDirectory = System.getProperty("user.dir")
				+ "/test-files-ef2/";
		FindApplication find = new FindApplication();
		call = new Call(find, new String[] { "-name", "TEST.txt" });
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		call.evaluate(System.in, outputStream);
		Thread.sleep(100);
		assertNotNull(outputStream);
		String stdoutStr = outputStream.toString();
		int idx = stdoutStr.length() - 8 - System.lineSeparator().length();
		assertEquals("TEST.txt", stdoutStr.substring(idx).trim());
	}

	@Test
	public void testRunGrepApplication() throws AbstractApplicationException,
			ShellException, InterruptedException {
		GrepApplication grep = new GrepApplication();
		String[] args = { "[0-9]{2,3}" };
		call = new Call(grep, args);
		ByteArrayInputStream stdin = new ByteArrayInputStream(TEXT1.getBytes());
		ByteArrayOutputStream stdout = new ByteArrayOutputStream();
		call.evaluate(stdin, stdout);
		Thread.sleep(100);
		assertNotNull(stdout);
		assertEquals(
				"123" + System.lineSeparator() + "456" + System.lineSeparator()
						+ "", stdout.toString());
	}

	@Test
	public void testRunHeadApplication() throws AbstractApplicationException,
			ShellException, InterruptedException {
		HeadApplication head = new HeadApplication();
		call = new Call(head, new String[] { "./test_resources/fileA.txt" });
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		call.evaluate(System.in, outputStream);
		Thread.sleep(100);
		assertNotNull(outputStream);
		assertEquals(TEXT1_NO_NEWLINE, outputStream.toString());
	}

	@Test
	public void testRunLsApplication() throws AbstractApplicationException,
			ShellException, InterruptedException {
		LsApplication ls = new LsApplication();
		call = new Call(ls, new String[] { "./test_resources" });
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		call.evaluate(System.in, outputStream);
		Thread.sleep(100);
		assertNotNull(outputStream);
		assertEquals("depth1\tfileA.txt" + System.lineSeparator() + "",
				outputStream.toString());
	}

	@Test
	public void testRunPwdApplication() throws AbstractApplicationException,
			ShellException, InterruptedException {
		PwdApplication pwd = new PwdApplication();
		call = new Call(pwd, new String[] {});
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		call.evaluate(System.in, outputStream);
		Thread.sleep(100);
		assertNotNull(outputStream);
		assertEquals(Environment.currentDirectory + "" + System.lineSeparator()
				+ "", outputStream.toString());
	}

	@Test
	public void testRunSedApplication() throws AbstractApplicationException,
			ShellException, InterruptedException {
		SedApplication sed = new SedApplication();
		call = new Call(sed, new String[] { "s^1^a^",
				"./test_resources/fileA.txt" });
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		call.evaluate(System.in, outputStream);
		Thread.sleep(100);
		assertNotNull(outputStream);
		assertEquals(TEXT1.replaceAll("1", "a"), outputStream.toString());
	}

	@Test
	public void testRunTailApplication() throws AbstractApplicationException,
			ShellException, InterruptedException {
		TailApplication tail = new TailApplication();
		call = new Call(tail,
				new String[] { "./test_resources/depth1/fileB.txt" });
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		call.evaluate(System.in, outputStream);
		Thread.sleep(100);
		assertNotNull(outputStream);
		assertEquals(TEXT2_NO_NEWLINE, outputStream.toString());
	}

	@Test
	public void testRunWcApplication() throws AbstractApplicationException,
			ShellException, InterruptedException {
		String expectedOutput = "4\t4\t16" + System.lineSeparator();
		String winExpectedOutput = "4\t4\t20" + System.lineSeparator();
		WcApplication wc = new WcApplication();
		call = new Call(wc, new String[] { "./test_resources/fileA.txt" });
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		call.evaluate(System.in, outputStream);
		Thread.sleep(100);
		assertNotNull(outputStream);
		if (Environment.IS_WINDOWS) {
			assertEquals(winExpectedOutput, outputStream.toString());
		} else {
			assertEquals(expectedOutput, outputStream.toString());
		}
	}

	@Test
	public void testRunning() throws AbstractApplicationException,
			ShellException, InterruptedException {
		WcApplication wc = new WcApplication();
		call = new Call(wc, new String[] { "./test_resources/fileA.txt" });
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		call.evaluate(System.in, outputStream);
		Thread.sleep(100);
		assertTrue(call.isDone());
	}

	@Test
	public void testTerminate() throws AbstractApplicationException,
			ShellException, InterruptedException {
		WcApplication wc = new WcApplication();
		call = new Call(wc, new String[] { "./test_resources/fileA.txt" });
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		call.evaluate(System.in, outputStream);
		call.terminate();
		assertTrue(call.isDone());
	}

	@Test
	public void testInputStream() throws AbstractApplicationException,
			ShellException, InterruptedException {
		WcApplication wc = new WcApplication();
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		call = new Call(wc, new String[] { "./test_resources/fileA.txt" });
		call.evaluate(System.in, outputStream);
		assertNotNull(call.getInputStream());
	}

	@Test
	public void testOutputStream() throws AbstractApplicationException,
			ShellException, InterruptedException {
		WcApplication wc = new WcApplication();
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		call = new Call(wc, new String[] { "./test_resources/fileA.txt" });
		call.evaluate(System.in, outputStream);
		assertNotNull(call.getOutputStream());
	}

	@Test
	public void testAppThread() throws AbstractApplicationException,
			ShellException, InterruptedException {
		WcApplication wc = new WcApplication();
		call = new Call(wc, new String[] { "./test_resources/fileA.txt" });
		AppThread thread = call.getAppThread();
		assertNull(thread);
	}
}
