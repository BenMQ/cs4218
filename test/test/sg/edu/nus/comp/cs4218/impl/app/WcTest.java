package test.sg.edu.nus.comp.cs4218.impl.app;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.exception.WcException;
import sg.edu.nus.comp.cs4218.impl.app.WcApplication;

public class WcTest {

	private static WcApplication wcApp = null;
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

	static private File testDir = new File("testDir");

	@BeforeClass
	public static void setup() throws IOException {
		testDir.mkdir();
		File testFile = new File(testDir, "1.txt");
		testFile.createNewFile();
		BufferedWriter writer = new BufferedWriter(new FileWriter(testFile));
		writer.write("1");
		writer.close();

		testFile = new File(testDir, "2.txt");
		testFile.createNewFile();
		writer = new BufferedWriter(new FileWriter(testFile));
		writer.write("2");
		writer.close();
	}

	@AfterClass
	public static void tearDown() {
		for (File file : testDir.listFiles()) {
			file.delete();
		}
		testDir.delete();
	}

	@Before
	public void before() throws Exception {
		wcApp = new WcApplication();
	}

	@After
	public void after() throws Exception {
		wcApp = null;
	}

	@Test
	public void testSingleLineStandardInput() {
		String testString = "12345678";
		InputStream inputStream = new ByteArrayInputStream(
				testString.getBytes(StandardCharsets.UTF_8));
		try {
			wcApp.run(null, inputStream, outContent);
		} catch (WcException e) {
			fail(e.getMessage());
		}
		assertEquals("1\t1\t8" + System.lineSeparator(), outContent.toString());
	}

	@Test
	public void testMultipleLineStandardInput() throws IOException {
		String testString = "12345678\n1234 5678";
		InputStream inputStream = new ByteArrayInputStream(
				testString.getBytes(StandardCharsets.UTF_8));

		try {
			wcApp.run(null, inputStream, outContent);
		} catch (WcException e) {
			fail(e.getMessage());
		}
		assertEquals("2\t3\t18" + System.lineSeparator(), outContent.toString());
	}

	@Test
	public void testFiles() throws IOException {
		try {
			wcApp.run(new String[] { "testDir/1.txt", "testDir/2.txt" }, null,
					outContent);
		} catch (WcException e) {
			fail(e.getMessage());
		}
		assertEquals(
				"1\t1\t1" + System.lineSeparator() + "1\t1\t1"
						+ System.lineSeparator() + "2\t2\t2\ttotal"
						+ System.lineSeparator(), outContent.toString());
	}

	@Test(expected = WcException.class)
	public void testNullInput() throws WcException {
		wcApp.run(null, null, outContent);
	}

}
