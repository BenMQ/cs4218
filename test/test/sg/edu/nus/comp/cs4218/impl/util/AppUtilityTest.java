package test.sg.edu.nus.comp.cs4218.impl.util;

import static org.junit.Assert.assertEquals;

import java.io.File;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.impl.util.AppUtility;

public class AppUtilityTest {

	private static String oldCWD;

	@Before
	public void beforeTestBackupCWD() {
		oldCWD = Environment.currentDirectory;
	}

	@After
	public void afterTestRestoreCWD() {
		Environment.currentDirectory = oldCWD;
	}

	@Test
	public void testGetCWD() {
		assertEquals(Environment.currentDirectory, AppUtility.getCWD());
	}

	@Test
	public void testGetCWD2() {
		Environment.currentDirectory = "Test";
		assertEquals(Environment.currentDirectory, AppUtility.getCWD());
	}

	@Test
	public void testConvertToAbsPath() {
		String path = "~";
		assertEquals(Environment.HOME_DIRECTORY,
				AppUtility.convertToAbsPath(path));
	}

	@Test
	public void testConvertToAbsPath2() {
		String path = ".";
		assertEquals(Environment.currentDirectory,
				AppUtility.convertToAbsPath(path));
	}

	@Test
	public void testConvertToAbsPath3() {
		String path = "*";
		assertEquals(Environment.currentDirectory + File.separatorChar + "*",
				AppUtility.convertToAbsPath(path));
	}

	@Test
	public void testIsValidPath() {
		String path = ".";
		assertEquals(true, AppUtility.isValidPath(path));
	}

	@Test
	public void testIsValidPath2() {
		String path = "~";
		assertEquals(true, AppUtility.isValidPath(path));
	}

	@Test
	public void testIsValidPath3() {
		String path = Environment.HOME_DIRECTORY;
		assertEquals(true, AppUtility.isValidPath(path));
	}

	@Test
	public void testIsValidPath4() {
		String path = Environment.currentDirectory;
		assertEquals(true, AppUtility.isValidPath(path));
	}

	@Test
	public void testIsValidPath5() {
		String path = "/";
		assertEquals(true, AppUtility.isValidPath(path));
	}

}
