package test.sg.edu.nus.comp.cs4218.impl.app;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.nio.charset.StandardCharsets;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.exception.TailException;
import sg.edu.nus.comp.cs4218.impl.app.TailApplication;

public class TailTest {

	private static TailApplication tailApp = null;
	private static ByteArrayOutputStream outContent = null;
	private static String testString = null;
	private static ByteArrayInputStream inputStream = null;

	static private File testDir = new File("test_resources");
	private static final String TEST_FILE_1 = "test_resources/5.txt";

	@Before
	public void setUp() throws Exception {
		testDir.mkdir();
		File testFile = new File(testDir, "5.txt");
		testFile.createNewFile();
		BufferedWriter writer = new BufferedWriter(new FileWriter(testFile));
		for (int i = 0; i < 5; i++) {
			writer.write(String.valueOf(i + 1));
			writer.write(System.lineSeparator());
		}
		writer.close();

		testFile = new File(testDir, "20.txt");
		testFile.createNewFile();
		writer = new BufferedWriter(new FileWriter(testFile));
		for (int i = 0; i < 20; i++) {
			writer.write(String.valueOf(i + 1));
			writer.write(System.lineSeparator());
		}
		writer.close();

		tailApp = new TailApplication();
		testString = "a" + System.lineSeparator() + "b";
		inputStream = new ByteArrayInputStream(
				testString.getBytes(StandardCharsets.UTF_8));
		outContent = new ByteArrayOutputStream();
	}

	@After
	public void tearDown() throws Exception {
		for (File file : testDir.listFiles()) {
			file.delete();
		}
		testDir.delete();

		tailApp = null;
		testString = null;
		inputStream = null;
		outContent = null;
	}

	/*
	 * Zero Argument Tests
	 */
	@Test(expected = TailException.class)
	public void testNoArgs() throws TailException {
		tailApp.run(null, null, outContent);
	}

	@Test
	public void testStandardInput() {
		try {
			tailApp.run(null, inputStream, outContent);
		} catch (TailException e) {
			fail(e.getMessage());
		}
		assertEquals(testString, outContent.toString());
	}

	/*
	 * One Argument Tests
	 */
	@Test(expected = TailException.class)
	public void testEmptyArg() throws TailException {
		tailApp.run(new String[] { " " }, null, outContent);
	}

	@Test(expected = TailException.class)
	public void testInvalidLineOption() throws TailException {
		tailApp.run(new String[] { "-n" }, null, outContent);
	}

	@Test(expected = TailException.class)
	public void testDir() throws TailException {
		tailApp.run(new String[] { "test_resources" }, null, outContent);
	}

	@Test(expected = TailException.class)
	public void testOneInvalidFile() throws TailException {
		tailApp.run(
				new String[] { "test_resources/7815696ecbf1c96e6894b779456d330e" },
				null, outContent);
	}

	@Test
	public void testOneFile20Line() throws TailException {
		testString = "";
		for (int i = 11; i <= 20; i++) {
			testString += i;
			if (i != 20) {
				testString += System.lineSeparator();
			}
		}
		tailApp.run(new String[] { "test_resources/20.txt" }, null, outContent);
		assertEquals(testString, outContent.toString());
	}

	@Test
	public void testOneFile5Line() throws TailException {
		testString = "";
		for (int i = 1; i <= 5; i++) {
			testString += i;
			if (i != 5) {
				testString += System.lineSeparator();
			}
		}
		tailApp.run(new String[] { TEST_FILE_1 }, null, outContent);
		assertEquals(testString, outContent.toString());
	}

	/*
	 * Two Argument Tests
	 */
	@Test(expected = TailException.class)
	public void testEmpty2Args() throws TailException {
		tailApp.run(new String[] { " ", "    " }, null, outContent);
	}

	@Test(expected = TailException.class)
	public void testInvalidSyntax() throws TailException {
		testString = "1" + System.lineSeparator();
		tailApp.run(new String[] { TEST_FILE_1, "-n" }, null, outContent);
		assertEquals(testString, outContent.toString());
	}

	@Test(expected = TailException.class)
	public void testInvalidSyntax2() throws TailException {
		testString = "1" + System.lineSeparator();
		tailApp.run(new String[] { "-n", TEST_FILE_1 }, null, outContent);
		assertEquals(testString, outContent.toString());
	}

	@Test(expected = TailException.class)
	public void testInvalidLineNum() throws TailException {
		testString = "1" + System.lineSeparator();
		tailApp.run(new String[] { "-n", "-99" }, null, outContent);
		assertEquals(testString, outContent.toString());
	}

	@Test(expected = TailException.class)
	public void testInvalidLineNum2() throws TailException {
		testString = "1" + System.lineSeparator();
		tailApp.run(new String[] { "-n", "*abc" }, null, outContent);
		assertEquals(testString, outContent.toString());
	}

	@Test
	public void testValid1Line() throws TailException {
		testString = "b";
		tailApp.run(new String[] { "-n", "1" }, inputStream, outContent);
		assertEquals(testString, outContent.toString());
	}

	@Test
	public void test999Lines() throws TailException {
		tailApp.run(new String[] { "-n", "999" }, inputStream, outContent);
		assertEquals(testString, outContent.toString());
	}

	@Test
	public void testZeroLines() {
		String[] args = { "-n", "0" };
		try {
			tailApp.run(args, inputStream, outContent);
		} catch (TailException e) {
			fail(e.getMessage());
		}
		assertEquals("", outContent.toString());
	}

	/*
	 * Three Argument Tests
	 */
	@Test(expected = TailException.class)
	public void testEmpty3Args() throws TailException {
		tailApp.run(new String[] { "", "", "" }, null, outContent);
	}

	@Test(expected = TailException.class)
	public void testInvalidSyntax3() throws TailException {
		testString = "1" + System.lineSeparator();
		tailApp.run(new String[] { TEST_FILE_1, "-n", "1" }, null, outContent);
		assertEquals(testString, outContent.toString());
	}

	@Test
	public void test3ValidArgs() throws TailException {
		testString = "5";
		tailApp.run(new String[] { "-n", "1", TEST_FILE_1 }, null, outContent);
		assertEquals(testString, outContent.toString());
	}

	@Test(expected = TailException.class)
	public void test3ArgsDir() throws TailException {
		testString = "1" + System.lineSeparator();
		tailApp.run(new String[] { "-n", "1", "test_resources" }, null,
				outContent);
		assertEquals(testString, outContent.toString());
	}

}
