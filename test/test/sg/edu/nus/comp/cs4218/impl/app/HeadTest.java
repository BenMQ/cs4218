package test.sg.edu.nus.comp.cs4218.impl.app;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.nio.charset.StandardCharsets;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.exception.HeadException;
import sg.edu.nus.comp.cs4218.impl.app.HeadApplication;

public class HeadTest {

	private static HeadApplication headApp = null;
	private static ByteArrayOutputStream outContent = null;
	private static String testString = null;
	private static ByteArrayInputStream inputStream = null;

	static private File testDir = new File("test_resources");
	private static final String TEST_FILE_1 = "test_resources/5.txt";

	@Before
	public void setUp() throws Exception {
		testDir.mkdir();
		File testFile = new File(testDir, "5.txt");
		testFile.createNewFile();
		BufferedWriter writer = new BufferedWriter(new FileWriter(testFile));
		for (int i = 0; i < 5; i++) {
			writer.write(String.valueOf(i + 1));
			writer.write(System.lineSeparator());
		}
		writer.close();

		testFile = new File(testDir, "20.txt");
		testFile.createNewFile();
		writer = new BufferedWriter(new FileWriter(testFile));
		for (int i = 0; i < 20; i++) {
			writer.write(String.valueOf(i + 1));
			writer.write(System.lineSeparator());
		}
		writer.close();

		headApp = new HeadApplication();
		testString = "a" + System.lineSeparator() + "b";
		inputStream = new ByteArrayInputStream(
				testString.getBytes(StandardCharsets.UTF_8));
		outContent = new ByteArrayOutputStream();

	}

	@After
	public void tearDown() throws Exception {
		for (File file : testDir.listFiles()) {
			file.delete();
		}
		testDir.delete();

		headApp = null;
		testString = null;
		inputStream = null;
		outContent = null;
	}

	/*
	 * Zero Argument Tests
	 */
	@Test(expected = HeadException.class)
	public void testNoArgs() throws HeadException {
		headApp.run(null, null, outContent);
	}

	@Test
	public void testStandardInput() {
		try {
			headApp.run(null, inputStream, outContent);
		} catch (HeadException e) {
			fail(e.getMessage());
		}
		assertEquals(testString, outContent.toString());
	}

	/*
	 * One Argument Tests
	 */
	@Test(expected = HeadException.class)
	public void testEmptyArg() throws HeadException {
		headApp.run(new String[] { " " }, null, outContent);
	}

	@Test(expected = HeadException.class)
	public void testInvalidLineOption() throws HeadException {
		headApp.run(new String[] { "-n" }, null, outContent);
	}

	@Test(expected = HeadException.class)
	public void testDir() throws HeadException {
		headApp.run(new String[] { "test_resources" }, null, outContent);
	}

	@Test(expected = HeadException.class)
	public void testOneInvalidFile() throws HeadException {
		headApp.run(
				new String[] { "test_resources/7815696ecbf1c96e6894b779456d330e" },
				null, outContent);
	}

	@Test
	public void testOneFile20Line() throws HeadException {
		testString = "";
		for (int i = 1; i <= 10; i++) {
			testString += i + System.lineSeparator();
		}
		headApp.run(new String[] { "test_resources/20.txt" }, null, outContent);
		assertEquals(testString, outContent.toString());
	}

	@Test
	public void testOneFile5Line() throws HeadException {
		testString = "";
		for (int i = 1; i <= 5; i++) {
			testString += i;
			if (i < 5) {
				testString += System.lineSeparator();
			}
		}
		headApp.run(new String[] { TEST_FILE_1 }, null, outContent);
		assertEquals(testString, outContent.toString());
	}

	/*
	 * Two Argument Tests
	 */
	@Test(expected = HeadException.class)
	public void testEmpty2Args() throws HeadException {
		headApp.run(new String[] { " ", "    " }, null, outContent);
	}

	@Test(expected = HeadException.class)
	public void testInvalidSyntax() throws HeadException {
		testString = "1" + System.lineSeparator();
		headApp.run(new String[] { TEST_FILE_1, "-n" }, null, outContent);
		assertEquals(testString, outContent.toString());
	}

	@Test(expected = HeadException.class)
	public void testInvalidSyntax2() throws HeadException {
		testString = "1" + System.lineSeparator();
		headApp.run(new String[] { "-n", TEST_FILE_1 }, null, outContent);
		assertEquals(testString, outContent.toString());
	}

	@Test(expected = HeadException.class)
	public void testInvalidLineNum() throws HeadException {
		testString = "1" + System.lineSeparator();
		headApp.run(new String[] { "-n", "-99" }, null, outContent);
		assertEquals(testString, outContent.toString());
	}

	@Test(expected = HeadException.class)
	public void testInvalidLineNum2() throws HeadException {
		testString = "1" + System.lineSeparator();
		headApp.run(new String[] { "-n", "*abc" }, null, outContent);
		assertEquals(testString, outContent.toString());
	}

	@Test
	public void testValid1Line() throws HeadException {
		testString = "a" + System.lineSeparator();
		headApp.run(new String[] { "-n", "1" }, inputStream, outContent);
		assertEquals(testString, outContent.toString());
	}

	@Test
	public void test999Lines() throws HeadException {
		headApp.run(new String[] { "-n", "999" }, inputStream, outContent);
		assertEquals(testString, outContent.toString());
	}

	@Test
	public void testZeroLines() {
		String[] args = { "-n", "0" };
		try {
			headApp.run(args, inputStream, outContent);
		} catch (HeadException e) {
			fail(e.getMessage());
		}
		assertEquals("", outContent.toString());
	}

	/*
	 * Three Argument Tests
	 */
	@Test(expected = HeadException.class)
	public void testEmpty3Args() throws HeadException {
		headApp.run(new String[] { "", "", "" }, null, outContent);
	}

	@Test(expected = HeadException.class)
	public void testInvalidSyntax3() throws HeadException {
		testString = "1" + System.lineSeparator();
		headApp.run(new String[] { TEST_FILE_1, "-n", "1" }, null, outContent);
		assertEquals(testString, outContent.toString());
	}

	@Test
	public void test3ValidArgs() throws HeadException {
		testString = "1" + System.lineSeparator();
		headApp.run(new String[] { "-n", "1", TEST_FILE_1 }, null, outContent);
		assertEquals(testString, outContent.toString());
	}

	@Test(expected = HeadException.class)
	public void test3ArgsDir() throws HeadException {
		testString = "1" + System.lineSeparator();
		headApp.run(new String[] { "-n", "1", "test_resources" }, null,
				outContent);
		assertEquals(testString, outContent.toString());
	}

}
