package test.sg.edu.nus.comp.cs4218.impl.cmd;

import static org.junit.Assert.assertNull;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import org.junit.Test;

import sg.edu.nus.comp.cs4218.exception.AbstractApplicationException;
import sg.edu.nus.comp.cs4218.exception.ShellException;
import sg.edu.nus.comp.cs4218.impl.app.PwdApplication;
import sg.edu.nus.comp.cs4218.impl.cmd.Call;
import sg.edu.nus.comp.cs4218.impl.cmd.Pipe;

public class PipeTest {
	@Test
	public void testPipe() throws AbstractApplicationException, ShellException {
		Call call1 = new Call(new PwdApplication());
		Call call2 = new Call(new PwdApplication());
		Pipe pipe = new Pipe(call1, call2);
		pipe.evaluate(new ByteArrayInputStream(new byte[] {}),
				new ByteArrayOutputStream());
		pipe.terminate(); // should not throw error
	}

	@Test
	public void testgetStdout() throws AbstractApplicationException,
			ShellException {
		Call call1 = new Call(new PwdApplication());
		Call call2 = new Call(new PwdApplication());
		Pipe pipe = new Pipe(call1, call2);
		assertNull(pipe.getStdOut());
	}
}
