package test.sg.edu.nus.comp.cs4218.impl;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import sg.edu.nus.comp.cs4218.ext2.FindApplicationTest;
import sg.edu.nus.comp.cs4218.ext2.IORedirectionTest;
import sg.edu.nus.comp.cs4218.ext2.SubstitutionTest;
import sg.edu.nus.comp.cs4218.ext2.WcApplicationTest;

@RunWith(Suite.class)
@SuiteClasses({ FindApplicationTest.class, IORedirectionTest.class,
		SubstitutionTest.class, WcApplicationTest.class })
public class TestEF2TDDSuite {

}
