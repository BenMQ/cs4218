package test.sg.edu.nus.comp.cs4218.impl;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import test.sg.edu.nus.comp.cs4218.coverage.CoverageTest;
import test.sg.edu.nus.comp.cs4218.hackathonfix.AppHacksFixes;

@RunWith(value = org.junit.runners.Suite.class)
@Suite.SuiteClasses(value = { AppTestSuite.class, CmdTestSuite.class,
		UtilTestSuite.class, M2TestSuite.class, TestEF2TDDSuite.class, CoverageTest.class, AppHacksFixes.class })
public class AllTest {

}
