package test.sg.edu.nus.comp.cs4218.impl;

import test.sg.edu.nus.comp.cs4218.impl.app.*;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(value = org.junit.runners.Suite.class)
@Suite.SuiteClasses(value = { GrepTest.class, TailTest.class, HeadTest.class,
		LsTest.class, PwdTest.class, SedTest.class, CatTest.class,
		CdTest.class, EchoTest.class, FindTest.class, WcTest.class })
public class AppTestSuite {
}
