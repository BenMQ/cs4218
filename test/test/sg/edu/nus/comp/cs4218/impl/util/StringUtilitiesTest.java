package test.sg.edu.nus.comp.cs4218.impl.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import sg.edu.nus.comp.cs4218.impl.util.StringUtilities;

public class StringUtilitiesTest {

	@Test
	public void testJoinArrayEmptyArray() {
		String[] input = new String[0];
		assertEquals("", StringUtilities.joinArray(input, ","));
	}

	@Test
	public void testJoinArrayEmptyArrayEmptyDelimiter() {
		String[] input = new String[0];
		assertEquals("", StringUtilities.joinArray(input, ""));
	}

	@Test
	public void testJoinArrayOneElement() {
		String[] input = { "1" };
		assertEquals("1", StringUtilities.joinArray(input, ","));
	}

	@Test
	public void testJoinArrayTwoElement() {
		String[] input = { "1", "2" };
		assertEquals("1,2", StringUtilities.joinArray(input, ","));
	}

	@Test
	public void testJoinArrayManyElements() {
		String[] input = { "1", "2", "3", "4" };
		assertEquals("1,2,3,4", StringUtilities.joinArray(input, ","));
	}

	@Test
	public void testJoinArrayManyElementsEmptyDelimiter() {
		String[] input = { "1", "2", "3", "4" };
		assertEquals("1234", StringUtilities.joinArray(input, ""));
	}

	@Test
	public void testJoinArrayManyElementsLongDelimiter() {
		String[] input = { "1", "2", "3", "4" };
		assertEquals("1xxx2xxx3xxx4", StringUtilities.joinArray(input, "xxx"));
	}

	@Test
	public void testJoinArrayNull() {
		String[] input = null;
		assertEquals(null, StringUtilities.joinArray(input, "xxx"));
	}

}
