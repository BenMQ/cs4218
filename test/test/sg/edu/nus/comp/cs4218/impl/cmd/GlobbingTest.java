package test.sg.edu.nus.comp.cs4218.impl.cmd;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.exception.AbstractApplicationException;
import sg.edu.nus.comp.cs4218.exception.ShellException;
import sg.edu.nus.comp.cs4218.impl.cmd.Evaluator;
import sg.edu.nus.comp.cs4218.impl.util.AppUtility;

public class GlobbingTest {

	private static Evaluator evaluator;

	@BeforeClass
	public static void setupGlobTest() throws IOException {
		File testDir = new File("testGlobDir");
		testDir.mkdir();
		String[] files = { "test.java", "foobar.java", "foo.java" };
		File testFile;
		for (String filename : files) {
			testFile = new File(testDir, filename);
			testFile.createNewFile();
		}
		File testSubDir = new File(testDir, "testSubDir/testSubSubDir");
		testSubDir.mkdirs();
		for (String filename : files) {
			testFile = new File(testSubDir, filename);
			testFile.createNewFile();
		}

		File testSubDir2 = new File(testDir, "testSubDir2/testSubSubDir");
		testSubDir2.mkdirs();
		for (String filename : files) {
			testFile = new File(testSubDir2, filename);
			testFile.createNewFile();
		}
	}

	@Before
	public void setUp() {
		evaluator = new Evaluator();
		Environment.currentDirectory = System.getProperty("user.dir");
	}

	@Test
	public void testGlobbingWithOne() throws ShellException, IOException {
		Set<String> expected = new HashSet<String>();
		expected.add("testGlobDir" + File.separator + "test.java");
		expected.add("testGlobDir" + File.separator + "foobar.java");
		expected.add("testGlobDir" + File.separator + "foo.java");
		expected = buildExpected(expected);

		List<String> actual = evaluator.glob("testGlobDir" + File.separator
				+ "*.java");
		assertEquals(expected, new HashSet<String>(actual));
	}

	@Test
	public void testGlobbingWithBare() throws ShellException, IOException {
		Set<String> expected = new HashSet<String>();
		Environment.currentDirectory = new File(Environment.currentDirectory,
				"testGlobDir").getAbsolutePath();
		expected.add("test.java");
		expected.add("foobar.java");
		expected.add("foo.java");
		expected = buildExpected(expected);

		List<String> actual = evaluator.glob("*.java");
		assertEquals(expected, new HashSet<String>(actual));
	}

	@Test
	public void testGlobbingWithBareInSubDirectory() throws ShellException,
			IOException {
		Set<String> expected = new HashSet<String>();
		Environment.currentDirectory = new File(Environment.currentDirectory,
				"testGlobDir/testSubDir/testSubSubDir").getAbsolutePath();
		expected.add("test.java");
		expected.add("foobar.java");
		expected.add("foo.java");
		expected = buildExpected(expected);

		List<String> actual = evaluator.glob("*.java");
		assertEquals(expected, new HashSet<String>(actual));
	}

	@Test
	public void testGlobbingWithPartial() throws ShellException, IOException {
		Set<String> expected = new HashSet<String>();
		Environment.currentDirectory = new File(Environment.currentDirectory,
				"testGlobDir").getAbsolutePath();
		expected.add("testSubDir" + File.separatorChar + "testSubSubDir"
				+ File.separatorChar + "test.java");
		expected.add("testSubDir2" + File.separatorChar + "testSubSubDir"
				+ File.separatorChar + "test.java");
		expected = buildExpected(expected);

		List<String> actual = evaluator.glob("testSubDir*" + File.separatorChar
				+ "testSubSubDir" + File.separatorChar + "test.java");
		// System.out.println(AppUtility.convertToAbsPath("testSubDir*"+File.separatorChar+"testSubSubDir"+File.separatorChar+"test.java"));
		assertEquals(expected, new HashSet<String>(actual));
	}

	@Test
	public void testGlobbingWithMultiple() throws ShellException, IOException {
		Set<String> expected = new HashSet<String>();
		Environment.currentDirectory = new File(Environment.currentDirectory,
				"testGlobDir").getAbsolutePath();
		expected.add("testSubDir/testSubSubDir/test.java");
		expected.add("testSubDir2/testSubSubDir/test.java");
		expected = buildExpected(expected);

		List<String> actual = evaluator.glob("*" + File.separatorChar + "*"
				+ File.separatorChar + "test.java");
		assertEquals(expected, new HashSet<String>(actual));
	}

	@Test
	public void testTokenizeWithGlobbing() throws ShellException,
			AbstractApplicationException {
		List<String> expected = new ArrayList<String>(Arrays.asList("foo"));
		expected.add(new File(
				AppUtility
						.convertToAbsPath("testGlobDir/testSubDir/testSubSubDir/test.java"))
				.getAbsolutePath());
		List<String> actual = evaluator.tokenize("foo testGlobDir"
				+ File.separatorChar + "testSubDir" + File.separatorChar + "*"
				+ File.separatorChar + "test.java");
		assertEquals(expected, actual);
	}

	@Test
	public void testTokenizeWithGlobbingQuoted() throws ShellException,
			AbstractApplicationException {
		List<String> expected = new ArrayList<String>(Arrays.asList("foo",
				"testGlobDir/testSubDir/*/test.java"));
		List<String> actual = evaluator
				.tokenize("foo \"testGlobDir/testSubDir/*/test.java\"");
		assertEquals(expected, actual);
	}

	private Set<String> buildExpected(Set<String> files) {
		Set<String> expected = new HashSet<String>();
		for (String file : files) {
			expected.add(new File(AppUtility.convertToAbsPath(file))
					.getAbsolutePath());
		}
		return expected;
	}

	@AfterClass
	public static void tearDownGlobTest() throws IOException {
		File testDir = new File("testGlobDir");
		delete(testDir.toPath());
	}

	public static void delete(Path path) throws IOException {
		Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
			@Override
			public FileVisitResult visitFile(Path file,
					BasicFileAttributes attrs) throws IOException {
				Files.delete(file);
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult visitFileFailed(Path file, IOException exc)
					throws IOException {
				Files.delete(file);
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult postVisitDirectory(Path dir, IOException exc)
					throws IOException {
				if (exc == null) {
					Files.delete(dir);
					return FileVisitResult.CONTINUE;
				} else {
					throw exc;
				}
			}
		});
	}
}
