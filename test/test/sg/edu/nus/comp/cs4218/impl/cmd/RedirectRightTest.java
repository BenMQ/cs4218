package test.sg.edu.nus.comp.cs4218.impl.cmd;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.exception.AbstractApplicationException;
import sg.edu.nus.comp.cs4218.exception.ShellException;
import sg.edu.nus.comp.cs4218.impl.app.PwdApplication;
import sg.edu.nus.comp.cs4218.impl.cmd.Call;
import sg.edu.nus.comp.cs4218.impl.cmd.RedirectRight;

public class RedirectRightTest {
	static private File testDir = new File("testDir");

	@BeforeClass
	public static void setup() throws IOException {
		testDir.mkdir();
		File testFile = new File(testDir, "1.txt");
		testFile.createNewFile();
	}

	@AfterClass
	public static void tearDown() {
		for (File file : testDir.listFiles()) {
			file.delete();
		}
		testDir.delete();

		// reset back to the user dir so that other test class
		// will not being affected by the modification
		Environment.currentDirectory = System.getProperty("user.dir");
	}

	@Test
	public void testRedirectRight() throws AbstractApplicationException,
			ShellException {
		Call call1 = new Call(new PwdApplication());

		RedirectRight redirect = new RedirectRight(call1, "testDir/1.txt");
		redirect.evaluate(new ByteArrayInputStream(new byte[] {}),
				new ByteArrayOutputStream());
		redirect.terminate(); // should not throw
	}
}
