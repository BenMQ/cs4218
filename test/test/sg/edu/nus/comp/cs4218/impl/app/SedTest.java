package test.sg.edu.nus.comp.cs4218.impl.app;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.exception.SedException;
import sg.edu.nus.comp.cs4218.impl.app.SedApplication;

public class SedTest {

	private static SedApplication sedApp = null;
	private static ByteArrayOutputStream outContent = null;
	private static String testString = null;
	private static ByteArrayInputStream inputStream = null;

	@Before
	public void setUp() throws Exception {
		sedApp = new SedApplication();
		outContent = new ByteArrayOutputStream();
		testString = "a" + System.lineSeparator() + "b"
				+ System.lineSeparator();
		inputStream = new ByteArrayInputStream(
				testString.getBytes(StandardCharsets.UTF_8));

	}

	@After
	public void tearDown() throws Exception {
		sedApp = null;
		testString = null;
		inputStream = null;
		outContent = null;
	}

	/*
	 * Zero Arguments Tests
	 */

	@Test(expected = SedException.class)
	public void testNullStdout() throws SedException {
		sedApp.run(null, inputStream, null);
	}

	@Test(expected = SedException.class)
	public void testNullStdin() throws SedException {
		sedApp.run(null, null, outContent);
	}

	@Test(expected = SedException.class)
	public void testNoArg() throws SedException {
		sedApp.run(new String[] {}, inputStream, outContent);
	}

	/*
	 * One Argument Tests
	 */
	@Test
	public void testOneArg() throws SedException {
		sedApp.run(new String[] { "s/b/a/" }, inputStream, outContent);
		String outcome = testString.replaceAll("b", "a");
		assertEquals(outcome, outContent.toString());
	}

	@Test
	public void testOneArg2() throws SedException {
		sedApp.run(new String[] { "sBbBaB" }, inputStream, outContent);
		String outcome = testString.replaceAll("b", "a");
		assertEquals(outcome, outContent.toString());
	}

	@Test
	public void testOneArg3() throws SedException {
		sedApp.run(new String[] { "s b a " }, inputStream, outContent);
		String outcome = testString.replaceAll("b", "a");
		assertEquals(outcome, outContent.toString());
	}

	@Test(expected = SedException.class)
	public void testOneArg4() throws SedException {
		sedApp.run(new String[] { "sBBbBBaBB" }, inputStream, outContent);
		String outcome = testString.replaceAll("b", "a");
		assertEquals(outcome, outContent.toString());
	}

	/*
	 * Test Regex Special Characters
	 */
	@Test
	public void testRegexChar() throws SedException {
		sedApp.run(new String[] { "s\\b\\a\\" }, inputStream, outContent);
		String outcome = testString.replaceAll("b", "a");
		assertEquals(outcome, outContent.toString());
	}

	@Test
	public void testRegexChar2() throws SedException {
		sedApp.run(new String[] { "s^b^a^" }, inputStream, outContent);
		String outcome = testString.replaceAll("b", "a");
		assertEquals(outcome, outContent.toString());
	}

	@Test
	public void testRegexChar3() throws SedException {
		sedApp.run(new String[] { "s{b{a{" }, inputStream, outContent);
		String outcome = testString.replaceAll("b", "a");
		assertEquals(outcome, outContent.toString());
	}

}
