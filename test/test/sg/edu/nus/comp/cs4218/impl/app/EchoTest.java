package test.sg.edu.nus.comp.cs4218.impl.app;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.exception.EchoException;
import sg.edu.nus.comp.cs4218.impl.app.EchoApplication;

public final class EchoTest {

	private static EchoApplication echo;

	@BeforeClass
	public static void setUp() {
		echo = new EchoApplication();
	}

	@AfterClass
	public static void tearDown() {

		echo = null;
	}

	/**
	 * Boundary and Exception
	 */
	@Test
	public void testNoArgument() throws EchoException {
		String[] cmd = {};
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		echo.run(cmd, System.in, outputStream);
		assertEquals(System.lineSeparator(), outputStream.toString());
	}

	@Test(expected = EchoException.class)
	public void testNullCommand() throws EchoException {
		String[] cmd = null;
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		echo.run(cmd, System.in, outputStream);
	}

	@Test(expected = EchoException.class)
	public void testNullOutputStream() throws EchoException {
		String[] args = { "echo" };
		echo.run(args, System.in, null);
	}

	/**
	 * Positive test
	 */
	@Test
	public void testNullInputStream() throws EchoException {
		String[] cmd = { "" };
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		echo.run(cmd, null, outputStream);
		assertEquals("There is one new line printed", System.lineSeparator(),
				outputStream.toString());
	}

	@Test
	public void testRunSingleInputArgument() throws EchoException {
		String[] cmd = { "test123" };
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		echo.run(cmd, null, outputStream);
		assertEquals("The output stream are different",
				"test123" + System.lineSeparator(), outputStream.toString());
	}

	@Test
	public void testRunMultipleInputArgument() throws EchoException {
		String[] cmd = { "test123", "abc123" };
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		echo.run(cmd, null, outputStream);
		assertEquals("The output stream are different", "test123 abc123"
				+ System.lineSeparator(), outputStream.toString());
	}

	/**
	 * Special tests
	 */
	@Test
	public void testRunWithSpecialCharater1() throws EchoException {
		String[] cmd = { "test123", "abc\n123" };
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		echo.run(cmd, null, outputStream);
		assertEquals("The output stream are different", "test123 abc\n123" + System.lineSeparator(),
				outputStream.toString());
	}

	@Test
	public void testRunWithSpecialCharater2() throws EchoException {
		String[] cmd = { "test123", "abc\\n123" };
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		echo.run(cmd, null, outputStream);
		assertEquals("The output stream are different", "test123 abc\\n123"+System.lineSeparator(),
				outputStream.toString());
	}
	//
	// @Test(expected = EchoException.class)
	// public void testRunWithSpecialCharater2() throws EchoException {
	// String[] cmd = { "test*123", "abc123" };
	// ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
	// echo.run(cmd, null, outputStream);
	// }
	//
	// @Test(expected = EchoException.class)
	// public void testRunWithSpecialCharater3() throws EchoException {
	// String[] cmd = { "test(123", "abc123" };
	// ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
	// echo.run(cmd, null, outputStream);
	// }
	//
	// @Test(expected = EchoException.class)
	// public void testRunWithSpecialCharater4() throws EchoException {
	// String[] cmd = { "test123", "abc)123" };
	// ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
	// echo.run(cmd, null, outputStream);
	// }
}
