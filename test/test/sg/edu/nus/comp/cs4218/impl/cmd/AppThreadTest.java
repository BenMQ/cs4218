package test.sg.edu.nus.comp.cs4218.impl.cmd;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.impl.app.HeadApplication;
import sg.edu.nus.comp.cs4218.impl.cmd.AppThread;

public class AppThreadTest {

	private static AppThread appThread = null;
	private static HeadApplication headApp = null;
	private static ByteArrayOutputStream outContent = null;
	private static String testString = null;
	private static ByteArrayInputStream inputStream = null;

	@Before
	public void setUp() throws Exception {
		headApp = new HeadApplication();
		testString = "a" + System.lineSeparator() + "b";
		inputStream = new ByteArrayInputStream(
				testString.getBytes(StandardCharsets.UTF_8));
		outContent = new ByteArrayOutputStream();
		appThread = new AppThread(headApp, inputStream, outContent);
	}

	@After
	public void tearDown() throws Exception {
		headApp = null;
		testString = null;
		inputStream = null;
		outContent = null;
		appThread = null;
	}

	@Test
	public void testRunOutput() {
		appThread.run();
		assertEquals(testString, appThread.getOutputStream().toString());
	}

	@Test
	public void testGetStdin() {
		assertEquals(inputStream, appThread.getInputStream());
	}

	@Test
	public void testIsRunning() {
		assertEquals(false, appThread.isRunning());
	}

}
