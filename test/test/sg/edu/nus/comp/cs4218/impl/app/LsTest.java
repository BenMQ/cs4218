package test.sg.edu.nus.comp.cs4218.impl.app;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.exception.LsException;
import sg.edu.nus.comp.cs4218.impl.app.LsApplication;

public class LsTest {
	static private LsApplication app;

	static private File testDir = new File("testLsDir");

	@BeforeClass
	public static void setup() throws IOException {
		testDir.mkdir();
		File testFile = new File(testDir, "1.txt");
		testFile.createNewFile();
		testFile = new File(testDir, "2.txt");
		testFile.createNewFile();
		testFile = new File(testDir, ".hidden.txt");
		testFile.createNewFile();
	}

	@AfterClass
	public static void tearDown() {
		for (File file : testDir.listFiles()) {
			file.delete();
		}
		testDir.delete();

		// reset back to the user dir so that other test class
		// will not being affected by the modification
		Environment.currentDirectory = System.getProperty("user.dir");
	}

	@Before
	public void before() {
		app = new LsApplication();
		Environment.currentDirectory = System.getProperty("user.dir");

	}

	@Test
	public void testProcessArguments() throws LsException {
		String[] args = {};
		app.processArguments(args);
		assertNull(app.targetPath);
	}

	@Test
	public void testProcessArgumentsOne() throws LsException {
		String[] args = { "testLsDir" };
		app.processArguments(args);
		assertEquals(testDir.getAbsolutePath(),
				app.targetPath.getAbsolutePath());
	}

	@Test(expected = LsException.class)
	public void testProcessArgumentsMany() throws LsException {
		String[] args = { "testLsDir", "testLsDir2", "-m" };
		app.processArguments(args);
	}

	@Test
	public void testRun() throws LsException {

		String[] args = {};

		ByteArrayOutputStream stdout = new ByteArrayOutputStream();
		Environment.currentDirectory = testDir.getAbsolutePath();
		app.run(args, null, stdout);
		assertEquals("1.txt\t2.txt" + System.lineSeparator() + "", new String(
				stdout.toByteArray()));
	}

	@Test
	public void testRunWithArgument() throws LsException {
		String[] args = { "testLsDir" };
		ByteArrayOutputStream stdout = new ByteArrayOutputStream();
		app.run(args, null, stdout);

		assertEquals("1.txt\t2.txt" + System.lineSeparator() + "", new String(
				stdout.toByteArray()));
	}

	@Test(expected = LsException.class)
	public void testRunError() throws LsException {
		String[] args = { "testLsDir", "testLsDir2", "-m" };
		ByteArrayOutputStream stdout = new ByteArrayOutputStream();
		Environment.currentDirectory = testDir.getAbsolutePath();
		app.run(args, null, stdout);
	}

}
