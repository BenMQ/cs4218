package test.sg.edu.nus.comp.cs4218.impl;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import test.sg.edu.nus.comp.cs4218.m2.integration.ChainOfApplicationSubCmdTest;
import test.sg.edu.nus.comp.cs4218.m2.integration.ChainOfApplicationsPipeTest;
import test.sg.edu.nus.comp.cs4218.m2.integration.ChainOfInteractionsGrepAndPipeTest;
import test.sg.edu.nus.comp.cs4218.m2.integration.ChainOfInteractionsGrepAndSubCmdTest;
import test.sg.edu.nus.comp.cs4218.m2.integration.RedirectionAndFindTest;

@RunWith(value = org.junit.runners.Suite.class)
@Suite.SuiteClasses(value = { ChainOfInteractionsGrepAndSubCmdTest.class,
		ChainOfInteractionsGrepAndPipeTest.class,
		ChainOfApplicationsPipeTest.class, ChainOfApplicationSubCmdTest.class,
		RedirectionAndFindTest.class })
public class M2TestSuite {
}
