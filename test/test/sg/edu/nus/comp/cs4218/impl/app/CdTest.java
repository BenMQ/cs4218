package test.sg.edu.nus.comp.cs4218.impl.app;

import static org.junit.Assert.assertEquals;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.exception.CdException;
import sg.edu.nus.comp.cs4218.impl.app.CdApplication;

public final class CdTest {

	private static CdApplication cd;
	private static File testDir;
	private static File folderDepth1;
	private static File folderDepth2;
	private static File folderDepth3;
	private static BufferedWriter writer;
	private static String currentDir;

	@BeforeClass
	public static void setUp() throws IOException {
		testDir = new File("test_resources");
		testDir.mkdir();

		// make folders in test_resources
		folderDepth1 = new File(testDir, "depth1");
		folderDepth1.mkdir();

		folderDepth2 = new File(folderDepth1, "depth2");
		folderDepth2.mkdir();

		folderDepth3 = new File(folderDepth2, "depth3");
		folderDepth3.mkdir();

		// file with content text2
		File testFile = new File(folderDepth3, "file.txt");
		testFile.createNewFile();
		writer = new BufferedWriter(new FileWriter(testFile));
		writer.write("abc");
		writer.close();

		cd = new CdApplication();
	}

	@Before
	public void instantiation() {
		Environment.currentDirectory = System.getProperty("user.dir");
		currentDir = Environment.currentDirectory;
	}

	@AfterClass
	public static void tearDown() throws IOException {
		for (File file : folderDepth3.listFiles()) {
			file.delete();
		}

		folderDepth3.delete();
		folderDepth2.delete();
		folderDepth1.delete();
		testDir.delete();

		cd = null;
		writer = null;
		currentDir = null;

		// recover so that it will not affect the other test classes
		Environment.currentDirectory = System.getProperty("user.dir");
	}

	/**
	 * Test Boundary and error handling
	 */
	@Test
	public void testNoArgument() throws CdException {
		String[] cmd = {};
		cd.run(cmd, System.in, System.out);
		assertEquals(System.getProperty("user.dir"),
				Environment.currentDirectory);
	}

	@Test
	public void testNullArgument() throws CdException {
		String[] cmd = null;
		cd.run(cmd, System.in, System.out);
		assertEquals("The current working directory is changed",
				System.getProperty("user.dir"), Environment.currentDirectory);
	}

	/**
	 * Positive tests, the test running under windows is not valid since window
	 * have different path format.
	 */
	@Test
	public void testRunValidFilePath() throws CdException {
		String[] cmd = { "." };
		cd.run(cmd, System.in, System.out);
		assertEquals("The current working directory is changed", currentDir,
				Environment.currentDirectory);
	}

	@Test
	public void testRunGoToParentFolder() throws CdException {
		String[] cmd = { ".." };
		File currentLocation = new File(currentDir);
		String expLoc = currentLocation.getParentFile().getAbsolutePath();
		cd.run(cmd, System.in, System.out);
		assertEquals("The location string are different", expLoc,
				Environment.currentDirectory);
	}

	@Test
	public void testRunGoToGrandParentFolder() throws CdException {
		String[] cmd = { "../.." };
		File currentLocation = new File(currentDir);
		String expLoc = currentLocation.getParentFile().getParentFile()
				.getAbsolutePath();
		cd.run(cmd, System.in, System.out);
		assertEquals("The location string are different", expLoc,
				Environment.currentDirectory);
	}

	@Test
	public void testRunGoToHomeDirectory() throws CdException {
		String[] cmd = { "~" };
		String expLoc = Environment.HOME_DIRECTORY;
		cd.run(cmd, System.in, System.out);
		assertEquals("The location string are not home directory", expLoc,
				Environment.currentDirectory);
	}


	@Test
	public void testRunGoToChildDirectory() throws CdException {
		String[] cmd = { "test_resources" };
		File currentLocation = new File(currentDir, "test_resources");
		String expLoc = currentLocation.getAbsolutePath();
		cd.run(cmd, System.in, System.out);
		assertEquals("The location string are different", expLoc,
				Environment.currentDirectory);
	}

	@Test
	public void testRunGoToChildDirectoryWithPrefix() throws CdException {
		String[] cmd = { "./test_resources" };
		File currentLocation = new File(currentDir, "test_resources");
		String expLoc = currentLocation.getAbsolutePath();
		cd.run(cmd, System.in, System.out);
		assertEquals("The location string are different", expLoc,
				Environment.currentDirectory);
	}

	@Test
	public void testRunGoToChildDirectoryWithMultipleDepth() throws CdException {
		String[] cmd = { "./test_resources/depth1" };
		File currentLocation = new File(currentDir, "test_resources/depth1");
		String expLoc = currentLocation.getAbsolutePath();
		cd.run(cmd, System.in, System.out);
		assertEquals("The location string are different", expLoc,
				Environment.currentDirectory);
	}

	/**
	 * Negative tests
	 */
	@Test(expected = CdException.class)
	public void testRunGoToFileLocation() throws CdException {
		String[] cmd = { "./test_resources/depth1/depth2/depth3/file.txt" };
		cd.run(cmd, System.in, System.out);
	}

	@Test(expected = CdException.class)
	public void testRunGoToNoneExistingFolder() throws CdException {
		String[] cmd = { "./test_resources/depthNO" };
		cd.run(cmd, System.in, System.out);
	}

	@Test(expected = CdException.class)
	public void testRunGoToNoneExistingFolderWithHomeDirectory()
			throws CdException {
		String[] cmd = { "~/abc123/depth" };
		cd.run(cmd, System.in, System.out);
	}
}
