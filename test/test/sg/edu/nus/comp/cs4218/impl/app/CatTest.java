package test.sg.edu.nus.comp.cs4218.impl.app;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.exception.CatException;
import sg.edu.nus.comp.cs4218.impl.app.CatApplication;

public class CatTest {

	private static CatApplication cat;

	private static File testDir;
	private static BufferedWriter writer;

	private final static String TEXT1 = "123" + System.lineSeparator() + "abc"
			+ System.lineSeparator() + "456" + System.lineSeparator() + "def"
			+ System.lineSeparator() + "";
	private final static String TEXT2 = "456" + System.lineSeparator() + "def"
			+ System.lineSeparator() + "123" + System.lineSeparator() + "abc"
			+ System.lineSeparator() + "";

	@BeforeClass
	public static void setUp() throws IOException {
		cat = new CatApplication();
		testDir = new File("test_resources");
		testDir.mkdir();

		// file with content text1
		File testFile = new File(testDir, "fileA.txt");
		testFile.createNewFile();
		writer = new BufferedWriter(new FileWriter(testFile));
		writer.write(TEXT1);
		writer.close();

		// file with content text2
		testFile = new File(testDir, "fileB.txt");
		testFile.createNewFile();
		writer = new BufferedWriter(new FileWriter(testFile));
		writer.write(TEXT2);
		writer.close();

		// empty file
		testFile = new File(testDir, "fileC.txt");
		testFile.createNewFile();
		writer.close();
	}

	@AfterClass
	public static void tearDown() {
		for (File file : testDir.listFiles()) {
			file.delete();
		}
		testDir.delete();

		cat = null;
		writer = null;
	}

	/**
	 * Test Boundary and error handling
	 */
	@Test(expected = CatException.class)
	public void testNoArgument() throws CatException {
		String[] cmd = {};
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		cat.run(cmd, System.in, outputStream);
	}

	@Test(expected = CatException.class)
	public void testNullArgument() throws CatException {
		String[] cmd = null;
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		cat.run(cmd, System.in, outputStream);
	}

	@Test(expected = CatException.class)
	public void testNullInputStream() throws CatException {
		String[] cmd = {};
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		cat.run(cmd, null, outputStream);
	}

	@Test(expected = CatException.class)
	public void testNullOutputStream() throws CatException {
		String[] cmd = {};
		cat.run(cmd, System.in, null);
	}

	/**
	 * Positive Test
	 */
	@Test
	public void testRunNoneEmptyContentFile() throws CatException {
		String[] cmd = { "./test_resources/fileA.txt" };
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		cat.run(cmd, System.in, outputStream);
		assertNotNull("Output stream must not return null", outputStream);
		assertEquals(
				"The expected string is not the same as string in fileA.txt",
				TEXT1, outputStream.toString());
	}

	@Test
	public void testRunEmptyContentFile() throws CatException {
		String[] cmd = { "./test_resources/fileC.txt" };
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		cat.run(cmd, System.in, outputStream);
		assertNotNull("Output stream must not return null", outputStream);
		assertEquals(
				"The expected string is not the same as string in fileC.txt",
				"", outputStream.toString());
	}

	@Test
	public void testRunMultipleContentFile() throws CatException {
		String[] cmd = { "./test_resources/fileA.txt",
				"./test_resources/fileB.txt" };
		String joinedText = TEXT1 + TEXT2;
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		cat.run(cmd, System.in, outputStream);
		assertNotNull("Output stream must not return null", outputStream);
		assertEquals("The expected string is not the same as joined string",
				joinedText, outputStream.toString());
	}

	@Test
	public void testRunWithoutFileButWithStandardInput() throws CatException {
		String text = "abcdefg" + System.lineSeparator() + "12345678"
				+ System.lineSeparator() + "" + System.lineSeparator() + "";
		String[] cmd = {};
		ByteArrayInputStream inputStream = new ByteArrayInputStream(
				text.getBytes());
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		cat.run(cmd, inputStream, outputStream);
		assertNotNull("Output stream must not return null", outputStream);
		assertEquals("The expected string is not the same as input stream",
				text, outputStream.toString());
	}

	/**
	 * Negative Test
	 */
	@Test
	public void testRunNoneExistFile() throws CatException {
		String[] cmd = { "." + File.separatorChar + "test_resources"
				+ File.separatorChar + "fileD.txt" };
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		File noneExistFile = new File(testDir, "fileD.txt");
		String msg = "cat : " + noneExistFile.getAbsolutePath()
				+ ": No such file or directory" + System.lineSeparator() + "";
		cat.run(cmd, System.in, outputStream);
		assertNotNull("Output stream must not return null", outputStream);
		assertEquals("The expected string is not the same as output message",
				msg, outputStream.toString());
	}

	@Test
	public void testRunNoneExistFolder() throws CatException {
		String[] cmd = { "." + File.separatorChar + "test_resources"
				+ File.separatorChar + "folder" };
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		File noneExistFolder = new File(testDir, "folder");
		String msg = "cat : " + noneExistFolder.getAbsolutePath()
				+ ": No such file or directory" + System.lineSeparator() + "";
		cat.run(cmd, System.in, outputStream);
		assertNotNull("Output stream must not return null", outputStream);
		assertEquals("The expected string is not the same as output message",
				msg, outputStream.toString());
	}

	@Test
	public void testRunExistingDirectory() throws CatException {
		String[] cmd = { "." + File.separatorChar + "test_resources" };
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		String msg = "cat : " + testDir.getAbsolutePath() + ": Is a directory"
				+ System.lineSeparator() + "";
		cat.run(cmd, System.in, outputStream);
		assertNotNull("Output stream must not return null", outputStream);
		assertEquals("The expected string is not the same as output message",
				msg, outputStream.toString());
	}

	@Test
	public void testRunWithBothValidFileAndInvalidFile() throws CatException {
		String[] cmd = {
				"." + File.separatorChar + "test_resources"
						+ File.separatorChar + "fileA.txt",
				"." + File.separatorChar + "test_resources"
						+ File.separatorChar + "fileD.txt" };
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		File noneExistFile = new File(testDir, "fileD.txt");
		String msg = TEXT1 + "cat : " + noneExistFile.getAbsolutePath()
				+ ": No such file or directory" + System.lineSeparator() + "";
		cat.run(cmd, System.in, outputStream);
		assertNotNull("Output stream must not return null", outputStream);
		assertEquals(
				"The expected string is not the same as fileA.txt and output message",
				msg, outputStream.toString());

	}
}
