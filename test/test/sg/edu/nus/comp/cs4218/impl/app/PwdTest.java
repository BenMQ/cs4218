package test.sg.edu.nus.comp.cs4218.impl.app;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.File;

import org.junit.Before;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.exception.PwdException;
import sg.edu.nus.comp.cs4218.impl.app.PwdApplication;

public class PwdTest {

	private static PwdApplication pwd;

	@Before
	public void before() {
		pwd = new PwdApplication();
	}

	@Test(expected = PwdException.class)
	public void testValidateArguments() throws PwdException {
		pwd.validateArguments("hello", "world");
	}

	@Test(expected = PwdException.class)
	public void testValidateArgumentsOne() throws PwdException {
		pwd.validateArguments("-m");
	}

	@Test
	public void testValidateArgumentsZero() throws PwdException {
		pwd.validateArguments();
	}

	@Test
	public void testRun() throws PwdException {
		ByteArrayOutputStream stdout = new ByteArrayOutputStream();
		String[] args = new String[0];
		pwd.run(args, null, stdout);
		File cwdFile = new File(Environment.currentDirectory);
		assertEquals(cwdFile.getAbsolutePath() + System.lineSeparator(),
				new String(stdout.toByteArray()));
	}

	@Test(expected = PwdException.class)
	public void testRunError() throws PwdException {
		ByteArrayOutputStream stdout = new ByteArrayOutputStream();
		String[] args = { "-v" };
		pwd.run(args, null, stdout);
	}
}
