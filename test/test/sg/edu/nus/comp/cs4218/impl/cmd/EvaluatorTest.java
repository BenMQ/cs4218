package test.sg.edu.nus.comp.cs4218.impl.cmd;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.exception.AbstractApplicationException;
import sg.edu.nus.comp.cs4218.exception.ShellException;
import sg.edu.nus.comp.cs4218.impl.cmd.Evaluator;

public class EvaluatorTest {

	private static Evaluator evaluator;

	@Before
	public void setUp() {
		evaluator = new Evaluator();
	}

	@Test
	public void testTokenizeEmptyString() throws ShellException,
			AbstractApplicationException, ShellException {
		List<String> expected = new ArrayList<String>();
		List<String> actual = evaluator.tokenize("");
		assertEquals("The tokenized result should be empty", expected, actual);
	}

	@Test
	public void testTokenizeBlankString() throws ShellException,
			AbstractApplicationException, ShellException {
		List<String> expected = new ArrayList<String>();
		List<String> actual = evaluator.tokenize("    ");
		assertEquals("The tokenized result should be empty", expected, actual);
	}

	@Test
	public void testTokenizeSingleArgument() throws ShellException,
			AbstractApplicationException, ShellException {
		List<String> expected = new ArrayList<String>(Arrays.asList("foo"));
		List<String> actual = evaluator.tokenize("foo");
		assertEquals(expected, actual);
	}

	@Test
	public void testTokenizeMultipleArgument() throws ShellException,
			AbstractApplicationException, ShellException {
		List<String> expected = new ArrayList<String>(Arrays.asList("foo",
				"bar", "foobar"));
		List<String> actual = evaluator.tokenize("foo bar foobar");
		assertEquals(expected, actual);
	}

	@Test
	public void testTokenizeWithDelimiterArgument() throws ShellException,
			AbstractApplicationException, ShellException {
		List<String> expected = new ArrayList<String>(Arrays.asList("foo", ";",
				"bar"));
		List<String> actual = evaluator.tokenize("foo;bar");
		assertEquals(expected, actual);
	}

	@Test
	public void testTokenizeWithMultipleDelimiterArgument()
			throws ShellException, AbstractApplicationException, ShellException {
		List<String> expected = new ArrayList<String>(Arrays.asList("foo", ";",
				"bar", "|", "foobar", "<", "hello", "world", "|"));
		List<String> actual = evaluator
				.tokenize("foo;bar | foobar<hello world |");
		assertEquals(expected, actual);
	}

	@Test
	public void testTokenizeWithSingleQuote() throws ShellException,
			AbstractApplicationException, ShellException {
		List<String> expected = new ArrayList<String>(Arrays.asList("foo",
				"bar"));
		List<String> actual = evaluator.tokenize("foo 'bar'");
		assertEquals(expected, actual);
	}

	@Test
	public void testTokenizeWithSingleQuoteNesting() throws ShellException,
			AbstractApplicationException, ShellException {
		List<String> expected = new ArrayList<String>(Arrays.asList("foo",
				"bar\";\""));
		List<String> actual = evaluator.tokenize("foo 'bar\";\"'");
		assertEquals(expected, actual);
	}

	@Test
	public void testTokenizeWithBackQuote() throws ShellException,
			AbstractApplicationException, ShellException {
		List<String> expected = new ArrayList<String>(Arrays.asList("foo",
				"bar"));
		List<String> actual = evaluator.tokenize("foo `echo bar`");
		assertEquals(expected, actual);
	}

	@Test
	public void testTokenizeWithBackQuoteNesting() throws ShellException,
			AbstractApplicationException, ShellException {
		List<String> expected = new ArrayList<String>(Arrays.asList("foo",
				"bar;"));
		List<String> actual = evaluator.tokenize("foo `echo \"bar;\"`");
		assertEquals(expected, actual);
	}

	@Test
	public void testTokenizeWithDoubleQuote() throws ShellException,
			AbstractApplicationException, ShellException {
		List<String> expected = new ArrayList<String>(Arrays.asList("foo",
				"bar"));
		List<String> actual = evaluator.tokenize("foo \"bar\"");
		assertEquals(expected, actual);
	}

	@Test
	public void testTokenizeWithDoubleQuoteNesting() throws ShellException,
			AbstractApplicationException, ShellException {
		List<String> expected = new ArrayList<String>(Arrays.asList("foo",
				"bar';'"));
		List<String> actual = evaluator.tokenize("foo \"bar';'\"");
		assertEquals(expected, actual);
	}

	@Test
	public void testTokenizeWithDoubleQuoteNestingWithBackQuotes()
			throws ShellException, AbstractApplicationException, ShellException {
		List<String> expected = new ArrayList<String>(Arrays.asList("foo",
				"barhello"));
		List<String> actual = evaluator.tokenize("foo \"bar`echo \"hello\"`\"");
		assertEquals(expected, actual);
	}

	@Test(expected = ShellException.class)
	public void testTokenizeWithNewLine() throws ShellException,
			AbstractApplicationException, ShellException {
		evaluator.tokenize("foo \nbar");
	}

	@Test
	public void testTokenizeWithTab() throws ShellException,
			AbstractApplicationException, ShellException {

		List<String> expected = new ArrayList<String>(Arrays.asList("foo",
				"bar"));
		List<String> actual = evaluator.tokenize("foo\tbar");
		assertEquals(expected, actual);
	}

	@Test(expected = ShellException.class)
	public void testTokenizeWithIncompleteQuotes() throws ShellException,
			AbstractApplicationException, ShellException {
		evaluator.tokenize("foo 'bar");
	}

	@Test
	public void testTokenizeWithMultipleArguments() throws ShellException,
			AbstractApplicationException, ShellException {
		List<String> expected = new ArrayList<String>(Arrays.asList("foo",
				"bar", "foo", "bar", "foo", "bar", "foo", "bar"));
		List<String> actual = evaluator
				.tokenize("foo bar foo bar foo bar foo bar");
		assertEquals(expected, actual);
	}

	@Test
	public void testTokenizeSingleChar() throws ShellException,
			AbstractApplicationException, ShellException {
		List<String> expected = new ArrayList<String>(Arrays.asList("grep",
				"#", "|", "ls"));
		List<String> actual = evaluator.tokenize("grep # | ls");
		assertEquals(expected, actual);
	}

	@Test
	public void testTokenizeWithContinuousWhitespaces() throws ShellException,
			AbstractApplicationException, ShellException {
		List<String> expected = new ArrayList<String>(Arrays.asList("foo",
				"bar", "foo", "bar"));
		List<String> actual = evaluator
				.tokenize("foo    bar foo\t\t\tbar\t\t\t");
		assertEquals(expected, actual);
	}

	@Test
	public void testTokenizeWithNewLineInSubstitution() throws ShellException,
			AbstractApplicationException, ShellException {
		List<String> expected = new ArrayList<String>(Arrays.asList("foo", ""));
		List<String> actual = evaluator.tokenize("foo `echo \"\"`");
		assertEquals(expected, actual);
	}

	@Test
	public void testTokenizeWithDoubleQuoteAndImmediateBackQuote()
			throws ShellException, AbstractApplicationException, ShellException {
		List<String> expected = new ArrayList<String>(Arrays.asList("foo", ""));
		List<String> actual = evaluator.tokenize("foo \"`echo \"\"`\"");
		assertEquals(expected, actual);
	}

	@Test
	public void testParseNormalStateAndGetNextStateWhiteSpace()
			throws ShellException, IOException {
		List<String> expected = new ArrayList<String>(Arrays.asList("prev"));
		ArrayList<String> result = new ArrayList<String>();
		StringBuilder buffer = new StringBuilder("prev");
		Evaluator.ParsingState state = evaluator
				.parseNormalStateAndGetNextState(' ', buffer, result);
		assertEquals(Evaluator.ParsingState.NORMAL, state);
		assertEquals(expected, result);
	}

	@Test
	public void testParseNormalStateAndGetNextStateDelimiter()
			throws ShellException, IOException {
		List<String> expected = new ArrayList<String>(
				Arrays.asList("prev", ";"));
		ArrayList<String> result = new ArrayList<String>();
		StringBuilder buffer = new StringBuilder("prev");
		Evaluator.ParsingState state = evaluator
				.parseNormalStateAndGetNextState(';', buffer, result);
		assertEquals(Evaluator.ParsingState.NORMAL, state);
		assertEquals(expected, result);
	}

	@Test
	public void testParseNormalStateAndGetNextStateSingleQuote()
			throws ShellException, IOException {
		List<String> expected = new ArrayList<String>();
		ArrayList<String> result = new ArrayList<String>();
		StringBuilder buffer = new StringBuilder();
		Evaluator.ParsingState state = evaluator
				.parseNormalStateAndGetNextState('\'', buffer, result);
		assertEquals(Evaluator.ParsingState.SINGLE_QUOTED, state);
		assertEquals(expected, result);
	}

	@Test(expected = ShellException.class)
	public void testParseNormalStateAndGetNextStateSingleQuoteError()
			throws ShellException, IOException {
		ArrayList<String> result = new ArrayList<String>();
		StringBuilder buffer = new StringBuilder("prev");
		evaluator.parseNormalStateAndGetNextState('\'', buffer, result);
	}

	@Test
	public void testParseNormalStateAndGetNextStateDoubleQuote()
			throws ShellException, IOException {
		List<String> expected = new ArrayList<String>();
		ArrayList<String> result = new ArrayList<String>();
		StringBuilder buffer = new StringBuilder();
		Evaluator.ParsingState state = evaluator
				.parseNormalStateAndGetNextState('"', buffer, result);
		assertEquals(Evaluator.ParsingState.DOUBLE_QUOTED, state);
		assertEquals(expected, result);
	}

	@Test(expected = ShellException.class)
	public void testParseNormalStateAndGetNextStateDoubleQuoteError()
			throws ShellException, IOException {
		ArrayList<String> result = new ArrayList<String>();
		StringBuilder buffer = new StringBuilder("prev");
		evaluator.parseNormalStateAndGetNextState('"', buffer, result);
	}

	@Test
	public void testParseNormalStateAndGetNextStateBackQuote()
			throws ShellException, IOException {
		List<String> expected = new ArrayList<String>();
		ArrayList<String> result = new ArrayList<String>();
		StringBuilder buffer = new StringBuilder();
		Evaluator.ParsingState state = evaluator
				.parseNormalStateAndGetNextState('`', buffer, result);
		assertEquals(Evaluator.ParsingState.BACK_QUOTED, state);
		assertEquals(expected, result);
	}

	@Test(expected = ShellException.class)
	public void testParseNormalStateAndGetNextStateBackQuoteError()
			throws ShellException, IOException {
		ArrayList<String> result = new ArrayList<String>();
		StringBuilder buffer = new StringBuilder("prev");
		evaluator.parseNormalStateAndGetNextState('`', buffer, result);
	}

	@Test
	public void testParseNormalStateAndGetNextStateNormal()
			throws ShellException, IOException {
		ArrayList<String> expected = new ArrayList<String>();
		ArrayList<String> result = new ArrayList<String>();
		StringBuilder buffer = new StringBuilder("prev");
		Evaluator.ParsingState state = evaluator
				.parseNormalStateAndGetNextState('x', buffer, result);
		assertEquals(Evaluator.ParsingState.NORMAL, state);
		assertEquals("prevx", buffer.toString());
		assertEquals(expected, result);
	}

	@Test
	public void testParseDoubleQuotedStateAndGetNextStateDoubleQuote()
			throws ShellException, IOException {
		ArrayList<String> expected = new ArrayList<String>(
				Arrays.asList("prev"));
		ArrayList<String> result = new ArrayList<String>();
		StringBuilder buffer = new StringBuilder("prev");
		Evaluator.ParsingState state = evaluator
				.parseDoubleQuotedStateAndGetNextState('"', buffer, result);
		assertEquals(Evaluator.ParsingState.EXPECTING_SPACE, state);
		assertEquals("", buffer.toString());
		assertEquals(expected, result);
	}

	@Test
	public void testParseDoubleQuotedStateAndGetNextStateNormal()
			throws ShellException, IOException {
		ArrayList<String> expected = new ArrayList<String>();
		ArrayList<String> result = new ArrayList<String>();
		StringBuilder buffer = new StringBuilder("prev");
		Evaluator.ParsingState state = evaluator
				.parseDoubleQuotedStateAndGetNextState('x', buffer, result);
		assertEquals(Evaluator.ParsingState.DOUBLE_QUOTED, state);
		assertEquals("prevx", buffer.toString());
		assertEquals(expected, result);
	}

	@Test
	public void testParseDoubleQuotedStateAndGetNextStateBackQuote()
			throws ShellException, IOException {
		ArrayList<String> expected = new ArrayList<String>(
				Arrays.asList("prev"));
		ArrayList<String> result = new ArrayList<String>();
		StringBuilder buffer = new StringBuilder("prev");
		Evaluator.ParsingState state = evaluator
				.parseDoubleQuotedStateAndGetNextState('`', buffer, result);
		assertEquals(Evaluator.ParsingState.BACK_QUOTED_IN_DOUBLE_QUOTED, state);
		assertEquals("", buffer.toString());
		assertEquals(expected, result);
	}

	@Test
	public void testParseBackQuotedStateAndGetNextStateBackQuote()
			throws ShellException, IOException, AbstractApplicationException {
		ArrayList<String> expected = new ArrayList<String>(Arrays.asList("x",
				"y", "z"));
		ArrayList<String> result = new ArrayList<String>();
		StringBuilder buffer = new StringBuilder("echo \"x y z\"");
		Evaluator.ParsingState state = evaluator
				.parseBackQuotedStateAndGetNextState('`', buffer, result);
		assertEquals(Evaluator.ParsingState.EXPECTING_SPACE, state);
		assertEquals("", buffer.toString());
		assertEquals(expected, result);
	}

	@Test
	public void testParseBackQuotedStateAndGetNextStateNormal()
			throws ShellException, IOException, AbstractApplicationException {
		ArrayList<String> expected = new ArrayList<String>();
		ArrayList<String> result = new ArrayList<String>();
		StringBuilder buffer = new StringBuilder("prev");
		Evaluator.ParsingState state = evaluator
				.parseBackQuotedStateAndGetNextState('x', buffer, result);
		assertEquals(Evaluator.ParsingState.BACK_QUOTED, state);
		assertEquals("prevx", buffer.toString());
		assertEquals(expected, result);
	}

	@Test
	public void testparseBackQuotedInDoubleQuotedStateAndGetNextStateBackQuote()
			throws ShellException, IOException, AbstractApplicationException {
		ArrayList<String> expected = new ArrayList<String>();
		ArrayList<String> result = new ArrayList<String>(Arrays.asList(""));
		StringBuilder buffer = new StringBuilder("echo foo");
		Evaluator.ParsingState state = evaluator
				.parseBackQuotedInDoubleQuotedStateAndGetNextState('`', buffer,
						result);
		assertEquals(Evaluator.ParsingState.DOUBLE_QUOTED, state);
		assertEquals("foo", buffer.toString());
		assertEquals(expected, result);
	}

	@Test
	public void testparseBackQuotedInDoubleQuotedStateAndGetNextStateNormal()
			throws ShellException, IOException, AbstractApplicationException {
		ArrayList<String> expected = new ArrayList<String>();
		ArrayList<String> result = new ArrayList<String>();
		StringBuilder buffer = new StringBuilder("prev");
		Evaluator.ParsingState state = evaluator
				.parseBackQuotedInDoubleQuotedStateAndGetNextState('x', buffer,
						result);
		assertEquals(Evaluator.ParsingState.BACK_QUOTED_IN_DOUBLE_QUOTED, state);
		assertEquals("prevx", buffer.toString());
		assertEquals(expected, result);
	}

	@Test
	public void testParseExpectingSpaceAndGetNextStateWhiteSpace()
			throws ShellException, IOException {
		ArrayList<String> expected = new ArrayList<String>();
		ArrayList<String> result = new ArrayList<String>();
		StringBuilder buffer = new StringBuilder("prev");
		Evaluator.ParsingState state = evaluator
				.parseExpectingSpaceAndGetNextState(' ', buffer, result);
		assertEquals(Evaluator.ParsingState.NORMAL, state);
		assertEquals("prev", buffer.toString());
		assertEquals(expected, result);
	}

	@Test
	public void testParseExpectingSpaceAndGetNextStateDelimiter()
			throws ShellException, IOException {
		ArrayList<String> expected = new ArrayList<String>(Arrays.asList(";"));
		ArrayList<String> result = new ArrayList<String>();
		StringBuilder buffer = new StringBuilder("prev");
		Evaluator.ParsingState state = evaluator
				.parseExpectingSpaceAndGetNextState(';', buffer, result);
		assertEquals(Evaluator.ParsingState.NORMAL, state);
		// buffer does not matter
		// assertEquals("prev", buffer.toString());
		assertEquals(expected, result);
	}

	@Test(expected = ShellException.class)
	public void testParseExpectingSpaceAndGetNextStateError()
			throws ShellException, IOException {
		ArrayList<String> expected = new ArrayList<String>(Arrays.asList(";"));
		ArrayList<String> result = new ArrayList<String>();
		StringBuilder buffer = new StringBuilder("prev");
		Evaluator.ParsingState state = evaluator
				.parseExpectingSpaceAndGetNextState('x', buffer, result);
		assertEquals(Evaluator.ParsingState.NORMAL, state);
	}
}
