package test.sg.edu.nus.comp.cs4218.impl;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import test.sg.edu.nus.comp.cs4218.impl.cmd.AppThreadTest;
import test.sg.edu.nus.comp.cs4218.impl.cmd.CallTest;
import test.sg.edu.nus.comp.cs4218.impl.cmd.EvaluatorTest;
import test.sg.edu.nus.comp.cs4218.impl.cmd.GlobbingTest;
import test.sg.edu.nus.comp.cs4218.impl.cmd.PipeTest;
import test.sg.edu.nus.comp.cs4218.impl.cmd.RedirectLeftTest;
import test.sg.edu.nus.comp.cs4218.impl.cmd.RedirectRightTest;

@RunWith(value = org.junit.runners.Suite.class)
@Suite.SuiteClasses(value = { AppThreadTest.class, CallTest.class,
		EvaluatorTest.class, GlobbingTest.class, PipeTest.class,
		RedirectLeftTest.class, RedirectRightTest.class })
public class CmdTestSuite {
}
