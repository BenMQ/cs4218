package test.sg.edu.nus.comp.cs4218.impl.app;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.exception.FindException;
import sg.edu.nus.comp.cs4218.impl.app.FindApplication;

public final class FindTest {

	private static FindApplication find;
	private static File testDir;
	private static File folderDepth1;
	private static String cwd;

	@Before
	public void before() {
		find = new FindApplication();
		Environment.currentDirectory = cwd;
	}

	@BeforeClass
	public static void setUp() throws IOException {
		testDir = new File("test_resources");
		testDir.mkdir();
		cwd = Environment.currentDirectory;
		String[] fileList1 = { "computer.txt", "computing.jar", "science.txt",
				"math.txt", "data.txt", "probability.txt" };
		for (int i = 0; i < fileList1.length; i++) {
			File file = new File(testDir, fileList1[i]);
			file.createNewFile();
		}

		// make folders in test_resources
		folderDepth1 = new File(testDir, "depth1");
		folderDepth1.mkdir();

		String[] fileList2 = { "computer.txt", "information.txt", "tech.txt",
				"coffee.txt", "dictionary.txt", "program.jar" };
		for (int i = 0; i < fileList2.length; i++) {
			File file = new File(folderDepth1, fileList2[i]);
			file.createNewFile();
		}

		find = new FindApplication();
	}

	@AfterClass
	public static void tearDown() {
		for (File file : folderDepth1.listFiles()) {
			file.delete();
		}
		folderDepth1.delete();

		for (File file : testDir.listFiles()) {
			file.delete();
		}
		testDir.delete();

		find = null;
	}

	/**
	 * Boundary and exception handler
	 */
	@Test(expected = FindException.class)
	public void testNoArgument() throws FindException {
		String[] cmd = {};
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		find.run(cmd, System.in, outputStream);
	}

	@Test(expected = FindException.class)
	public void testOneArgument() throws FindException {
		String[] cmd = { "-name" };
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		find.run(cmd, System.in, outputStream);
	}

	@Test(expected = FindException.class)
	public void testNullArgument() throws FindException {
		String[] cmd = null;
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		find.run(cmd, System.in, outputStream);
	}

	@Test(expected = FindException.class)
	public void testNullOutputStream() throws FindException {
		String[] cmd = { "-name" };
		find.run(cmd, System.in, null);
	}

	/**
	 * Positive tests
	 */
	@Test
	public void testRunWithoutSpecifyPath() throws FindException {
		String[] cmd = { "-name", "*.jar" };
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		String expectedResult = "." + File.separatorChar + "test_resources"
				+ File.separatorChar + "computing.jar" + System.lineSeparator()
				+ "." + File.separatorChar + "test_resources"
				+ File.separatorChar + "depth1" + File.separatorChar
				+ "program.jar" + System.lineSeparator() + "";
		find.run(cmd, System.in, outputStream);
		assertNotNull("The output stream must not be null", outputStream);
		assertEquals("The output jar files are different", expectedResult,
				outputStream.toString());
	}

	@Test
	public void testRunWithSpecifiedPath() throws FindException {
		String[] cmd = {
				"." + File.separatorChar + "test_resources"
						+ File.separatorChar + "depth1", "-name", "*.jar" };
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		String expectedResult = "." + File.separatorChar + "test_resources"
				+ File.separatorChar + "depth1" + File.separatorChar
				+ "program.jar" + System.lineSeparator();
		find.run(cmd, System.in, outputStream);
		assertNotNull("The output stream must not be null", outputStream);
		assertEquals("The output jar files are different", expectedResult,
				outputStream.toString());
	}

	@Test
	public void testRunWithSpecifiedPathAndWithAsteriskInBetweenSearchPattern()
			throws FindException {
		String[] cmd = {
				"." + File.separatorChar + "test_resources"
						+ File.separatorChar + "depth1", "-name", "co*.txt" };
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		String expectedResult = "." + File.separatorChar + "test_resources"
				+ File.separatorChar + "depth1" + File.separatorChar
				+ "coffee.txt" + System.lineSeparator() + "."
				+ File.separatorChar + "test_resources" + File.separatorChar
				+ "depth1" + File.separatorChar + "computer.txt"
				+ System.lineSeparator() + "";
		find.run(cmd, System.in, outputStream);
		assertNotNull("The output stream must not be null", outputStream);
		assertEquals("The output jar files are different", expectedResult,
				outputStream.toString());
	}

	@Test
	public void testRunWithAsteriskAtEndOfSearchPattern() throws FindException {
		String[] cmd = { "-name", "computer.*" };
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		String expectedResult = "." + File.separatorChar + "test_resources"
				+ File.separatorChar + "computer.txt" + System.lineSeparator()
				+ "." + File.separatorChar + "test_resources"
				+ File.separatorChar + "depth1" + File.separatorChar
				+ "computer.txt" + System.lineSeparator() + "";
		find.run(cmd, System.in, outputStream);
		assertNotNull("The output stream must not be null", outputStream);
		assertEquals("The output jar files are different", expectedResult,
				outputStream.toString());
	}

	/**
	 * Negative tests
	 */
	@Test(expected = FindException.class)
	public void testThreeArgumentWithNoNameSpecified() throws FindException {
		String[] cmd = { ".", "*.txt" };
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		find.run(cmd, System.in, outputStream);
	}

	@Test
	public void testRunSearchNoneExistingFile() throws FindException {
		String[] cmd = { "-name", "abcde.txt" };
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		find.run(cmd, System.in, outputStream);
		assertNotNull(outputStream);
		assertEquals(
				"The output should be empty as there is no file abcde.txt", "",
				outputStream.toString());
	}

}
