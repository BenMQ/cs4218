package sg.edu.nus.comp.cs4218.impl.cmd;

import java.io.InputStream;
import java.io.OutputStream;

import sg.edu.nus.comp.cs4218.Application;
import sg.edu.nus.comp.cs4218.Command;
import sg.edu.nus.comp.cs4218.exception.AbstractApplicationException;
import sg.edu.nus.comp.cs4218.exception.ShellException;

public class Call implements Command {

	private String[] args = null;
	private Application app = null;
	private AppThread appThread = null;

	/**
	 * Call constructor with arguments
	 * 
	 * @param app
	 *            The predefined application
	 * @param args
	 *            The input arguments of the predefined application
	 */
	public Call(Application app, String... args) {
		this(app);
		this.args = args;
	}

	/**
	 * Call constructor without arguments
	 * 
	 * @param app
	 *            The predefined application
	 */
	public Call(Application app) {
		this.app = app;
	}

	@Override
	public void evaluate(InputStream stdin, OutputStream stdout)
			throws AbstractApplicationException, ShellException {
		OutputStream actualStdout = stdout;
		if (stdout == null) {
			actualStdout = System.out;
		}
		if (app == null) {
			throw new ShellException("Invalid application");
		}
		appThread = new AppThread(app, stdin, actualStdout, args);
		appThread.run();
	}

	@Override
	public void terminate() {
		appThread.stop();

	}

	public AppThread getAppThread() {
		return appThread;
	}

	public InputStream getInputStream() {
		return appThread.getInputStream();
	}

	public OutputStream getOutputStream() {
		return appThread.getOutputStream();
	}

	public boolean isDone() {
		return !appThread.isRunning();
	}

}
