package sg.edu.nus.comp.cs4218.impl.cmd;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import sg.edu.nus.comp.cs4218.Application;
import sg.edu.nus.comp.cs4218.Command;
import sg.edu.nus.comp.cs4218.Shell;
import sg.edu.nus.comp.cs4218.exception.AbstractApplicationException;
import sg.edu.nus.comp.cs4218.exception.ShellException;
import sg.edu.nus.comp.cs4218.impl.util.AppUtility;

public class Evaluator implements Shell {

	List<String> appCommands = null;
	List<String> opCommands = null;

	static final char SINGLE_QUOTE = '\'';
	static final char DOUBLE_QUOTE = '"';
	static final char BACK_QUOTE = '`';
	static final char NEW_LINE = '\n';
	static final char SPACE = ' ';
	static final char PIPE = '|';
	static final char SEMICOLON = ';';
	static final char LESS_THAN = '<';
	static final char GREATER_THAN = '>';
	static final char TAB = '\t';
	static final char WILDCARD_CHAR = '*';

	static final ArrayList<Character> DELIMITERS = new ArrayList<Character>(
			Arrays.asList(PIPE, SEMICOLON, LESS_THAN, GREATER_THAN));
	static final ArrayList<Character> WHITESPACES = new ArrayList<Character>(
			Arrays.asList(SPACE, TAB));

	public enum ParsingState {
		NORMAL, SINGLE_QUOTED, DOUBLE_QUOTED, BACK_QUOTED, BACK_QUOTED_IN_DOUBLE_QUOTED, EXPECTING_SPACE
	}

	/**
	 * Constructor of evaluator, init application commands and operator
	 * commands(delimiters) TODO : Application command and operation command
	 * needed for evaluation TODO : Alternative way is constructing grammar
	 * rules
	 */
	public Evaluator() {
		appCommands = new ArrayList<String>();
		opCommands = new ArrayList<String>();
	}

	@Override
	public void parseAndEvaluate(String cmdline, OutputStream stdout)
			throws AbstractApplicationException, ShellException {
		List<String> tokens = tokenize(cmdline);
		constructCalls(tokens, stdout);
	}

	private void constructCalls(List<String> tokens, OutputStream stdout)
			throws ShellException, AbstractApplicationException, ShellException {
		// If there are no tokens, no calls to construct
		if (tokens.size() < 1) {
			return;
		}
		// If there is only one token
		if (tokens.size() == 1) {
			constructCallOneToken(tokens, stdout);
		} else {
			// Add a ';' to the end of the tokens signaling end of evaluation
			if (tokens.get(tokens.size() - 1).toString().equals(";") == false) {
				tokens.add(";");
			}

			// More than 1 token
			int lastStoppedIndex = 0;
			Call currCall = null;
			Command commandStack = null;

			// Logic switches for Pipe, L-Redirect, R-Redirect
			boolean isPipe = false;
			boolean isLeftRedirect = false;
			boolean isRightRedirect = false;

			boolean isEvaluateCurrCall = false;
			while (lastStoppedIndex < tokens.size()) {
				String currAppName = tokens.get(lastStoppedIndex++);
				ArrayList<String> currAppArgsList = new ArrayList<String>();
				Application currApp = AppUtility
						.getApplicationFromString(currAppName);

				// Gets app args then stop
				for (int i = lastStoppedIndex; i < tokens.size(); i++) {
					String token = tokens.get(i);
					if (token.equals("|")) {
						isPipe = true;
						isLeftRedirect = false;
						isRightRedirect = false;
						lastStoppedIndex = i + 1;
						break;
					} else if (token.equals("<")) {
						isLeftRedirect = true;
						isPipe = false;
						isRightRedirect = false;
						lastStoppedIndex = i + 1;
						break;
					} else if (token.equals(">")) {
						isRightRedirect = true;
						isPipe = false;
						isLeftRedirect = false;
						lastStoppedIndex = i + 1;
						break;
					} else if (token.equals(";")) {
						isEvaluateCurrCall = true;
						lastStoppedIndex = i + 1;
						break;
					}
					currAppArgsList.add(token);
					lastStoppedIndex = i;
				}
				// Reached the end of tokens, must evaluate now even no ';' is
				// present
				if (lastStoppedIndex + 1 >= tokens.size()) {
					isEvaluateCurrCall = true;
				}

				// Execute logic switches for Pipe, L-Redirect, R-Redirect
				if (isPipe) {
					if (commandStack == null) {
						commandStack = new Call(currApp,
								currAppArgsList.toArray(new String[] {}));
					} else if (!tokens.get(lastStoppedIndex - 1).equals("|")) {
						commandStack = new Pipe(commandStack, new Call(currApp,
								currAppArgsList.toArray(new String[] {})));
					}
				} else if (isLeftRedirect) {
					isLeftRedirect = false;
					if (commandStack == null) {
						commandStack = new Call(currApp,
								currAppArgsList.toArray(new String[] {}));
						commandStack = new RedirectLeft(commandStack,
								tokens.get(lastStoppedIndex));
					} else {
						commandStack = new RedirectLeft(commandStack,
								currAppName);
					}

				} else if (isRightRedirect) {
					isRightRedirect = false;
					if (commandStack == null) {
						commandStack = new Call(currApp,
								currAppArgsList.toArray(new String[] {}));
						commandStack = new RedirectRight(commandStack,
								tokens.get(lastStoppedIndex));
					} else {
						commandStack = new RedirectRight(commandStack,
								currAppName);
					}
				}

				currCall = new Call(currApp,
						currAppArgsList.toArray(new String[] {}));
				if (isEvaluateCurrCall) {
					if (commandStack != null) {
						commandStack.evaluate(null, stdout);
						commandStack = null;
						isPipe = false;
					} else {
						currCall.evaluate(null, stdout);
						currCall = null;
					}
					commandStack = null;
					currCall = null;
					isEvaluateCurrCall = false;
				}
			}

		}

	}

	private void constructCallOneToken(List<String> tokens, OutputStream stdout)
			throws ShellException, AbstractApplicationException, ShellException {
		String appName = tokens.get(0);
		Application application = AppUtility.getApplicationFromString(appName);
		if (application == null) {
			throw new ShellException("Application not found: " + appName);
		}
		Call call = new Call(application);
		call.evaluate(null, stdout);
	}

	/**
	 * Tokenize the input string to a list of strings
	 * 
	 * @param rawCmd
	 *            The original input string
	 * @return A list of strings
	 * @throws ShellException
	 */
	public List<String> tokenize(String rawCmd) throws ShellException,
			AbstractApplicationException, ShellException {

		if (rawCmd == null) {
			throw new ShellException("Received null string");
		}

		ArrayList<String> result = new ArrayList<String>();
		ParsingState state = ParsingState.NORMAL;
		StringBuilder buffer = new StringBuilder();
		char tempChar;
		String cmdline = rawCmd.trim();
		for (int i = 0; i < cmdline.length(); i++) {
			tempChar = cmdline.charAt(i);
			if (tempChar == NEW_LINE) {
				throw new ShellException("Unexpected new line");
			}
			switch (state) {
			case NORMAL:
				try {
					state = parseNormalStateAndGetNextState(tempChar, buffer,
							result);
				} catch (IOException e) {
					throw new ShellException(
							"Encountered IO Error while performing globbing");
				}
				break;
			case SINGLE_QUOTED:
				state = parseSingleQuotedStateAndGetNextState(tempChar, buffer,
						result);
				break;
			case DOUBLE_QUOTED:
				state = parseDoubleQuotedStateAndGetNextState(tempChar, buffer,
						result);
				break;
			case BACK_QUOTED:
				state = parseBackQuotedStateAndGetNextState(tempChar, buffer,
						result);
				break;
			case BACK_QUOTED_IN_DOUBLE_QUOTED:
				state = parseBackQuotedInDoubleQuotedStateAndGetNextState(
						tempChar, buffer, result);
				break;
			case EXPECTING_SPACE:
				state = parseExpectingSpaceAndGetNextState(tempChar, buffer,
						result);
				break;
			}
		}
		if (state == ParsingState.NORMAL) {
			String buffered = buffer.toString();
			buffer.setLength(0); // empty buffer
			try {
				if (buffered.length() > 0) {
					result.addAll(glob(buffered));
				}
			} catch (IOException e) {
				throw new ShellException(
						"Encountered IO Error while performing globbing");
			}
		} else if (state == ParsingState.EXPECTING_SPACE) {
			// no op
		} else {
			throw new ShellException("Unexpected end of input");
		}
		return result;
	}

	/**
	 * Get next state from Normal state
	 * 
	 * @param c
	 *            The input single character need to be verified
	 * @param buffer
	 *            The string builder
	 * @param result
	 *            The tokenized string list
	 * @return The next state
	 * @throws ShellException
	 * @throws IOException
	 */
	public ParsingState parseNormalStateAndGetNextState(char c,
			StringBuilder buffer, ArrayList<String> result)
			throws ShellException, IOException {
		if (WHITESPACES.contains(c)) {
			String buffered = buffer.toString();
			buffer.setLength(0); // empty buffer
			if (buffered.trim().length() > 0) {
				result.addAll(glob(buffered));
			}
			return ParsingState.NORMAL;
		} else if (DELIMITERS.contains(c)) {
			String buffered = buffer.toString();
			buffer.setLength(0); // empty buffer
			if (buffered.trim().length() > 0) {
				result.addAll(glob(buffered));
			}
			// add the delimiter itself
			result.add(Character.toString(c));
			return ParsingState.NORMAL;
		} else if (c == SINGLE_QUOTE) {
			if (buffer.length() == 0) {
				return ParsingState.SINGLE_QUOTED;
			} else {
				throw new ShellException("Unexpected single quote");
			}
		} else if (c == DOUBLE_QUOTE) {
			if (buffer.length() == 0) {
				return ParsingState.DOUBLE_QUOTED;
			} else {
				throw new ShellException("Unexpected double quote");
			}
		} else if (c == BACK_QUOTE) {
			if (buffer.length() == 0) {
				return ParsingState.BACK_QUOTED;
			} else {
				throw new ShellException("Unexpected back quote");
			}
		} else {
			buffer.append(c);
			return ParsingState.NORMAL;
		}
	}

	/**
	 * Get next state from processing single quoted state
	 * 
	 * @param c
	 *            The input character
	 * @param buffer
	 *            The string builder
	 * @param result
	 *            The tokenized string list
	 * @return The next state
	 * @throws ShellException
	 */
	public ParsingState parseSingleQuotedStateAndGetNextState(char c,
			StringBuilder buffer, ArrayList<String> result)
			throws ShellException {
		switch (c) {
		case SINGLE_QUOTE:
			result.add(buffer.toString());
			buffer.setLength(0); // empty buffer
			return ParsingState.EXPECTING_SPACE;
		default:
			buffer.append(c);
			return ParsingState.SINGLE_QUOTED;
		}
	}

	/**
	 * Get next state from processing double quoted state
	 * 
	 * @param c
	 *            The input character
	 * @param buffer
	 *            The string builder
	 * @param result
	 *            The tokenized string list
	 * @return The next state
	 * @throws ShellException
	 */
	public ParsingState parseDoubleQuotedStateAndGetNextState(char c,
			StringBuilder buffer, ArrayList<String> result)
			throws ShellException {
		switch (c) {
		case DOUBLE_QUOTE:
			result.add(buffer.toString());
			buffer.setLength(0); // empty buffer
			return ParsingState.EXPECTING_SPACE;
		case BACK_QUOTE:
			// temporarily add current buffer to result array, need to
			// take out again later to merge with backquoted content
			result.add(buffer.toString());
			buffer.setLength(0); // empty buffer
			return ParsingState.BACK_QUOTED_IN_DOUBLE_QUOTED;
		default:
			buffer.append(c);
			return ParsingState.DOUBLE_QUOTED;
		}
	}

	/**
	 * Get next state from processing back quoted state
	 * 
	 * @param c
	 *            The input character
	 * @param buffer
	 *            The string builder
	 * @param result
	 *            The tokenized string list
	 * @return The next state
	 * @throws ShellException
	 */
	public ParsingState parseBackQuotedStateAndGetNextState(char c,
			StringBuilder buffer, ArrayList<String> result)
			throws AbstractApplicationException, ShellException {
		switch (c) {
		case BACK_QUOTE:
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			parseAndEvaluate(buffer.toString(), outputStream);
			String singleLineOutput = outputStream.toString()
					.replaceAll("[\\n\\r]+$", "").replaceAll("\\n", " ")
					.replaceAll("\\r", "");
			String[] substitution = singleLineOutput.split("\\s+");
			result.addAll(Arrays.asList(substitution));

			buffer.setLength(0); // empty buffer
			return ParsingState.EXPECTING_SPACE;
		default:
			buffer.append(c);
			return ParsingState.BACK_QUOTED;
		}
	}

	/**
	 * Get next state from processing back quoted in double quoted state
	 * 
	 * @param c
	 *            The input character
	 * @param buffer
	 *            The string builder
	 * @param result
	 *            The tokenized string list
	 * @return The next state
	 * @throws ShellException
	 */
	public ParsingState parseBackQuotedInDoubleQuotedStateAndGetNextState(
			char c, StringBuilder buffer, ArrayList<String> result)
			throws AbstractApplicationException, ShellException {
		switch (c) {
		case BACK_QUOTE:
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			parseAndEvaluate(buffer.toString(), outputStream);
			String singleLineOutput = outputStream.toString()
					.replaceAll("[\\n\\r]+$", "").replaceAll("\\n", " ")
					.replaceAll("\\r", "");
			// take out the last result
			String previous = result.get(result.size() - 1);
			result.remove(result.size() - 1);
			buffer.setLength(0);
			buffer.append(previous);
			buffer.append(singleLineOutput);

			return ParsingState.DOUBLE_QUOTED;
		default:
			buffer.append(c);
			return ParsingState.BACK_QUOTED_IN_DOUBLE_QUOTED;
		}
	}

	/**
	 * Get next state from processing space state
	 * 
	 * @param c
	 *            The input character
	 * @param buffer
	 *            The string builder
	 * @param result
	 *            The tokenized string list
	 * @return The next state
	 * @throws ShellException
	 */
	public ParsingState parseExpectingSpaceAndGetNextState(char c,
			StringBuilder buffer, ArrayList<String> result)
			throws ShellException {
		if (WHITESPACES.contains(c)) {
			return ParsingState.NORMAL;
		} else if (DELIMITERS.contains(c)) {
			// add the delimiter itself
			result.add(Character.toString(c));
			return ParsingState.NORMAL;
		} else {
			throw new ShellException("Unexpected character: " + c);
		}
	}

	/**
	 * Interpreting asterisk in an unquoted part TODO : Can be refactored to be
	 * independent class
	 * 
	 * @param pattern
	 *            The input string pattern
	 * @return The complete file list
	 * @throws IOException
	 */
	public List<String> glob(String oldPattern) throws IOException {
		String pattern = oldPattern;
		pattern = AppUtility.convertToAbsPath(pattern);
		ArrayList<String> files = new ArrayList<String>();
		ArrayList<String> filesLastRound;

		int firstWildCard = pattern.indexOf(WILDCARD_CHAR);
		if (firstWildCard == -1) {
			files.add(oldPattern);
			return files;
		}
		String separator = Pattern.quote(File.separator);
		// find the longest path without a wild card char
		int lastSlash = 0;
		for (int i = 0; i < firstWildCard; i++) {
			if (pattern.charAt(i) == File.separatorChar) {
				lastSlash = i;
			}
		}
		// this will be where we start the search
		files.add(pattern.substring(0, lastSlash));
		String[] splittedComponents = pattern.substring(lastSlash + 1).split(
				separator);
		for (String pathComponent : splittedComponents) {
			filesLastRound = new ArrayList<String>(files);
			files = new ArrayList<String>();
			if (pathComponent.indexOf(WILDCARD_CHAR) == -1) {
				// no wild card, append to all candidates
				for (String partial : filesLastRound) {
					files.add(partial + File.separator + pathComponent);
				}
			} else {
				// has wild cards
				String regex = pathComponent.replaceAll("\\*", ".*");
				// System.out.println(regex);
				for (String partial : filesLastRound) {
					File file = new File(partial);
					if (file.isDirectory()) {
						// only proceed if it is a directory
						for (String memberFile : file.list()) {
							if (memberFile.matches(regex)) {
								files.add(partial + File.separator + memberFile);
							}
						}
					}
				}
			}
		}
		return files;
	}
}
