package sg.edu.nus.comp.cs4218.impl.cmd;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import sg.edu.nus.comp.cs4218.Command;
import sg.edu.nus.comp.cs4218.exception.AbstractApplicationException;
import sg.edu.nus.comp.cs4218.exception.ShellException;

public class Pipe implements Command {

	private ByteArrayOutputStream stdout = null;
	private Call fromCmd = null;
	private Call toCmd = null;
	private Pipe fromPipe = null;
	private Command fromRedirect = null;

	/**
	 * Pipe constructor, binds the output of the left part to the input of the
	 * right part.
	 * 
	 * @param fromPipe
	 *            The output of left command
	 * @param toCmd
	 *            The input of the right command
	 * 
	 *            public Pipe(Pipe fromPipe, Call toCmd) { this.fromPipe =
	 *            fromPipe; this.toCmd = toCmd; }
	 */

	/**
	 * Pipe constructor that bind two of call commands
	 * 
	 * @param fromCmd
	 *            The left output call command
	 * @param toCmd
	 *            The right input call command
	 */
	public Pipe(Call fromCmd, Call toCmd) {
		prepareCalls(fromCmd, toCmd);
	}

	public Pipe(Command commandStack, Call toCmd) {
		if (commandStack instanceof Pipe) {
			this.fromPipe = (Pipe) commandStack;
			this.toCmd = toCmd;
		} else if (commandStack instanceof Call) {
			prepareCalls((Call) commandStack, toCmd);
		} else if (commandStack instanceof RedirectLeft) {
			this.fromRedirect = (RedirectLeft) commandStack;
			this.toCmd = toCmd;
		} else if (commandStack instanceof RedirectRight) {
			this.fromRedirect = (RedirectRight) commandStack;
			this.toCmd = toCmd;
		}
	}

	/**
	 * Set the call commands
	 * 
	 * @param fromCmd
	 *            The left output call command
	 * @param toCmd
	 *            The right input call command
	 */
	private void prepareCalls(Call fromCmd, Call toCmd) {
		this.fromCmd = fromCmd;
		this.toCmd = toCmd;
	}

	@Override
	public void evaluate(InputStream stdin, OutputStream stdout)
			throws AbstractApplicationException, ShellException {
		InputStream actualStdin = stdin;
		OutputStream actualStdout = new ByteArrayOutputStream();
		if (fromCmd != null) {
			fromCmd.evaluate(actualStdin, actualStdout);
		} else if (fromPipe != null) {
			fromPipe.evaluate(actualStdin, actualStdout);
			actualStdin = new ByteArrayInputStream(
					((ByteArrayOutputStream) fromPipe.getStdOut())
							.toByteArray());
		} else if (fromRedirect != null) {
			fromRedirect.evaluate(actualStdin, actualStdout);
		} else {
			throw new ShellException("Invalid pipe operation!");
		}
		actualStdin = new ByteArrayInputStream(
				((ByteArrayOutputStream) actualStdout).toByteArray());
		toCmd.evaluate(actualStdin, stdout);
		if (stdout instanceof ByteArrayOutputStream) {
			this.stdout = (ByteArrayOutputStream) stdout;
		}

	}

	@Override
	public void terminate() {
		if (fromCmd != null) {
			fromCmd.terminate();
		}
		if (toCmd != null) {
			toCmd.terminate();
		}
		if (fromPipe != null) {
			fromPipe.terminate();
		}
	}

	/**
	 * Get standard output
	 * 
	 * @return The stdout
	 */
	public ByteArrayOutputStream getStdOut() {
		return stdout;
	}
}
