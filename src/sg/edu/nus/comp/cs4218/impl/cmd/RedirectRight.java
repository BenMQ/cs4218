package sg.edu.nus.comp.cs4218.impl.cmd;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import sg.edu.nus.comp.cs4218.Command;
import sg.edu.nus.comp.cs4218.exception.AbstractApplicationException;
import sg.edu.nus.comp.cs4218.exception.ShellException;
import sg.edu.nus.comp.cs4218.impl.util.AppUtility;

public class RedirectRight implements Command {

	private Command commandStack = null;
	private String fileName = null;

	/**
	 * This class handles the processing of the command 'fromCmd > fileName'
	 * which means redirect the output of fromCmd to the input file fileName
	 * 
	 * @param fromCmd
	 *            the left hand side of the '>' operator
	 * @param fileName
	 *            the right hand side of the '>' operator
	 */
	public RedirectRight(Command commandStack, String fileName) {
		this.commandStack = commandStack;
		this.fileName = fileName;
	}

	@Override
	public void evaluate(InputStream stdin, OutputStream stdout)
			throws AbstractApplicationException, ShellException {
		if ((fileName != null && fileName.length() < 1) || fileName == null) {
			throw new ShellException("invalid file to redirect output to.");
		}
		if (commandStack instanceof RedirectRight) {
			throw new ShellException(
					"Too many output redirect files specified.");
		}
		commandStack.evaluate(stdin, stdout);
		String absFilePath = AppUtility.convertToAbsPath(fileName);
		File file = new File(absFilePath);
		boolean errorFlag = false;
		try {
			FileWriter fileWriter = new FileWriter(file);
			fileWriter.write(((ByteArrayOutputStream) stdout).toString());
			fileWriter.flush();
			fileWriter.close();
		} catch (IOException e) {
			errorFlag = true;
		}
		if (errorFlag) {
			throw new ShellException("Error writing to file: " + absFilePath);
		}
		// Clear output stream content because already written to file
		if (stdout instanceof ByteArrayOutputStream) {
			((ByteArrayOutputStream) stdout).reset();
		}

	}

	@Override
	public void terminate() {
		if (commandStack != null) {
			commandStack.terminate();
		}
	}

}
