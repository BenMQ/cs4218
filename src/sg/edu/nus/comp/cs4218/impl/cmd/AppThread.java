package sg.edu.nus.comp.cs4218.impl.cmd;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import sg.edu.nus.comp.cs4218.Application;
import sg.edu.nus.comp.cs4218.exception.AbstractApplicationException;

public class AppThread extends Thread {

	private boolean stopped = true;

	private String[] args = null;
	public Application app = null;
	private InputStream stdin = null;
	private OutputStream stdout = null;

	public AppThread(Application app, InputStream stdin, OutputStream stdout,
			String... args) {
		this.app = app;
		if (args == null) {
			this.args = new String[] {};
		} else {
			this.args = args;
		}
		this.stdin = stdin;
		this.stdout = stdout;
	}

	public boolean isRunning() {
		return !stopped;
	}

	public InputStream getInputStream() {
		return stdin;
	}

	public OutputStream getOutputStream() {
		return stdout;
	}

	public void run() {
		try {
			stopped = false;
			app.run(args, stdin, stdout);
		} catch (AbstractApplicationException e) {
			if (stdout instanceof ByteArrayOutputStream) {
				((ByteArrayOutputStream) stdout).reset();
			}
			System.out.println(e.getMessage());
		} catch (NullPointerException e) {

		}
		stopped = true;
	}

}
