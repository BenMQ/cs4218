package sg.edu.nus.comp.cs4218.impl.cmd;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import sg.edu.nus.comp.cs4218.Command;
import sg.edu.nus.comp.cs4218.exception.AbstractApplicationException;
import sg.edu.nus.comp.cs4218.exception.ShellException;
import sg.edu.nus.comp.cs4218.impl.util.AppUtility;

public class RedirectLeft implements Command {

	private Command commandStack = null;
	private String fileName = null;

/**
	 * This class handles the processing of the command 'fromCmd < fileName'
	 * which means redirect the contents of fileName to the inputstream of commandStack
	 * @param commandStack the left hand side of the '<' operator
	 * @param fileName the right hand side of the '<' operator
	 */
	public RedirectLeft(Command commandStack, String fileName) {
		this.commandStack = commandStack;
		this.fileName = fileName;
	}

	@Override
	public void evaluate(InputStream stdin, OutputStream stdout)
			throws AbstractApplicationException, ShellException {
		if (commandStack instanceof RedirectLeft) {
			throw new ShellException("Too many input redirect files specified.");
		}
		if ((fileName != null && fileName.length() < 1) || fileName == null) {
			commandStack.evaluate(null, stdout);
		} else {
			String absFilePath = AppUtility.convertToAbsPath(fileName);
			byte[] byteArr = null;
			boolean errorFlag = false;
			try {
				byteArr = Files.readAllBytes(Paths.get(absFilePath));
			} catch (IOException e) {
				errorFlag = true;
			}
			if (errorFlag) {
				throw new ShellException("Error reading from file: "
						+ absFilePath);
			}
			commandStack.evaluate(new ByteArrayInputStream(byteArr), stdout);
		}

	}

	@Override
	public void terminate() {
		if (commandStack != null) {
			commandStack.terminate();
		}
	}

}
