package sg.edu.nus.comp.cs4218.impl;

import java.io.ByteArrayOutputStream;
import java.util.Scanner;

import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.exception.AbstractApplicationException;
import sg.edu.nus.comp.cs4218.exception.ShellException;
import sg.edu.nus.comp.cs4218.impl.cmd.Evaluator;

public class ShellCLI {

	private static Evaluator evaluator = null;

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		evaluator = new Evaluator();
		System.out.print(Environment.currentDirectory + "$ ");
		
		while (scanner.hasNextLine()) {
			String userInput = scanner.nextLine();
			if (userInput.equals("quit")) {
				scanner.close();
				return;
			}
			processUserInput(userInput);
			System.out.print(Environment.currentDirectory + "$ ");
		}
		
		scanner.close();
	}

	private static void processUserInput(String userInput) {
		try {
			ByteArrayOutputStream byteArrOutputStream = new ByteArrayOutputStream();
			evaluator.parseAndEvaluate(userInput, byteArrOutputStream);
			if (byteArrOutputStream != null && byteArrOutputStream.size() > 0) {
				System.out.print(byteArrOutputStream);
			}
		} catch (AbstractApplicationException e) {
			System.out.println(e.getMessage());
		} catch (ShellException e) {
			System.out.println(e.getMessage());
		}
	}

}
