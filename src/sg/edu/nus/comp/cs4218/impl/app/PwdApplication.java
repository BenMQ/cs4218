package sg.edu.nus.comp.cs4218.impl.app;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

import sg.edu.nus.comp.cs4218.Application;
import sg.edu.nus.comp.cs4218.exception.PwdException;
import sg.edu.nus.comp.cs4218.impl.util.AppUtility;

public class PwdApplication implements Application {

	/**
	 * {@inheritDoc}
	 * 
	 * @throws PwdException
	 *             if invalid arguments are given
	 */
	@Override
	public void run(String[] args, InputStream stdin, OutputStream stdout)
			throws PwdException {
		validateArguments(args);
		PrintWriter stdoutWriter = new PrintWriter(stdout);
		stdoutWriter.println(new File(AppUtility.getCWD()).getAbsolutePath());
		stdoutWriter.flush();
	}

	/**
	 * Validate all given arguments. Currently no arguments are accepted
	 * 
	 * @param args
	 *            list of arguments to validate
	 * @throws PwdException
	 *             if argument is illegal
	 */
	public void validateArguments(String... args) throws PwdException {
		if (args.length > 0) {
			throw new PwdException("too many arguments");
		}
	}

}
