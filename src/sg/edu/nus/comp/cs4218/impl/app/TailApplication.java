package sg.edu.nus.comp.cs4218.impl.app;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

import sg.edu.nus.comp.cs4218.Application;
import sg.edu.nus.comp.cs4218.exception.TailException;
import sg.edu.nus.comp.cs4218.impl.util.AppUtility;

public class TailApplication implements Application {

	private static final String LINES_OPTION = "-n";
	private int linesToPrint = 10;
	private PrintWriter printWriter = null;

	@Override
	public void run(String[] args, InputStream stdin, OutputStream stdout)
			throws TailException {
		printWriter = new PrintWriter(stdout);
		initialize();
		if (args == null || args.length == 0) {
			if (stdin == null) {
				throw new TailException("stdin is null");
			}
			Scanner scanner = new Scanner(stdin);
			printLines(scanner);
			scanner.close();
		} else if (args.length == 1) {
			processOneArg(args);
		} else if (args.length == 2) {
			processTwoArg(stdin, args);
		} else if (args.length == 3) {
			processThreeArg(args);
		} else {// args.length != 3
			throw new TailException(
					"invalid params. Usage: tail [OPTIONS] [FILE]");
		}
		printWriter.flush();
	}

	private void processThreeArg(String... args) throws TailException {
		// check for invalid tokens -> [OPTIONS] or [FILE]
		if (args[0].equals(LINES_OPTION)) {
			String expectedNumStr = args[1];
			boolean isValidNum = true;
			try {
				linesToPrint = Integer.parseInt(expectedNumStr);
			} catch (NumberFormatException e) {
				isValidNum = false;
			}
			if (!isValidNum) {
				throw new TailException("\"" + expectedNumStr
						+ "\" is not a valid integer");
			}
			String filePathStr = AppUtility.convertToAbsPath(args[2]);
			File file = new File(filePathStr);
			if (file.exists()) {
				if (file.isDirectory()) {
					throw new TailException(file.getName() + " is a directory");
				} else {
					Scanner scanner = null;
					try {
						scanner = new Scanner(file);
						printLines(scanner);
						scanner.close();
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}
				}
			} else {
				throw new TailException(file.getName() + " cannot be found.");
			}
		} else {// args[0] != LINES_OPTION
			throw new TailException(
					"invalid params. Usage: tail [OPTIONS] [FILE]");
		}
	}

	private void processTwoArg(InputStream stdin, String... args)
			throws TailException {
		// check for [OPTIONS] or [FILE]
		if (args[0].equals(LINES_OPTION)) {
			String expectedNumStr = args[1];
			boolean isValidNum = true;
			try {
				linesToPrint = Integer.parseInt(expectedNumStr);
			} catch (NumberFormatException e) {
				isValidNum = false;
			}
			if (isValidNum) {
				if (stdin == null) {
					throw new TailException("stdin is null");
				}
				Scanner scanner = new Scanner(stdin);
				printLines(scanner);
				scanner.close();
			} else {// !isValidNum
				throw new TailException("\"" + expectedNumStr
						+ "\" is not a valid integer");
			}
		} else {
			throw new TailException(
					"invalid params. Usage: tail [OPTIONS] [FILE]");
		}
	}

	private void processOneArg(String... args) throws TailException {
		// check for [OPTIONS] or [FILE]
		if (args[0].equals(LINES_OPTION)) {
			throw new TailException(LINES_OPTION
					+ " is specified with no valid line number.");
		} else {
			// [FILE] option specified
			String filePathStr = args[0];
			filePathStr = AppUtility.convertToAbsPath(filePathStr);
			File file = new File(filePathStr);
			if (file.exists()) {
				if (file.isDirectory()) {
					throw new TailException(file.getName() + " is a directory");
				} else {
					Scanner scanner = null;
					try {
						scanner = new Scanner(file);
						printLines(scanner);
						scanner.close();
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}

				}
			} else {
				throw new TailException(file.getName() + " cannot be found.");
			}
		}
	}

	private void initialize() {
		linesToPrint = 10;
	}

	private void printLines(Scanner scanner) throws TailException{
		if (linesToPrint < 0) {
			throw new TailException("Negative lines to print.");
		}
		ArrayList<String> tempArrayList = new ArrayList<String>();
		while (scanner.hasNextLine()) {
			tempArrayList.add(scanner.nextLine());
		}
		int startIdx = 0;
		if (tempArrayList.size() > linesToPrint) {
			startIdx = tempArrayList.size() - linesToPrint;
		}
		for (int i = startIdx; i < tempArrayList.size(); i++) {
			if(i+1 == tempArrayList.size()){
				printWriter.print(tempArrayList.get(i));
			} else {
				printWriter.println(tempArrayList.get(i));
			}
			
		}
	}

}
