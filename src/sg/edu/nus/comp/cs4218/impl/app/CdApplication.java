package sg.edu.nus.comp.cs4218.impl.app;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;

import sg.edu.nus.comp.cs4218.Application;
import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.exception.CdException;
import sg.edu.nus.comp.cs4218.impl.util.AppUtility;

public class CdApplication implements Application {

	/**
	 * Set current working directory.
	 * 
	 * @param absPath
	 *            The absolute path.
	 * @return True if setting of CWD is successful, false otherwise.
	 */
	private boolean setCWD(String absPath) {

		Environment.currentDirectory = absPath;
		if (!AppUtility.getCWD().equals(absPath)) {
			return false;
		}

		return true;
	}

	/**
	 * Check whether the path string is valid.
	 * 
	 * @param absPath
	 *            Path string.
	 * @return Absolute path, otherwise return null.
	 * 
	 *         private String getValidPath(String absPath) {
	 * 
	 *         File file = new File(absPath);
	 * 
	 *         if (file.isDirectory()) { try { return file.getCanonicalPath(); }
	 *         catch (IOException e) { e.printStackTrace(); } }
	 * 
	 *         return null; }
	 */

	/**
	 * Runs application with specified input data and specified output stream.
	 */
	public void run(String[] args, InputStream stdin, OutputStream stdout)
			throws CdException {

		// If no additional argument for cd, do nothing
		if (args == null || args.length != 1) {
			return;
		}
		
		File file = new File(args[0]);
		String absPath = args[0];
		// Convert to absolute path
		if(file.isAbsolute()){
			throw new CdException("Path is not relative.");
		} else {
			absPath = AppUtility.convertToAbsPath(args[0]);
		}

		// Check valid path
		if (!AppUtility.isValidPath(absPath)) {
			throw new CdException("Can't find the file directory.");
		}

		// Set the absolute path as current working directory
		if (!this.setCWD(absPath)) {
			throw new CdException("Set current working directory failed.");
		}
	}
}
