package sg.edu.nus.comp.cs4218.impl.app;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;

import sg.edu.nus.comp.cs4218.Application;
import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.exception.LsException;
import sg.edu.nus.comp.cs4218.impl.util.AppUtility;
import sg.edu.nus.comp.cs4218.impl.util.StringUtilities;

public class LsApplication implements Application {

	/**
	 * Delimiter between each file or path.
	 */
	private static final String FILE_DELIMITER = "\t";
	/**
	 * Prefix for hidden files, which should not be printed.
	 */
	private static final String HIDDEN_PREFIX = ".";
	/**
	 * Store list of paths to operate on.
	 */
	public File targetPath;

	/**
	 * {@inheritDoc}
	 * 
	 * @throws LsException
	 *             if the specified paths do not exist or if invalid options are
	 *             found.
	 */
	@Override
	public void run(String[] args, InputStream stdin, OutputStream stdout)
			throws LsException {
		processArguments(args);
		if (targetPath == null) {
			targetPath = new File(AppUtility.getCWD());
		}
		PrintWriter stdoutWriter = new PrintWriter(stdout);
		ArrayList<String> contents = generateContentsFrom(targetPath);
		String[] contentsArray = contents.toArray(new String[contents.size()]);
		stdoutWriter.println(StringUtilities.joinArray(contentsArray,
				FILE_DELIMITER));
		stdoutWriter.flush();

	}

	public ArrayList<String> generateContentsFrom(File path) throws LsException{
		ArrayList<String> contents = new ArrayList<String>();
		if(path == null || path.list()==null){
			throw new LsException("File is null");
		}
		for (String child : path.list()) {
			if (!child.startsWith(HIDDEN_PREFIX)) {
				contents.add(child);
			}
		}
		return contents;
	}

	/**
	 * Validate and process each given argument. Note that there are no legal
	 * arguments for <code>ls</code>.
	 * 
	 * @param args
	 *            list of arguments to process.
	 * @throws LsException
	 *             if a invalid or path option is encountered.
	 */
	public void processArguments(String... args) throws LsException {
		for (String arg : args) {
			if (arg.charAt(0) == '-' && arg.length() > 1) {
				throw new LsException("illegal option -- " + arg.substring(1));
			} else if (targetPath != null) {
				throw new LsException("Too many arguments");
			} else {
				File path = new File(arg);
				File currentDirectory = new File(Environment.currentDirectory);
				if (!path.isAbsolute()) {
					path = new File(currentDirectory, arg);
				}
				if (path.exists()) {
					targetPath = path;
				} else {
					throw new LsException(arg + ": No such file or directory");
				}
			}
		}
	}
}
