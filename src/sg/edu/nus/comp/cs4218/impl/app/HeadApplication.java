package sg.edu.nus.comp.cs4218.impl.app;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

import sg.edu.nus.comp.cs4218.Application;
import sg.edu.nus.comp.cs4218.exception.HeadException;
import sg.edu.nus.comp.cs4218.impl.util.AppUtility;

public class HeadApplication implements Application {

	private static final String LINES_OPTION = "-n";
	private int linesToPrint = 10;
	private int linesCounted = 0;
	private PrintWriter printWriter = null;

	@Override
	public void run(String[] args, InputStream stdin, OutputStream stdout)
			throws HeadException {
		printWriter = new PrintWriter(stdout);
		initialize();
		if (args == null || args.length == 0) {
			if (stdin == null) {
				throw new HeadException("stdin is null");
			}
			Scanner scanner = new Scanner(stdin);
			printLines(scanner);
			scanner.close();
		} else if (args.length == 1) {
			processOneArg(args[0]);
		} else if (args.length == 2) {
			processTwoArg(stdin, args);

		} else if (args.length == 3) {
			processThreeArg(args);
		} else {// invalid args[] length
			throw new HeadException(
					"invalid params. Usage: tail [OPTIONS] [FILE]");
		}
		printWriter.flush();
	}

	private void processThreeArg(String... args) throws HeadException {
		// check for invalid tokens -> [OPTIONS] or [FILE]
		if (args[0].equals(LINES_OPTION)) {
			String expectedNumStr = args[1];
			boolean isValidNum = true;
			try {
				linesToPrint = Integer.parseInt(expectedNumStr);
			} catch (NumberFormatException e) {
				isValidNum = false;
			}
			if (!isValidNum) {
				throw new HeadException("\"" + expectedNumStr
						+ "\" is not a valid integer");
			}
			String filePathStr = AppUtility.convertToAbsPath(args[2]);
			File file = new File(filePathStr);
			if (file.exists()) {
				if (file.isDirectory()) {
					throw new HeadException(file.getName() + " is a directory");
				} else {
					Scanner scanner = null;
					try {
						scanner = new Scanner(file);
						printLines(scanner);
						scanner.close();
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}

				}
			}
			else {
				throw new HeadException(file.getName() + " cannot be found.");
			}

		} else {// args[0] != LINES_OPTION
			throw new HeadException(
					"invalid params. Usage: tail [OPTIONS] [FILE]");
		}
	}

	private void processTwoArg(InputStream stdin, String... args)
			throws HeadException {
		// check for [OPTIONS] or [FILE]
		if (args[0].equals(LINES_OPTION)) {
			String expectedNumStr = args[1];
			boolean isValidNum = true;
			try {
				linesToPrint = Integer.parseInt(expectedNumStr);
			} catch (NumberFormatException e) {
				isValidNum = false;
			}
			if (isValidNum) {
				if (stdin == null) {
					throw new HeadException("stdin is null");
				}
				Scanner scanner = new Scanner(stdin);
				printLines(scanner);
				scanner.close();

			} else {// !isValidNum
				throw new HeadException("\"" + expectedNumStr
						+ "\" is not a valid integer");
			}
		} else {
			throw new HeadException(
					"invalid params. Usage: tail [OPTIONS] [FILE]");
		}
	}

	private void processOneArg(String arg) throws HeadException {
		// check for [OPTIONS] or [FILE]
		if (arg.equals(LINES_OPTION)) {
			throw new HeadException(LINES_OPTION
					+ " is specified with no valid line number.");
		} else {
			// [FILE] option specified
			String filePathStr = arg;
			filePathStr = AppUtility.convertToAbsPath(filePathStr);
			File file = new File(filePathStr);
			if (file.exists()) {
				if (file.isDirectory()) {
					throw new HeadException(file.getName() + " is a directory");
				} else {
					Scanner scanner = null;
					try {
						scanner = new Scanner(file);
						printLines(scanner);
						scanner.close();
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}

				}
			} else {
				throw new HeadException(file.getName() + " cannot be found.");
			}
		}
	}

	private void initialize() {
		linesToPrint = 10;
		linesCounted = 0;
	}

	private void printLines(Scanner scanner) throws HeadException {
		if (linesToPrint < 0) {
			throw new HeadException("Lines to print is negative");
		}
		while (scanner.hasNextLine()) {
			String lineToPrint = scanner.nextLine();
			if (linesCounted == linesToPrint) {
				break;
			}
			if(scanner.hasNextLine()) {
				printWriter.println(lineToPrint);
			}else{
				printWriter.print(lineToPrint);
			}
			
			linesCounted++;
		}
	}

}
