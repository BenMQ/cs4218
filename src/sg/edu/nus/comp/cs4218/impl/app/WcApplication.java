package sg.edu.nus.comp.cs4218.impl.app;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

import sg.edu.nus.comp.cs4218.Application;
import sg.edu.nus.comp.cs4218.exception.WcException;
import sg.edu.nus.comp.cs4218.impl.util.AppUtility;

public class WcApplication implements Application {

	private static final String CHAR_OPTION = "-m";
	private static final String WORD_OPTION = "-w";
	private static final String NEWLINE_OPTION = "-l";

	private boolean lineCountFlag = false;
	private boolean wordCountFlag = false;
	private boolean charCountFlag = false;

	private boolean filesFlag = false;
	private int totalLineCount = 0;
	private int totalWordCount = 0;
	private int totalCharCount = 0;

	private boolean isStdInNull = false;
	
	private ArrayList<String> filepathList = new ArrayList<String>();

	@Override
	public void run(String[] args, InputStream stdin, OutputStream stdout)
			throws WcException {
		// System.setProperty("line.separator","\n");
		isStdInNull = stdin == null;
		initialize();
		// Arguments handling
		processArguments(args);
		if (!lineCountFlag && !wordCountFlag && !charCountFlag) {
			lineCountFlag = true;
			wordCountFlag = true;
			charCountFlag = true;
		}
		PrintWriter printWriter = new PrintWriter(stdout);
		if (filesFlag) {
			processFiles(printWriter);
		} else {
			// No files parameter, default to stdin
			if (stdin == null) {
				throw new WcException("no files param and stdin is null");
			}
			Scanner scanner = new Scanner(stdin);
			scanner = scanner.useDelimiter("\\A");
			String stdinStr = scanner.hasNext() ? scanner.next() : "";
			scanner.close();
			String result = process(stdinStr);
			printWriter.println(result);

		}
		printWriter.flush();
	}

	/**
	 * @param printWriter
	 *            the PrintWriter object to print to stdout
	 * @throws WcException
	 */
	private void processFiles(PrintWriter printWriter) throws WcException {
		for (int i = 0; i < filepathList.size(); i++) {
			String filepathStr = AppUtility.convertToAbsPath(filepathList
					.get(i));
			String answer = "";
//			if (filepathStr == null) {
//				continue;
//			}
			File file = new File(filepathStr);
			if (file.exists()) {
				if (file.isDirectory()) {
					throw new WcException(file.getName() + ": is a directory");
				} else {
					// f is a file, not directory
					Scanner scanner = null;
					try {
						scanner = new Scanner(file);
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}
					if (scanner == null) {
						throw new WcException(file.getName()
								+ ": is not a file or directory");
					} else {
						answer = process(scanner.hasNext() ? scanner
								.useDelimiter("\\A").next() : "");
					}
				}
			} else {
				// file does not exists
				throw new WcException(file.getName()
						+ ": is not a file or directory");
			}

			String result = answer;
			printWriter.println(result);
		}
		if (filepathList.size() > 1 && printWriter != null) {
			printWriter.println(totalCount());
		}
	}

	/**
	 * @param args
	 *            arguments passed to WcApplication
	 * @throws WcException
	 */
	private void processArguments(String... args) throws WcException {
		if (args == null) {
			return;
		}
		for (int i = 0; i < args.length; i++) {
			String arg = args[i];
			
			if (!processOptionParam(arg)) {
				processFileParam(arg);
			}
		}
	}

	/**
	 * Resets variables used, intended for reusability of object
	 */
	private void initialize() {
		charCountFlag = false;
		wordCountFlag = false;
		lineCountFlag = false;
		filesFlag = false;
		totalCharCount = 0;
		totalWordCount = 0;
		totalLineCount = 0;
		filepathList = new ArrayList<String>();
	}

	/**
	 * @param arg
	 *            the argument parameter to process
	 * @return true if arg is options parameter and false if it is a file
	 *         parameter
	 * @throws WcException
	 */
	private boolean processOptionParam(String arg) throws WcException {
		if((arg == null || arg.trim().length() == 0) && isStdInNull){
			throw new WcException("Invalid arg");
		}
		if (arg.equals(CHAR_OPTION)) {
			charCountFlag = true;
		} else if (arg.equals(WORD_OPTION)) {
			wordCountFlag = true;
		} else if (arg.equals(NEWLINE_OPTION)) {
			lineCountFlag = true;
		} else {
			if (arg.charAt(0) == '-') {
				String optionStr = "";
				if (arg.length() > 1) {
					optionStr = arg.substring(1);
				}
				throw new WcException("invalid option -- '" + optionStr
						+ "' , " + arg);
			} else {
				return false;
			}
		}
		return true;
	}

	/**
	 * @param arg
	 *            the file parameter to process
	 * @throws WcException
	 */
	private void processFileParam(String arg) throws WcException {
		filepathList.add(arg);
		filesFlag = true;
	}

	/**
	 * @param input
	 *            the String to process
	 * @return the resulting string to show on the CLI
	 */
	private String process(String input) {
		int wcount = 0;
		int lcount = 0;
		int ccount = 0;
		String result = "";
		if (lineCountFlag) {
			lcount = input.split("\n").length;
			result += "" + lcount;
			totalLineCount += lcount;
		}
		if (wordCountFlag) {
			String[] lines = input.split("\n");
			for (String line : lines) {
				wcount += line.split(" ").length;
			}
			result += "\t" + wcount;
			totalWordCount += wcount;
		}
		if (charCountFlag) {
			ccount = input.length();
			result += "\t" + ccount;
			totalCharCount += ccount;
		}
		return result.trim();
	}

	/**
	 * @return the formatted result of the total count
	 */
	private String totalCount() {
		String result = "";
		if (lineCountFlag) {
			result += "" + totalLineCount;
		}
		if (wordCountFlag) {
			result += "\t" + totalWordCount;
		}
		if (charCountFlag) {
			result += "\t" + totalCharCount;
		}
		return result.trim() + "\ttotal";
	}

}
