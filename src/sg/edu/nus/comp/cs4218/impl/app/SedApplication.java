package sg.edu.nus.comp.cs4218.impl.app;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

import sg.edu.nus.comp.cs4218.Application;
import sg.edu.nus.comp.cs4218.exception.SedException;
import sg.edu.nus.comp.cs4218.impl.util.AppUtility;

public class SedApplication implements Application {

	private static final String FIRST_TOKEN = "s";
	private static final String ALL_TOKEN = "g";
	private static final char[] REGEX_CHARS = { '\\', '^', '$', '.', '|', '?',
			'*', '+', '(', ')', '[', ']', '{', '}' };

	@Override
	public void run(String[] args, InputStream stdin, OutputStream stdout)
			throws SedException {
		if (stdout == null) {
			throw new SedException("stdout is null");
		}
		if (args == null) {
			throw new SedException("invalid number of params.");
		}

		PrintWriter printWriter = new PrintWriter(stdout);
		if (args.length == 1 || args.length == 2) {
			if (args.length == 1 && stdin == null) {
				throw new SedException("stdin is null");
			}
			Scanner scanner = null;
			if (args.length == 1) {
				scanner = new Scanner(stdin);
			}
			if (args.length == 2) {
				File file = getFile(args[1]);
				try {
					scanner = new Scanner(file);
				} catch (FileNotFoundException e) {
					throw new SedException("File is not found");
				}
			}
			processReplacement(args, printWriter, scanner);
		} else {
			throw new SedException("invalid number of params.");
		}
	}

	private void processReplacement(String[] args, PrintWriter printWriter,
			Scanner scanner) throws SedException {
		String replacementStr = args[0];
		char lastChar = replacementStr.charAt(replacementStr.length()-1);
		if(lastChar != 'g' && lastChar != replacementStr.charAt(1)){
			throw new SedException("Invalid format");
		}
		char replacementChar = replacementStr.charAt(1);
		String rCharStr = "" + replacementChar;
		for (char regexChar : REGEX_CHARS) {
			if (regexChar == replacementChar) {
				rCharStr = "\\" + replacementChar;
				break;
			}
		}
		String[] replacementStrArr = replacementStr.split(rCharStr);
		if (replacementStrArr.length == 3) {
			if (replacementStrArr[0].equals(FIRST_TOKEN)) {
				String regex = replacementStrArr[1];
				String replacement = replacementStrArr[2];
				while (scanner.hasNextLine()) {
					String line = scanner.nextLine();
					printWriter.println(line.replaceFirst(regex, replacement));
				}
			} else {
				throw new SedException("invalid REPLACEMENT format: "
						+ replacementStr);
			}

		} else if (replacementStrArr.length == 4) {
			if (replacementStrArr[0].equals(FIRST_TOKEN)
					&& replacementStrArr[3].equals(ALL_TOKEN)) {
				String regex = replacementStrArr[1];
				String replacement = replacementStrArr[2];
				while (scanner.hasNextLine()) {
					String line = scanner.nextLine();
					printWriter.println(line.replaceAll(regex, replacement));
				}
			} else {
				throw new SedException("invalid REPLACEMENT format: "
						+ replacementStr);
			}
		} else {
			throw new SedException("invalid REPLACEMENT format: "
					+ replacementStr);
		}
		printWriter.flush();
		scanner.close();
	}

	/**
	 * @param filepath
	 *            the filepath string
	 * @return a valid file object
	 * @throws SedException
	 *             will be raised if file is a directory or not found
	 */
	private File getFile(String filepath) throws SedException {
		String filepathStr = AppUtility.convertToAbsPath(filepath);
		File file = new File(filepathStr);
		if (file.exists()) {
			if (file.isDirectory()) {
				throw new SedException(file.getName() + ": is a directory");
			}
		} else {
			throw new SedException(file.getName()
					+ ": is not a file or directory");
		}
		return file;
	}

}
