package sg.edu.nus.comp.cs4218.impl.app;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import sg.edu.nus.comp.cs4218.Application;
import sg.edu.nus.comp.cs4218.exception.CatException;
import sg.edu.nus.comp.cs4218.impl.util.AppUtility;

public class CatApplication implements Application {

	/**
	 * Read file content to string.
	 * 
	 * @param absFilePath
	 *            The file path.
	 * @return The file content string.
	 * @throws CatException
	 */
	private String getFileString(String absFilePath) throws CatException{
		boolean isError = false;
		try {

			String contentsStr = "";

			List<String> contents = Files.readAllLines(Paths.get(absFilePath),
					StandardCharsets.UTF_8);

			for (int i = 0; i < contents.size(); i++) {
				contentsStr += contents.get(i) + System.lineSeparator();
			}

			return contentsStr;

		} catch (IOException e) {
			isError = true;
		}
		if(isError) {
			throw new CatException("IOException is raised.");
		}
		return null;
	}

	/**
	 * Print the file contents to standard output.
	 * 
	 * @param pathList
	 *            The file path list.
	 * @param stdout
	 *            The standard output.
	 * @throws CatException
	 */
	private void printFileContents(List<String> pathList, OutputStream stdout) throws CatException{
		boolean isError = false;
		for (int i = 0; i < pathList.size(); i++) {
			String absPath = AppUtility.convertToAbsPath(pathList.get(i));
			File file = new File(absPath);

			String content; // content string
			if (file.isFile()) {
				content = this.getFileString(absPath);
			} else if (file.isDirectory()) {
				content = "cat : " + absPath + ": Is a directory"
						+ System.lineSeparator();
			} else {
				content = "cat : " + absPath + ": No such file or directory"
						+ System.lineSeparator();
			}

			try {
				stdout.write(content.getBytes());
			} catch (IOException e) {
				isError = true;
			}
			if(isError) {
				throw new CatException("IOException is raised");
			}
		}
	}

	/**
	 * No file specified, use standard input.
	 * 
	 * @param stdin
	 *            Standard input.
	 * @param stdout
	 *            Standard output.
	 * @throws CatException
	 */
	private void acceptStdInput(InputStream stdin, OutputStream stdout)
			throws CatException {
		int idx;
		if (stdin == null || stdout == null) {
			throw new CatException("stdin is null");
		}
		try {
			while ((idx = stdin.read()) != -1) {
				stdout.write(idx);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Runs application with specified input data and specified output stream.
	 */
	public void run(String[] args, InputStream stdin, OutputStream stdout)
			throws CatException {

		// check stdin and stdout
		if (stdout == null) {
			throw new CatException("No specified stdout.");
		}

		if ((args == null || args.length == 0) && stdin != null
				&& stdin.equals(System.in)) {
			throw new CatException("Cat: Unable to use System.in as stdin");
		}

		// Check valid no. of args
		if (args == null && stdin == null) {
			throw new CatException("stdin is null, no args specified");
		}

		// If no file is specified, using stdin
		if (args.length == 0) {
			this.acceptStdInput(stdin, stdout);
		} else {
			List<String> pathList = new ArrayList<String>();

			for (int i = 0; i < args.length; i++) {
				pathList.add(args[i]);
			}

			this.printFileContents(pathList, stdout);
		}
	}
}
