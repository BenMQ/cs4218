package sg.edu.nus.comp.cs4218.impl.app;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import sg.edu.nus.comp.cs4218.Application;
import sg.edu.nus.comp.cs4218.exception.EchoException;

public class EchoApplication implements Application {

	/**
	 * Write argument list to standard output.
	 * 
	 * @param argsList
	 *            The argument list.
	 * @param stdout
	 *            The standard output.
	 * @throws EchoException
	 */

	private void writeArgumentsToStdOut(List<String> argsList,
			OutputStream stdout) throws EchoException {

		for (int i = 0; i < argsList.size(); i++) {

			String content = argsList.get(i);

			// if (content.contains("\\") || content.contains("*")
			// || content.contains("(") || content.contains(")")) {
			// throw new EchoException("Argument contains special character.");
			// }

			if (!(i == argsList.size() - 1)) {
				content += " ";
			}

			try {
				stdout.write(content.getBytes());
				stdout.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Print new line to standard output.
	 * 
	 * @param stdout
	 *            The standard output.
	 * @throws EchoException
	 */
	private void printNewLineToStdOut(OutputStream stdout) {

		try {
			stdout.write(System.lineSeparator().getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Runs application with specified input data and specified output stream.
	 * 
	 * Note that : The space and quoting and special characters in arguments
	 * need to be cleared in parser. This class do not handle the issue.
	 * 
	 * TODO : Globbing with pattern 'abc*' and '\\' or '\'
	 */
	public void run(String[] args, InputStream stdin, OutputStream stdout)
			throws EchoException {

		// check stdout
		if (stdout == null) {
			throw new EchoException("No specified stdout.");
		}

		// Check valid no. of args
		if (args == null) {
			throw new EchoException("Argument is null.");
		}

		// If no argument need to print, write a new line to stdout.
		if (args.length == 0) {

			this.printNewLineToStdOut(stdout);
		} else {

			List<String> argsList = new ArrayList<String>();

			for (int i = 0; i < args.length; i++) {
				argsList.add(args[i]);
			}

			this.writeArgumentsToStdOut(argsList, stdout);
			this.printNewLineToStdOut(stdout);
		}
	}
}
