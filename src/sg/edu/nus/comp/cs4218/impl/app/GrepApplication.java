package sg.edu.nus.comp.cs4218.impl.app;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import sg.edu.nus.comp.cs4218.Application;
import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.exception.GrepException;
import sg.edu.nus.comp.cs4218.exception.LsException;

public class GrepApplication implements Application {

	/**
	 * Store list of paths to operate on.
	 */
	public ArrayList<File> targetPaths = new ArrayList<File>();
	/**
	 * Store the string pattern for grep.
	 */
	public Pattern pattern;

	@Override
	public void run(String[] args, InputStream stdin, OutputStream stdout)
			throws GrepException {
		// TODO Auto-generated method stub
		processArguments(args);

		PrintWriter stdoutWriter = new PrintWriter(stdout);
		if (targetPaths.size() > 0) {
			for (File path : targetPaths) {
				for (String line : generateContentsFrom(path)) {
					stdoutWriter.println(line);
				}
			}
		} else {
			if (stdin == null) {
				throw new GrepException("Grep: stdin is null");
			}
			for (String line : generateContentFromStdIn(stdin)) {
				stdoutWriter.println(line);
			}
		}
		stdoutWriter.flush();
	}

	/**
	 * Find the matches from a given file using the pattern. Expects pattern to
	 * be not null.
	 * 
	 * @param file
	 *            file to be matched
	 * @return list of matched lines
	 * @throws GrepException
	 *             if encounters I/O error when reading the file
	 */
	public ArrayList<String> generateContentsFrom(File file)
			throws GrepException {
		ArrayList<String> contents = new ArrayList<String>();
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(file));
			String line = reader.readLine();
			Matcher matcher;
			while (line != null) {
				matcher = pattern.matcher(line);
				if (matcher.find()) {
					contents.add(line);
				}
				line = reader.readLine();
			}
			reader.close();
		} catch (FileNotFoundException e) {
			throw (GrepException) new GrepException(file.getPath()
					+ ": No such file or directory").initCause(e);

		} catch (IOException e) {
			throw (GrepException) new GrepException("Encountered I/O error: "
					+ e.getMessage()).initCause(e);
		}
		return contents;
	}

	public ArrayList<String> generateContentFromStdIn(InputStream stdin)
			throws GrepException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(stdin));
		ArrayList<String> contents = new ArrayList<String>();
		Matcher matcher;
		try {
			String line = reader.readLine();
			while (line != null) {
				matcher = pattern.matcher(line);
				if (matcher.find()) {
					contents.add(line);
				}
				line = reader.readLine();
			}
			reader.close();
			return contents;
		} catch (IOException e) {
			throw (GrepException) new GrepException("Encountered I/O error: "
					+ e.getMessage()).initCause(e);
		}
	}

	/**
	 * Validate and process each given argument. Note that there are no legal
	 * arguments for <code>ls</code>.
	 * 
	 * @param args
	 *            list of arguments to process.
	 * @throws LsException
	 *             if a invalid or path option is encountered.
	 */
	public void processArguments(String... args) throws GrepException {
		targetPaths = new ArrayList<File>(args.length);
		for (String arg : args) {
			if (arg.length() > 0 && arg.charAt(0) == '-') {
				processOption(arg);
			} else if (pattern == null) {
				processPattern(arg);
			} else {
				processPath(arg);
			}
		}
	}

	/**
	 * Parse a JAVA regex pattern and store it.
	 * 
	 * @param pattern
	 *            pattern to parse
	 * @throws GrepException
	 *             if the pattern is invalid
	 */
	public void processPattern(String pattern) throws GrepException {
		try {
			this.pattern = Pattern.compile(pattern);
		} catch (PatternSyntaxException e) {
			throw (GrepException) new GrepException("Invalid Pattern - "
					+ e.getMessage()).initCause(e);
		}
	}

	/**
	 * Validate and process a given pathname. If the path name is relative,
	 * assume it is relative to the current working directory.
	 * 
	 * @param arg
	 *            relative or absolute path.
	 * @throws GrepException
	 *             if the given path does not exist.
	 */
	public void processPath(String arg) throws GrepException {
		File path = new File(arg);
		File currentDirectory = new File(Environment.currentDirectory);
		if (!path.isAbsolute()) {
			path = new File(currentDirectory, arg);
		}

		if (path.exists()) {
			targetPaths.add(path);
		} else {
			throw new GrepException(arg + ": No such file or directory");
		}
	}

	/**
	 * Validate and process a given option. Currently no options are legal.
	 * 
	 * @param arg
	 *            option for the command.
	 * @throws GrepException
	 *             if the given option is not legal.
	 */
	public void processOption(String arg) throws GrepException {
		if (arg.length() > 1) {
			throw new GrepException("illegal option -- " + arg.substring(1));
		}
	}

}
