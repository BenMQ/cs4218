package sg.edu.nus.comp.cs4218.impl.app;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.regex.PatternSyntaxException;

import sg.edu.nus.comp.cs4218.Application;
import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.exception.FindException;
import sg.edu.nus.comp.cs4218.impl.util.AppUtility;

public class FindApplication implements Application {

	private String pattern = "";
	private String rootPath = "";
	private String inputPath = ".";

	/**
	 * Get a list of directories path string under the input directory path.
	 * 
	 * @param absPath
	 *            The absolute directory path.
	 * @return The list of directories path under the path.
	 */
	private List<String> getDirectoryList(String absPath) {

		List<String> dirList = new ArrayList<String>();

		File directory = new File(absPath);

		File[] fileList = directory.listFiles();

		if (fileList == null) {
			return dirList;
		}

		for (int i = 0; i < fileList.length; i++) {
			if (fileList[i].isDirectory()) {
				try {
					dirList.add(fileList[i].getCanonicalPath());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return dirList;
	}

	/**
	 * Get the file list under the input directory.
	 * 
	 * @param absPath
	 *            The current directory.
	 * @return The list of file's path strings.
	 */
	private List<String> getFileList(String absPath) {
		List<String> fileList = new ArrayList<String>();

		File directory = new File(absPath);

		File[] folderList = directory.listFiles();

		if (folderList == null) {
			return fileList;
		}

		for (int i = 0; i < folderList.length; i++) {
			if (folderList[i].isFile()) {
				fileList.add(folderList[i].getName());
			}
		}

		return fileList;
	}

	/**
	 * Match the file name with the pattern
	 * 
	 * @param fileName
	 *            The input file name.
	 * @return True if the file name match the pattern.
	 */
	private boolean isMatched(String fileName) throws FindException {
		boolean isError = false;
		try{
			return fileName.matches(pattern);
		}
		catch(PatternSyntaxException e){
			isError = true;
		}
		if(isError){
			throw new FindException("Invalid pattern");
		}
		return !isError;
	}

	/**
	 * Write matched file name to standard output.
	 * 
	 * @param absPath
	 *            The absolute path.
	 * @param stdout
	 *            The standard output.
	 */
	private void writeMatchedFileName(String absPath, OutputStream stdout) {

		try {
			stdout.write(absPath.getBytes());
			stdout.flush();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Recursively search files and print the matched files.
	 * 
	 * @param startingPath
	 *            The starting path.
	 * @param stdout
	 *            The standard output.
	 */
	private void searchFiles(String startingPath, OutputStream stdout)
			throws FindException {

		String absPath = AppUtility.convertToAbsPath(startingPath);

		Stack<String> directoryQueue = new Stack<String>();
		directoryQueue.add(absPath);

		while (!directoryQueue.isEmpty()) {

			String currentDir = directoryQueue.pop();

			List<String> newDirs = this.getDirectoryList(currentDir);

			directoryQueue.addAll(newDirs); // add newly found directories to
											// queue

			List<String> filesInDir = this.getFileList(currentDir);

			// Match the pattern and write to standard output
			for (int i = 0; i < filesInDir.size(); i++) {

				String currentFile = filesInDir.get(i);

				if (this.isMatched(currentFile)) {
					// followed by new line
					String filePath = currentDir + File.separatorChar
							+ currentFile + System.lineSeparator();
					// replace root path with input path
					filePath = filePath.replace(rootPath, inputPath);
					this.writeMatchedFileName(filePath, stdout);
				}
			}
		}
	}

	/**
	 * Runs application with specified input data and specified output stream.
	 */
	public void run(String[] args, InputStream stdin, OutputStream stdout)
			throws FindException {

		// check stdout
		if (stdout == null) {
			throw new FindException("No specified stdout.");
		}

		// Check valid no. of args
		if (args == null || args.length < 2) {
			throw new FindException("Less than 2 argument.");
		}

		// if no path specified, using the current path
		if (args.length == 2) {
			if (!args[0].equals("-name")) {
				throw new FindException("No -name argument specified.");
			}
			if (Environment.currentDirectory == null
					|| Environment.currentDirectory.equals("")) {
				throw new FindException("No specified directory.");
			}
			rootPath = Environment.currentDirectory;
			pattern = args[1];

		} else if (args.length == 3) {
			if (!AppUtility.isValidPath(args[0])) {
				throw new FindException("No such directory.");
			}

			inputPath = args[0];
			rootPath = AppUtility.convertToAbsPath(args[0]);

			if (!args[1].equals("-name")) {
				throw new FindException("No -name argument specified.");
			}

			pattern = args[2];
		} else {
			throw new FindException("Too many arguments. "
					+ "Use format : find [PATH] name PATTERN");
		}

		pattern = pattern.replaceAll("\\*", ".*");
		// search and print matched files
		this.searchFiles(rootPath, stdout);
	}
}
