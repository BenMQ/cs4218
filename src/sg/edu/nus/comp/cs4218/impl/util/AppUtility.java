package sg.edu.nus.comp.cs4218.impl.util;

import java.io.File;
import java.io.IOException;

import sg.edu.nus.comp.cs4218.Application;
import sg.edu.nus.comp.cs4218.Environment;

public final class AppUtility {

	private AppUtility() {
	}

	/**
	 * Get current working directory.
	 * 
	 * @return The path of current working directory.
	 */
	public static String getCWD() {
		return Environment.currentDirectory;
	}

	/**
	 * Convert user's input path to absolute path. The path will be composition
	 * of '.', '~', and '..'
	 * 
	 * @param userPath
	 *            User's input path.
	 * @return The absolute path.
	 */
	public static String convertToAbsPath(String userPath) {

		String absPath = "";
		if (userPath.length() < 1) {
			return userPath;
		}
		if (userPath.charAt(0) == '~') {
			absPath = Environment.HOME_DIRECTORY + userPath.substring(1);
		} else if (userPath.charAt(0) == '/'
				|| (Environment.IS_WINDOWS && userPath.length() >= 2 && userPath
						.charAt(1) == ':')) {
			absPath = userPath;
		} else {
			absPath = Environment.currentDirectory + File.separatorChar
					+ userPath;
		}
		File tmpFile = new File(absPath);
		try {
			absPath = tmpFile.getCanonicalPath();
		} catch (IOException e) {
		}
		return absPath;
	}

	/**
	 * Check whether a path is a valid path.
	 * 
	 * @param path
	 *            The user input path.
	 * @return True if the input string is a valid path.
	 */
	public static boolean isValidPath(String path) {

		String absPath = convertToAbsPath(path);

		File file = new File(absPath);

		if (file.exists() && file.isDirectory()) {
			return true;
		}

		return false;
	}

	/**
	 * @param appName
	 *            name of the application
	 * @return a java instance of the appName specified
	 */
	public static Application getApplicationFromString(String rawAppName) {
		String appName = rawAppName;
		Application application = null;
		if (rawAppName == null || rawAppName.length() < 1) {
			return null;
		}
		appName = Character.toUpperCase(appName.charAt(0))
				+ appName.substring(1);
		try {
			Class className = Class.forName("sg.edu.nus.comp.cs4218.impl.app."
					+ appName + "Application");
			application = (Application) className.newInstance();
		} catch (ClassNotFoundException e) {
			application = null;
		} catch (InstantiationException e) {
			application = null;
		} catch (IllegalAccessException e) {
			application = null;
		} catch (NoClassDefFoundError e) {
			application = null;
		}
		return application;
	}
}
