package sg.edu.nus.comp.cs4218.impl.util;

/**
 * A collection of String utility methods
 * 

 */
public final class StringUtilities {
	/**
	 * Utility classes should not be instantiated
	 */
	private StringUtilities() {
	}

	/**
	 * Join an array of strings with a given delimiter. There are no trailing
	 * delimiters. For empty arrays, an empty string will be returned. If the
	 * array given is <code>null</code>, return <code>null</code>.
	 * 
	 * @param array
	 *            array to be converted.
	 * @param delimiter
	 *            delimiter to be inserted between adjacent elements.
	 * @return array joined with the delimiter.
	 */
	public static String joinArray(String[] array, String delimiter) {
		if (array == null) {
			return null;
		} else if (array.length == 0) {
			return "";
		} else {
			StringBuilder buffer = new StringBuilder();
			buffer.append(array[0]);
			for (int i = 1; i < array.length; i++) {
				buffer.append(delimiter);
				buffer.append(array[i]);
			}
			return buffer.toString();
		}
	}
}
