package sg.edu.nus.comp.cs4218.ext2;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintWriter;

import org.junit.Before;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.Application;
import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.exception.AbstractApplicationException;
import sg.edu.nus.comp.cs4218.exception.WcException;
import sg.edu.nus.comp.cs4218.impl.app.WcApplication;

public class WcApplicationTest {

	protected Application testApp;
	private ByteArrayOutputStream stdout;

	// Just helper field
	protected String[] args;

	@Before
	public void setUp() {
		Environment.currentDirectory = System.getProperty("user.dir")
				+ "/test-files-ef2/";
		testApp = new WcApplication();
		stdout = new ByteArrayOutputStream();

	}

	// Negative Test
	@Test(expected = WcException.class)
	public void testRunEmptyInputs() throws AbstractApplicationException {
		args = new String[] {};
		testApp.run(args, null, stdout);
	}

	@Test(expected = WcException.class)
	public void testProcessFileWrongInput() throws AbstractApplicationException {
		args = new String[] { "0lines.txt", "hello" };
		testApp.run(args, null, stdout);
	}

	@Test(expected = WcException.class)
	public void testReadFileNonExistentFile()
			throws AbstractApplicationException {
		args = new String[] { "abc.txt" };
		testApp.run(args, null, stdout);
	}

	@Test(expected = WcException.class)
	public void testCheckOptionInvalid() throws AbstractApplicationException {
		args = new String[] { "hey", "-w" };
		testApp.run(args, null, stdout);
	}

	// Positive Test
	@Test
	public void testWcPositive1() throws Exception {
		InputStream in = new ByteArrayInputStream("a\nb\nc\n".getBytes());
		testApp.run(new String[] { "-l", "-m", "-w" }, in, stdout);
		// Note: you can customize the output as long as the number of each
		// category is correct.
		if (Environment.IS_WINDOWS) {
			assertEquals("3\t3\t6\r\n", stdout.toString());
		} else {
			assertEquals("3\t3\t6\n", stdout.toString());
		}

	}

	@Test
	public void testWcPositive2() throws Exception {
		InputStream in = new ByteArrayInputStream("a\nb\nc\n".getBytes());
		testApp.run(new String[] { "-l" }, in, stdout);
		// Note: you can customize the output
		if (Environment.IS_WINDOWS) {
			assertEquals("3\r\n", stdout.toString());
		} else {
			assertEquals("3\n", stdout.toString());
		}

	}

	@Test
	public void testWcPositive3() throws Exception {
		PrintWriter out = new PrintWriter("test-files-ef2/a.txt");
		out.println("ABC\n");
		out.close();
		testApp.run(new String[] { "a.txt", "-l", "-m", "-w" }, null, stdout);
		// Note: you can customize the output
		if (Environment.IS_WINDOWS) {
			assertEquals("2\t2\t6\r\n", stdout.toString());
		} else {
			assertEquals("1\t1\t5\n", stdout.toString());
		}

	}

}
