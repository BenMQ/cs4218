package sg.edu.nus.comp.cs4218.ext2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.Shell;
import sg.edu.nus.comp.cs4218.exception.AbstractApplicationException;
import sg.edu.nus.comp.cs4218.exception.ShellException;
import sg.edu.nus.comp.cs4218.impl.cmd.Evaluator;

public class SubstitutionTest {

	private static Shell shell;
	private static ByteArrayOutputStream stdout;
	String[] args;

	static String readFile(String path) throws IOException {
		byte[] byteArr = Files.readAllBytes(Paths.get(System
				.getProperty("user.dir") + "/test-files-ef2/" + path));
		return new String(byteArr);
	}

	@Before
	public void setUp() {
		Environment.currentDirectory = System.getProperty("user.dir")
				+ "/test-files-ef2/";
		shell = new Evaluator();
	}

	@Test
	public void testSubPositve1() throws Exception {
		String cmdLine = "echo `echo ABC`";
		stdout = new ByteArrayOutputStream();
		shell.parseAndEvaluate(cmdLine, stdout);

		String expected = "ABC" + System.lineSeparator();

		assertEquals(expected, stdout.toString());
	}

	@Test
	public void testSubPositve2() throws Exception {
		String cmdLine = "echo `echo \"ABC\"`";
		stdout = new ByteArrayOutputStream();
		shell.parseAndEvaluate(cmdLine, stdout);

		String expected = "ABC" + System.lineSeparator();

		assertEquals(expected, stdout.toString());
	}

	@Test
	public void testSubPositve3() throws Exception {
		String cmdLine = "echo `echo ABC; echo DEF`";
		stdout = new ByteArrayOutputStream();
		shell.parseAndEvaluate(cmdLine, stdout);

		String expected = "ABC DEF" + System.lineSeparator();

		assertEquals(expected, stdout.toString());
	}

	@Test(expected = ShellException.class)
	public void testSubNegative1() throws AbstractApplicationException,
			ShellException {
		String cmdLine = "echo `ech ABC`";
		stdout = new ByteArrayOutputStream();
		shell.parseAndEvaluate(cmdLine, stdout);
	}

	@Test(expected = ShellException.class)
	public void testSubNegative2() throws AbstractApplicationException,
			ShellException {
		String cmdLine = "echo ``echo ABC`";
		stdout = new ByteArrayOutputStream();
		shell.parseAndEvaluate(cmdLine, stdout);
	}

	@Test
	public void testSubNegative3() throws Exception {
		try {
			String cmdLine = "echo `echo ABC > a.txt` `ech DEF`";
			stdout = new ByteArrayOutputStream();
			shell.parseAndEvaluate(cmdLine, stdout);
			fail("Should not reach!");
		} catch (Exception e) {
			String expected = "ABC" + System.lineSeparator();

			assertEquals(expected, readFile("a.txt"));
		}
	}

}
