package sg.edu.nus.comp.cs4218.ext2;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.File;

import org.junit.Before;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.Application;
import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.exception.AbstractApplicationException;
import sg.edu.nus.comp.cs4218.exception.FindException;
import sg.edu.nus.comp.cs4218.impl.app.FindApplication;

public class FindApplicationTest {

	protected Application testApp;
	private ByteArrayOutputStream stdout;

	@Before
	public void setUp() {
		Environment.currentDirectory = System.getProperty("user.dir")
				+ "/test-files-ef2/";
		testApp = new FindApplication();
		stdout = new ByteArrayOutputStream();
	}

	// Negative Test
	@Test(expected = FindException.class)
	public void testRunNoDirectoryGiven() throws AbstractApplicationException {
		Environment.currentDirectory = null;
		testApp.run(new String[] { "-name", "TEST.txt" }, null, stdout);
	}

	@Test(expected = FindException.class)
	public void testRunTooManyArgs() throws AbstractApplicationException {
		testApp.run(new String[] { "-name", "a.txt", "a.txt" }, null, stdout);
	}

	@Test(expected = FindException.class)
	public void testRunDirectoryGivenWrongOrderArgs()
			throws AbstractApplicationException {
		testApp.run(new String[] { "a.txt", "-name" }, null, stdout);
	}

	// Positive Test
	@Test
	public void testFindPostive1() throws Exception {
		testApp.run(new String[] { "-name", "TEST1.txt" }, null, stdout);
		// Note: You can customize the output.
		String stdoutStr = stdout.toString();
		int idx = stdoutStr.length() - 9 - System.lineSeparator().length();
		assertEquals("TEST1.txt", stdoutStr.substring(idx).trim());
	}

	@Test
	public void testFindPostive2() throws Exception {
		String path = Environment.currentDirectory + File.separator + "tmp";
		testApp.run(new String[] { path, "-name", "TEST.txt" }, null, stdout);
		// Note: You can customize the output.
		String stdoutStr = stdout.toString();
		int idx = stdoutStr.length() - 8 - System.lineSeparator().length();
		assertEquals("TEST.txt", stdoutStr.substring(idx).trim());
	}

	@Test
	public void testFindPostive3() throws Exception {
		String path = Environment.currentDirectory + File.separator + "tmp";
		testApp.run(new String[] { path, "-name", "*.txt" }, null, stdout);
		// Note: You can customize the output.
		String stdoutStr = stdout.toString();
		int idx = stdoutStr.length() - 8 - System.lineSeparator().length();
		assertEquals("TEST.txt", stdoutStr.substring(idx).trim());
	}
}
