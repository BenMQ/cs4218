Milestone 3 README

## Artifacts to be submitted:

1. Project source code.
2. Project test cases.
3. Test cases that verify fixes for the bugs discovered during Hackathlon in a
   separate test class (these are new test cases that verify the fixes).  Each
   test case should have a javadoc comment referring to the BUG_ID, fix location in
   the source code with class name and line numbers.

   This can be found in test.sg.edu.nus.comp.cs4218.hackathonfix.AppHacksFixes
   File location:
   cs4218_flaming-octo-robot/test-hackathon/test-cases/test/sg/edu/nus/comp/cs4218/hackathonfix/AppHacksFixes.java


4. Pdf file with a documentation for invalid (not valid) test cases or duplicates after Hackathlon.

	File location:
	cs4218_flaming-octo-robot/milestone-3-files/hackathon_invalid_test_documentation.pdf

5. Test cases that improve branch coverage over Milestone 2 test cases (in a separate test class).

	This can be found in test.sg.edu.nus.comp.cs4218.coverage.CoverageTest
	File location:
	cs4218_flaming-octo-robot/test/test/sg/edu/nus/comp/cs4218/coverage/CoverageTest.java


6. Two coverage reports that show branch coverage improvement with new test cases (incremental coverage).
   Coverage reports should be generated with EclEmma (html format).

   Branch coverage at Milestone 2: 80%
   Branch coverage at Milestone 3: 85%

   File location:
   cs4218_flaming-octo-robot/milestone-3-files/reports


7. QA report in PDF file format with team name and team members' names.

	File location:
   cs4218_flaming-octo-robot/milestone-3-files/qa-report
