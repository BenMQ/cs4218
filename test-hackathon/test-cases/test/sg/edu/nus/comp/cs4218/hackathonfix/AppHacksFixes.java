package test.sg.edu.nus.comp.cs4218.hackathonfix;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.File;

import org.junit.Before;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.Application;
import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.exception.AbstractApplicationException;
import sg.edu.nus.comp.cs4218.exception.HeadException;
import sg.edu.nus.comp.cs4218.exception.LsException;
import sg.edu.nus.comp.cs4218.exception.SedException;
import sg.edu.nus.comp.cs4218.exception.TailException;
import sg.edu.nus.comp.cs4218.exception.WcException;
import sg.edu.nus.comp.cs4218.impl.app.HeadApplication;
import sg.edu.nus.comp.cs4218.impl.app.LsApplication;
import sg.edu.nus.comp.cs4218.impl.app.SedApplication;
import sg.edu.nus.comp.cs4218.impl.app.TailApplication;
import sg.edu.nus.comp.cs4218.impl.app.WcApplication;

public class AppHacksFixes {

	private Application app;
	private ByteArrayOutputStream stdout;

	@Before
	public void setUp() throws Exception {

		Environment.currentDirectory = System.getProperty("user.dir")
				+ slashify("/test-hackathon/test-files-hack");

		app = null;
		stdout = new ByteArrayOutputStream();
	}

	/**
	 * Expected application exception when when an empty arg is given. However,
	 * StringIndexOutOfBoundsException was throw. This bug occur in both Wc and
	 * Ls application.
	 * 
	 * Refer to specification, page 9, under Applications specification: If
	 * command line arguments are wrong, the application throws an exception.
	 * 
	 * Fix Location:
	 * WcApplication, Lines:30-31,38,155-157
	 */
	@Test(expected = WcException.class)
	public void wcInvalidEmptyArg() throws AbstractApplicationException {

		String args[] = { "" };

		app = new WcApplication();
		app.run(args, null, stdout);
	}


	/**
	 * Ls Application does not throw an exception, when a valid file is given
	 * instead of a valid path.
	 * 
	 * Refer to specification, page 9, under Applications specification: ls
	 * [PATH], where PATH is define as the directory.
	 * 
	 * Fix Location:
	 * LsApplication, Lines: 52-57
	 */
	@Test(expected = LsException.class)
	public void lsInvalidPathWithValidFile()
			throws AbstractApplicationException {

		String args[] = { "validFile.txt" };

		app = new LsApplication();
		app.run(args, null, stdout);
	}

	/**
	 * Incorrect argument for Sed Application. Sed Application does not throw an
	 * exception when an incorrect the replacement argument format
	 * (s/regexp/replacement) is given. Missing separate symbol at end of the
	 * replacement argument.
	 * 
	 * Refer to specification, page 11, under Applications specification: Under
	 * specification replacement rule, a valid replacement argument is
	 * "s/regexp/replacement/" or "s/regexp/replacement/g".
	 * 
	 * Fix Location:
	 * SedApplication, Lines:57-60
	 */
	@Test(expected = SedException.class)
	public void sedInvalidReplacementArgument()
			throws AbstractApplicationException {

		String args[] = { "s/text/test", "randomText.txt" };

		app = new SedApplication();
		app.run(args, null, stdout);
	}

	/**
	 * Expected exception for invalid file argument. Head accepts invalid file
	 * and no exception is throw. A similar bug, occur when an invalid directory
	 * is given.
	 * 
	 * Refer to specification, page 9, under Applications specification: If
	 * command line arguments are wrong, the application throws an exception.
	 * 
	 * Fix Location:
	 * HeadApplication, Lines: 78-80
	 */
	@Test(expected = HeadException.class)
	public void headInvalidFileOrDirectory()
			throws AbstractApplicationException {

		String args[] = { "-n", "5", "invalid.txt" };

		app = new HeadApplication();
		app.run(args, null, stdout);
	}

	/**
	 * Expected exception for invalid file argument. Tail accepts invalid file
	 * and no exception is throw. An similar bug, occur when an invalid
	 * directory is given.
	 * 
	 * Refer to specification, page 9, under Applications specification: If
	 * command line arguments are wrong, the application throws an exception.
	 * 
	 * Fix Location:
	 * TaiApplication, Lines: 75-76
	 */
	@Test(expected = TailException.class)
	public void tailInvalidFileOrDirectory()
			throws AbstractApplicationException {

		String[] args = { "-n", "5", "invalid.txt" };

		app = new TailApplication();
		app.run(args, null, stdout);
	}

	/**
	 * Expected exception for invalid option value. Head accepts negative
	 * integer for option value.
	 * 
	 * Refer to specification, page 9, under Applications specification: If
	 * command line arguments are wrong, the application throws an exception.
	 * 
	 * Fix Location:
	 * HeadApplication, Lines: 151-154
	 */
	@Test(expected = HeadException.class)
	public void headNegativeOption() throws AbstractApplicationException {

		String[] args = { "-n", "-1", "longText.txt" };

		app = new HeadApplication();
		app.run(args, null, stdout);
	}

	/**
	 * Expected exception for invalid option value. Tail accepts negative
	 * integer for option value.
	 * 
	 * Refer to specification, page 9, under Applications specification: If
	 * command line arguments are wrong, the application throws an exception.
	 * 
	 * Fix Location:
	 * TailApplication, Lines: 146-148
	 */
	@Test(expected = TailException.class)
	public void tailNegativeOption() throws AbstractApplicationException {

		String[] args = { "-n", "-1", "longText.txt" };

		TailApplication app = new TailApplication();
		app.run(args, null, stdout);
	}

	
	/**
	 * Head Application should output the file content as it is, should not
	 * print additional newline when given a non-newline terminating content.
	 * 
	 * Refer to forum post, "Terminating newline in cat".
	 * 
	 * Fix Location:
	 * HeadApplication, Lines:157-166
	 */
	@Test
	public void headShouldNotTerminatesWithNewline()
			throws AbstractApplicationException {

		String[] args = { "non-newline-terminated.txt" };

		app = new HeadApplication();
		app.run(args, null, stdout);

		String expected = "no newline";
		assertEquals(expected, stdout.toString());
	}
	
	/**
	 * Tail Application should output the file content as it is, should not
	 * print additional newline when given a non-newline terminating content.
	 * 
	 * Refer to forum post, "Terminating newline in cat".
	 * 
	 * Fix Location:
	 * TailApplication, Lines: 159-164
	 */
	@Test
	public void tailShouldNotTerminatesWithNewline()
			throws AbstractApplicationException {

		String[] args = { "non-newline-terminated.txt" };

		app = new TailApplication();
		app.run(args, null, stdout);

		String expected = "no newline";
		assertEquals(expected, stdout.toString());
	}
	

	private String slashify(String arg) {
		return arg.replace('/', File.separatorChar);
	}

}
