package sg.edu.nus.comp.cs4218.impl.app;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.File;

import org.junit.Before;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.Application;
import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.exception.AbstractApplicationException;
import sg.edu.nus.comp.cs4218.exception.CatException;
import sg.edu.nus.comp.cs4218.exception.CdException;
import sg.edu.nus.comp.cs4218.exception.FindException;
import sg.edu.nus.comp.cs4218.exception.HeadException;
import sg.edu.nus.comp.cs4218.exception.LsException;
import sg.edu.nus.comp.cs4218.exception.SedException;
import sg.edu.nus.comp.cs4218.exception.TailException;
import sg.edu.nus.comp.cs4218.exception.WcException;

public class AppHacks {

	private Application app;
	private ByteArrayOutputStream stdout;

	@Before
	public void setUp() throws Exception {

		Environment.currentDirectory = System.getProperty("user.dir")
				+ slashify("/test-hackathon/test-files-hack");

		app = null;
		stdout = new ByteArrayOutputStream();
	}

	/**
	 * Expected application exception when when an empty arg is given. However,
	 * StringIndexOutOfBoundsException was throw. This bug occur in both Wc and
	 * Ls application.
	 * 
	 * Refer to specification, page 9, under Applications specification: If
	 * command line arguments are wrong, the application throws an exception.
	 */
	@Test(expected = WcException.class)
	public void wcInvalidEmptyArg() throws AbstractApplicationException {

		String args[] = { "" };

		app = new WcApplication();
		app.run(args, null, stdout);
	}

	/**
	 * Incorrect output for Wc Application's number of newlines. For example,
	 * given a text "abc", where it does not ends with a newline. The count
	 * should be 0, however, the currently Wc Application return 1. The bug
	 * occur due to the count of number of lines and not the number of newlines
	 * in any given text.
	 * 
	 * Refer to specification, page 11, under Applications specification: The
	 * option -l prints only the newline counts.
	 
	@Ignore("Refer to HACKATHON_ISSUE_REPORT_raw.txt") @Test
	public void wcCountingNewline() throws AbstractApplicationException {

		String args[] = { "-l", "non-newline-terminated.txt" };

		app = new WcApplication();
		app.run(args, null, stdout);

		String expected = "\t0";
		assertEquals(expected, stdout.toString());
	}*/

	/**
	 * Cd application should not accept absolute path as state in project
	 * description page 9 where cd [PATH] - relative directory path
	 */
	@Test(expected = CdException.class)
	public void cannotCDAbsolutePath() throws AbstractApplicationException {
		
		String absolutePath = System.getProperty("user.dir");
		String[] args = { absolutePath };
		
		app = new CdApplication();
		app.run(args, null, stdout);
	}

	/**
	 * Find Application throw an PatternSyntaxException instead FindExpection,
	 * when a absolute path is given under the PATTERN argument.
	 * 
	 * Refer to specification, page 9, under Applications specification: If
	 * command line arguments are wrong, the application throws an exception.
	 */
	@Test(expected = FindException.class)
	public void findInvalidPatternUsingAbsPath()
			throws AbstractApplicationException {

		File validFile = new File(Environment.currentDirectory, "validFile.txt");
		String[] args = { ".", "-name", validFile.getAbsolutePath() };

		app = new FindApplication();
		app.run(args, null, stdout);
	}

	/**
	 * Ls Application does not throw an exception, when a valid file is given
	 * instead of a valid path.
	 * 
	 * Refer to specification, page 9, under Applications specification: ls
	 * [PATH], where PATH is define as the directory.
	 */
	@Test(expected = LsException.class)
	public void lsInvalidPathWithValidFile()
			throws AbstractApplicationException {

		String args[] = { "validFile.txt" };

		app = new LsApplication();
		app.run(args, null, stdout);
	}

	/**
	 * Incorrect argument for Sed Application. Sed Application does not throw an
	 * exception when an incorrect the replacement argument format
	 * (s/regexp/replacement) is given. Missing separate symbol at end of the
	 * replacement argument.
	 * 
	 * Refer to specification, page 11, under Applications specification: Under
	 * specification replacement rule, a valid replacement argument is
	 * "s/regexp/replacement/" or "s/regexp/replacement/g".
	 */
	@Test(expected = SedException.class)
	public void sedInvalidReplacementArgument()
			throws AbstractApplicationException {

		String args[] = { "s/text/test", "randomText.txt" };

		app = new SedApplication();
		app.run(args, null, stdout);
	}

	/**
	 * Expected exception for invalid file argument. Head accepts invalid file
	 * and no exception is throw. A similar bug, occur when an invalid directory
	 * is given.
	 * 
	 * Refer to specification, page 9, under Applications specification: If
	 * command line arguments are wrong, the application throws an exception.
	 */
	@Test(expected = HeadException.class)
	public void headInvalidFileOrDirectory()
			throws AbstractApplicationException {

		String args[] = { "-n", "5", "invalid.txt" };

		app = new HeadApplication();
		app.run(args, null, stdout);
	}

	/**
	 * Expected exception for invalid file argument. Tail accepts invalid file
	 * and no exception is throw. An similar bug, occur when an invalid
	 * directory is given.
	 * 
	 * Refer to specification, page 9, under Applications specification: If
	 * command line arguments are wrong, the application throws an exception.
	 */
	@Test(expected = TailException.class)
	public void tailInvalidFileOrDirectory()
			throws AbstractApplicationException {

		String[] args = { "-n", "5", "invalid.txt" };

		app = new TailApplication();
		app.run(args, null, stdout);
	}

	/**
	 * Expected exception for invalid option value. Head accepts negative
	 * integer for option value.
	 * 
	 * Refer to specification, page 9, under Applications specification: If
	 * command line arguments are wrong, the application throws an exception.
	 */
	@Test(expected = HeadException.class)
	public void headNegativeOption() throws AbstractApplicationException {

		String[] args = { "-n", "-1", "longText.txt" };

		app = new HeadApplication();
		app.run(args, null, stdout);
	}

	/**
	 * Expected exception for invalid option value. Tail accepts negative
	 * integer for option value.
	 * 
	 * Refer to specification, page 9, under Applications specification: If
	 * command line arguments are wrong, the application throws an exception.
	 */
	@Test(expected = TailException.class)
	public void tailNegativeOption() throws AbstractApplicationException {

		String[] args = { "-n", "-1", "longText.txt" };

		TailApplication app = new TailApplication();
		app.run(args, null, stdout);
	}

	/**
	 * Expected CatException to be throw by Cat Application when given invalid
	 * argument. However, exception message was written into stdout message
	 * instead of exception being thrown. The bug occur also when an valid
	 * folder is given as a argument.
	 * 
	 * Refer to specification, page 9, under Applications specification: If
	 * command line arguments are wrong, the application throws an exception.
	 */
	@Test(expected = CatException.class)
	public void catWithInvalidArg() throws AbstractApplicationException {

		String[] args = { "invalid.txt" };

		app = new CatApplication();
		app.run(args, null, stdout);
	}

	/**
	 * Cat Application should output the file content as it is, should not
	 * print additional newline when given a non-newline terminating content.
	 * 
	 * Refer to forum post, "Terminating newline in cat".
	 */
	@Test
	public void catShouldNotTerminatesWithNewline()
			throws AbstractApplicationException {

		String[] args = { "non-newline-terminated.txt" };

		app = new CatApplication();
		app.run(args, null, stdout);

		String expected = "no newline";
		assertEquals(expected, stdout.toString());
	}
	
	/**
	 * Head Application should output the file content as it is, should not
	 * print additional newline when given a non-newline terminating content.
	 * 
	 * Refer to forum post, "Terminating newline in cat".
	 */
	@Test
	public void headShouldNotTerminatesWithNewline()
			throws AbstractApplicationException {

		String[] args = { "non-newline-terminated.txt" };

		app = new HeadApplication();
		app.run(args, null, stdout);

		String expected = "no newline";
		assertEquals(expected, stdout.toString());
	}
	
	/**
	 * Tail Application should output the file content as it is, should not
	 * print additional newline when given a non-newline terminating content.
	 * 
	 * Refer to forum post, "Terminating newline in cat".
	 */
	@Test
	public void tailShouldNotTerminatesWithNewline()
			throws AbstractApplicationException {

		String[] args = { "non-newline-terminated.txt" };

		app = new TailApplication();
		app.run(args, null, stdout);

		String expected = "no newline";
		assertEquals(expected, stdout.toString());
	}
	
	/**
	 * Sed Application should output the file content as it is, should not
	 * print additional newline when given a non-newline terminating content.
	 * 
	 * Refer to forum post, "Terminating newline in cat".
	 */
	@Test
	public void sedShouldNotTerminatesWithNewline()
			throws AbstractApplicationException {

		String[] args = { "s/new/old/g", "non-newline-terminated.txt" };

		app = new SedApplication();
		app.run(args, null, stdout);

		String expected = "no oldline";
		assertEquals(expected, stdout.toString());
	}
	
	/**
	 * CdApplication should not more than 1 path, if have should throw
	 * exception.
	 * 
	 * Refer to specifications, page 9 under CD command format:
	 * cd PATH   <= strictly 1 arg accepted only
	 * PATH: relative directory path.
	 */
	@Test(expected = CdException.class)
	public void shouldThrowExceptionWhenCdApplicationHasMoreThanOneArg()
			throws AbstractApplicationException {
		
		String[] args = { "..", slashify("../test-files") };
		
		app = new CdApplication();
		app.run(args, null, stdout);
	}

	private String slashify(String arg) {
		return arg.replace('/', File.separatorChar);
	}

}
