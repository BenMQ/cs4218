package sg.edu.nus.comp.cs4218.impl.cmd;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.exception.*;

public class CmdHacks {
	
	private static Evaluator shell;
	private ByteArrayOutputStream stdout;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		shell = new Evaluator();
	}

	@Before
	public void setUp() throws Exception {

		Environment.currentDirectory = System.getProperty("user.dir")
				+ slashify("/test-hackathon");

		stdout = new ByteArrayOutputStream();
	}

	/**
	 * The bug is due to improper escaping of back-quote that is enclosed in
	 * single-quote under command substitution situation.
	 * 
	 * Refer to specification, page 5, under Quoting grammar:
	 * <single-quoted>�::=�"'"�<non-newline�and�non-single-quote>�"'"
	 * 
	 * So inside single-quote, the interpretation of back-quote is literal.
	 */
	@Test
	public void shouldEscapeBackQuoteEnclosedInSingleQuote()
			throws AbstractApplicationException, ShellException {
		
		String cmdline = "echo `echo 'the ` should be escaped by single-quote'`";
		shell.parseAndEvaluate(cmdline, stdout);
		
		String expected = String.format("the ` should be escaped by single-quote%n");
		
		assertEquals(expected, stdout.toString());
	}
	
	/**
	 * The bug is due to evaluation of commands enclosed in back-quote first.
	 * 
	 * Refer to specification, page 7, under Sequence semantics:
	 * Run the first command; when the first command terminates, run the second command.
	 * 
	 * So for this test the command substitution in the 3rd command should be evaluated
	 * only after the first 2 commands are evaluated.
	 */
	@Test
	public void shouldEvaluateSequenceCommandInOrderFirst()
			throws AbstractApplicationException, ShellException, IOException {
		
		String cmdline = "echo abc > tmp; cat tmp; echo `echo def > tmp; cat tmp`; cat tmp";
		shell.parseAndEvaluate(cmdline, stdout);
		
		// Clean up files
		Files.deleteIfExists(Paths.get(Environment.currentDirectory, "tmp"));
		
		String expected = String.format("abc%n" + "def%n" + "def%n");
		
		assertEquals(expected, stdout.toString());
	}
	
	/**
	 * The bug is due to fixing the "first token" as the first argument and
	 * taking it as application name.
	 * 
	 * Refer to specification, page 6, under Call Command syntax:
	 * <call>�::=�
	 * 		[�<whitespace>�]�[�<redirection>�<whitespace>�]*�<argument>
	 * 		[�<whitespace>�<atom>�]*�[�<whitespace>�]
	 * 
	 * From the syntax, we can see that redirection can be the first element,
	 * before the first argument.
	 */
	@Test
	public void shouldAllowAnyOrderingOfElements()
			throws AbstractApplicationException, ShellException, IOException {
		
		String cmdline = "> tmp echo 'ordering of elements does not matter'; cat < tmp";
		shell.parseAndEvaluate(cmdline, stdout);

		// Clean up files
		Files.deleteIfExists(Paths.get(Environment.currentDirectory, "tmp"));
		
		String expected = String.format("ordering of elements does not matter%n");
		
		assertEquals(expected, stdout.toString());
	}
	
	/**
	 * The bug is due to output redirection of file immediately after "seeing"
	 * the output-redirection atom.
	 * 
	 * Refer to specification, page 6, under Call Command.
	 */
	@Test
	public void shouldWriteTextToFileForAnyOrderingOfElements()
			throws AbstractApplicationException, ShellException, IOException {
		
		String cmdline = "echo > tmp 'should not write empty file'; cat < tmp";
		shell.parseAndEvaluate(cmdline, stdout);

		// Clean up files
		Files.deleteIfExists(Paths.get(Environment.currentDirectory, "tmp"));
		
		String expected = String.format("should not write empty file%n");
		
		assertEquals(expected, stdout.toString());
	}
	
	/**
	 * The bug occurs at { cat < original > copy } where the file 'copy' is not
	 * redirected as output.
	 * 
	 * Refer to specification, page 6, under Call Command semantics:
	 * When Shell executes an application, it performs the following steps:
	 * 1.	IO-redirection. Open InputStream from the file for input redirection
	 * 		(the one following "<" symbol). Open the OutputStream to the file
	 * 		for output redirection (the one following ">" symbol).
	 * 
	 * This means that both input and output stream can be used at the same time.
	 */
	@Test
	public void shouldAllowInputAndOutputRedirectionUsedTogether()
			throws AbstractApplicationException, ShellException, IOException {
		
		String cmdline = "echo hello > original; cat < original > copy; cat copy";
		shell.parseAndEvaluate(cmdline, stdout);

		// Clean up files
		Files.deleteIfExists(Paths.get(Environment.currentDirectory, "original"));
		Files.deleteIfExists(Paths.get(Environment.currentDirectory, "copy"));
		
		String expected = String.format("hello%n");
		
		assertEquals(expected, stdout.toString());
	}
	
	/**
	 * The bug is due to some evaluator error but the main thing is that evaluator
	 * cannot support a redirection in the second command when it is piped with
	 * the first command.
	 * 
	 * Refer to forum post "Piping and IO Redirection".
	 */
	@Test
	public void inputRedirectionShouldHaveHigherPriorityToPipedInput()
			throws AbstractApplicationException, ShellException, IOException {
		
		String cmdline = slashify("echo 'piped input' | cat < test-files/5callop.txt133");
		shell.parseAndEvaluate(cmdline, stdout);
		
		String expected = "Scallops live in all the world's oceans.";
		
		assertEquals(expected, stdout.toString());
	}
	
	/**
	 * The bug occurs because the "empty arg" that replaced `echo ''` is
	 * treated as an argument which should not be after argument splitting.
	 * The final command executed should be simply { pwd } without any
	 * arguments ("empty arg" should not split as argument).
	 * 
	 * Refer to specification, page 8, under Command Substitution semantics:
	 * Note that command substitution is performed after command-level parsing
	 * but before argument splitting.
	 * 
	 * Also, refer to forum post "Whitespaces from substitution".
	 */
	@Test
	public void shouldSplitArgumentAfterCommandSubstitution()
			throws AbstractApplicationException, ShellException, IOException {
		
		String cmdline = "pwd `echo ''`";
		shell.parseAndEvaluate(cmdline, stdout);
		
		String expected = Environment.currentDirectory + System.lineSeparator();
		
		assertEquals(expected, stdout.toString());
	}
	
	/**
	 * The bug occurs because globbing returns a list of absolute paths.
	 * This test is independent of the args ordering that is returned.
	 * 
	 * Refer to specifications, page 7 under Globbing semantics:
	 * 1. Collect all the paths to existing files and directories such that
	 * these paths can be obtained by ==replacing all the unquoted asterisk==
	 * symbols in ARG by some (possibly empty) sequences of non-slash characters.
	 * 
	 * The reason absolute paths cannot be return can be traced back that only
	 * the asterisks can be replaced, in this case there should not be any new
	 * characters before "test-files/pr".
	 */
	@Test
	public void shouldReturnRelativePathAfterGlobbing()
			throws AbstractApplicationException, ShellException, IOException {
		
		String pattern = slashify("test-files/pr*wn/*");
		List<String> result = shell.glob(pattern);
		
		HashSet<String> expected = new HashSet<String>();
		Collections.addAll(expected,
				slashify("test-files/prawn/prawn01.txt"),
				slashify("test-files/prawn/prawn02.txt"),
				slashify("test-files/prawn/prawn03.txt"));
		
		for (String arg : result) {
			if (expected.contains(arg)) {
				expected.remove(arg);
			} else {
				fail();
			}
		}
		
		// If all args matched, the set will be empty.
		assertTrue(expected.isEmpty());
	}
	
	/**
	 * The bug occurs because when the globbing cannot resolve any match, it did not
	 * return the original argument.
	 * 
	 * Refer to specifications, page 8 under Globbing semantics:
	 * 3. If there are no such paths, leave ARG without changes.
	 */
	@Test
	public void shouldReturnOriginalArgIfNoMatchForGlobPattern()
			throws AbstractApplicationException, ShellException, IOException {
		
		String pattern = slashify("test-files/x*z");
		List<String> result = shell.glob(pattern);
		
		String expected = slashify("test-files/x*z");
		
		assertTrue(result.size() == 1);
		assertEquals(expected, result.get(0));
	}
	
	/**
	 * The bug occurs because args without dot (.) is returned, in this case,
	 * the directory "mussel7715" is returned.
	 * This tests ignored the result of absolute paths here, and also,
	 * is independent of the args ordering that is returned.
	 * 
	 * Refer to specifications, page 7 under Globbing.
	 */
	@Test
	public void shouldReturnArgsConformingToGlobPattern()
			throws AbstractApplicationException, ShellException, IOException {
		
		String pattern = slashify("test-files/oyster1337/*.*");
		List<String> result = shell.glob(pattern);
		
		HashSet<String> expected = new HashSet<String>();
		Collections.addAll(expected,
				slashify("test-files/oyster1337/oxintro01.txt"),
				slashify("test-files/oyster1337/oybody20.txt"),
				slashify("test-files/oyster1337/oybody30.txt"),
				slashify("test-files/oyster1337/ozend41.txt"));
		
		for (String arg : result) {
			if (expected.contains(arg)) {
				expected.remove(arg);
			} else {
				fail();
			}
		}
		
		// If all args matched, the set will be empty.
		assertTrue(expected.isEmpty());
	}
	
	/**
	 * The bug occurs because while '*' is escaped inside sub-command but it
	 * should be noted that after command substitution happens, the final
	 * command evaluated becomes { echo test-files/pr*wn } and the asterisk
	 * is not escaped.
	 * 
	 * Refer to specifications, page 6 under Call Command semantics:
	 * A call command is evaluated in the following order:
	 * 		1. Command substitution is performed.
	 * 		2. The command is split into arguments. The command string is split
	 * 			into substring corresponding to the <argument>�non-terminal.
	 * 			Note that one backquoted argument can produce several arguments
	 * 			after command substitution. All the quotes symbols that form
	 * 			<quoted> non-terminal are removed.
	 * 		3. Filenames are expanded (see globbing).
	 */
	@Test
	public void shouldEvaluateSubCommandFirstThenGlobbing()
			throws AbstractApplicationException, ShellException, IOException {
		
		String cmdline = slashify("echo `echo 'test-files/pr*wn'`");
		shell.parseAndEvaluate(cmdline, stdout);
		
		String expected = slashify(String.format("test-files/prawn%n"));
		
		assertEquals(expected, stdout.toString());
	}
	
	/**
	 * Shell should terminate execution of commands when one exception is
	 * encountered with sequential execution of semicolon.
	 * 
	 * Refer to specifications, page 7 under Semicolon Operator semantics:
	 * "If an exception is thrown during the execution of the first command,
	 * the execution if the whole command must be terminated."
	 */
	@Test (expected = PwdException.class)
	public void willNotContinueCommandExecutionWhenException()
			throws AbstractApplicationException, ShellException {

		String cmdline = "cat `pwd nothing`; echo abc";

		try {
			shell.parseAndEvaluate(cmdline, stdout);
		} catch (CatException e) {
			fail(); // should not have thrown CatException
		}

		// Actually this line should not be executed at all if PwdException is
		// thrown
		assertTrue(stdout.toString().indexOf("abc") == -1);
	}

	/**
	 * Parser should allow quoting with normal argument without whitespace
	 * character [<argument> ::= ( <quoted> | <unquoted> )+] therefore command :
	 * {echo a`echo b`} is syntax correct and should be executable
	 */
	@Test
	public void shouldAllowNoSpaceBetweenQuotingInArgument() throws AbstractApplicationException, ShellException {
        String cmdline = "echo a`echo b`";
        shell.parseAndEvaluate(cmdline, stdout);
			
		String expected = String.format("ab" + "%n");
		
		assertEquals(expected, stdout.toString());
	}
	
    /**
	 * Shell not execute multiple pipe correctly echo bc | wc | wc should out
	 * put line 1, word 3 and 8 chars but receiving line 1, word 1, 4 chars
	 * 
	 * Shell is believed to only pass first command to last command stdin
	 * skipping all pipes in between which is not correct
	 */
	@Test
	public void shouldSupportManyPipeCommands() throws AbstractApplicationException, ShellException {
        String cmdline = "echo bc | wc | wc";
        shell.parseAndEvaluate(cmdline, stdout);

		int lines = 1;
		int words = 3;
		int chars = 6 + System.lineSeparator().length();
		String expected = String.format("\t%d\t%d\t%d%n", lines, words, chars);
		
		assertEquals(expected, stdout.toString());
	}

	/**
     * Shell should not create file when the command line is ill-formed.
     * Cannot have ended off the command line with semicolon.
     * 
     * This test case is very similar to the one in Integration Testing,
     * slide 9 last test case.
     * 
     * Refer to specifications, page 5 under Command Line Parsing:
     * <seq> ::= <command> ";" <command>
     * Also stated in forum, a <command> cannot be empty.
     */
	@Test
	public void shouldNotExecuteFirstCommandWhenSecondCommandIsEmpty()
			throws AbstractApplicationException, IOException {

        Path tmpFile = Paths.get(Environment.currentDirectory, "tmpFile.txt");
        Files.deleteIfExists(tmpFile);  // ensure no such temp file
        
		String cmdline = "`echo a > tmpFile.txt`;";

		try {
			shell.parseAndEvaluate(cmdline, stdout);
			fail();  // ShellException must be thrown!
			
		} catch (ShellException e) {
			// Upon exception, no file should be written, if written,
			// it means that the exception was not due to cmdline grammar!
			if (tmpFile.toFile().exists()) {
				Files.deleteIfExists(tmpFile);  // clean up
				fail();  // should not exists
			}
		}
	}

	private String slashify(String arg) {
		return arg.replace('/', File.separatorChar);
	}

}
